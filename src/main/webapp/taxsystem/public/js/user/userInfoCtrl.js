tax.controller('userInfoCtrl',function($scope,$http){
  $scope.user = JSON.parse(sessionStorage.user);
  $scope.newuser = JSON.parse(sessionStorage.user);
  $scope.phoneTest = function (num){
    var re =/^1[3|7|5|8]\d{9}$/;
     if (re.test(num)) {
         return true;
     } else {
         return false;
     }
  }
  $http.get(getCompanyURL)
    .success(function(res){
      $scope.companys = res.resultParm.list;
    })
    .error(function(){
  });
  $scope.edit = false;
  $scope.editUser = function(){
    $scope.edit = !$scope.edit;
    if (!$scope.edit) {
      $scope.updateUser();
    }
  }
  $scope.cancelUser = function(){
    $scope.edit = false;
  }
  $scope.updateUser = function(){
    if ($scope.phoneTest($scope.newuser.phone)) {
      $http.post(updateUserURL,$scope.newuser)
        .success(function(res){
          alertMes(res.serviceResult,res.resultInfo);
          if (res.serviceResult) {
            $scope.user = angular.copy($scope.newuser);
            $scope.edit = false;
          }
        })
        .error(function(){
          alertMes('noHttp',null);
      });
    }else {
      Notify("请输入正确的手机号码", 'top-right', '5000', 'error', 'fa-times', true);
    }
  }
});
