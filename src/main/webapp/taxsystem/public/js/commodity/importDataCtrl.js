
tax.controller('importDataCtrl',function($scope,$http,$stateParams){
  //获取所有商品大类别
  $http.get(getLargeClassURL)
    .success(function(res){
      if (res.serviceResult) {
        $scope.largeClass = res.resultParm.largeClass;
      }
    })
    .error(function(){

  });

  //获取所有商品小类别
  $scope.$watch('selectLargeClass',function(nv,ov) {
		if (ov!=nv) {
      //获取所有商品类别
      $http.get(getSmallClassURL,{
        params:{
          largeClassId:$scope.selectLargeClass
        }
      })
      .success(function(res){
        if (res.serviceResult) {
          $scope.smallClass = res.resultParm.smallClass;
        }
      })
      .error(function(){

      });
		}
	});

  //获取所有公司信息
	$http.get(getCompanyURL)
		.success(function(res){
			$scope.companys = res.resultParm.list;
		})
		.error(function(){
	});

  //录入单条信息
  $scope.addRecordBtn = false;
  $scope.addRecord = function(){
    $scope.addRecordBtn = true;
    var formdata = new FormData(document.getElementById("formrecord"));
    var xhr = new XMLHttpRequest();
	    xhr.onload=function(event)
	    {
	      if  ( ( xhr.status >= 200 && xhr.status < 300) || xhr.status == 304)   //上传成功

	      {
          $scope.addRecordBtn = false;
					var response = JSON.parse(xhr.response);
					alertMes(response.serviceResult,response.resultInfo);
					if(response.serviceResult){
            $("#resBtn").click();
					}
	      }
	      else
	      {
	        alertMes(false,"信息录入失败");
          $scope.addRecordBtn = false;
	      }
	    };
			xhr.open("POST", addRecordURL,true);
			xhr.send(formdata);
  }

  //批量导入数据
  $scope.importModel = false;
  $scope.importData = function(){
    $scope.importModel = !$scope.importModel;
  }
  $scope.sureImport = function() {
    $scope.importBtn = true;
    var formdata = new FormData(document.getElementById("excleform"));
    var xhr = new XMLHttpRequest();
	    xhr.onload=function(event)
	    {
	      if  ( ( xhr.status >= 200 && xhr.status < 300) || xhr.status == 304)   //上传成功

	      {
          $scope.importBtn = false;
					var response = JSON.parse(xhr.response);
					alertMes(response.serviceResult,response.resultInfo);
					if(response.serviceResult){
            $(".closeImport").click();
					}
	      }
	      else
	      {
	        alertMes(false,"批量导入失败");
          $scope.importBtn = false;
	      }
	    };
			xhr.open("POST", impoerDataURL,true);
			xhr.send(formdata);
  }

});
