package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.SmallClass;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.CompanyService;
import com.hydx.vat.service.LargeClassService;
import com.hydx.vat.service.ListDataService;
import com.hydx.vat.service.SmallClassService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/smallClassHandler")
public class SmallClassHandler {

	@Autowired
	private SmallClassService smallClassService;

	@Autowired
	private LargeClassService largeClassService;

	@Autowired
	private CommodityService commodityService;

	@Autowired
	private ListDataService listDataService;

	@Autowired
	private CompanyService companyService;

	/*
	 * 获取所有小类别
	 */
	@RequestMapping(value = "getSmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getSmallClass() {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("smallClass", smallClassService.getSmallClass());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 这个公司每种商品每年的进销对比图表
	 */
	@RequestMapping(value = "getInAndOutSmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getSmallClass(Integer companyId, Integer smallClassId, Integer type,
			Integer largeClassId, Integer year, String num) {
		ResultMessage result = new ResultMessage();
		try {

			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			if (type == 0) {
				result.getResultParm().put("time", commodityService.getAllYear(year));
			}
			if (type == 1) {
				result.getResultParm().put("time", commodityService.getQuarter(companyId, year));
			}
			if (type == 2) {
				result.getResultParm().put("time", commodityService.getMonthBycompanyId(companyId, year));
			}

			result.getResultParm().put("inSmallClass",
					smallClassService.getOutSmallClass(companyId, largeClassId, smallClassId, type, year, num));
			result.getResultParm().put("outSmallClass",
					smallClassService.getInSmallClass(companyId, largeClassId, smallClassId, type, year, num));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取进小类别的营业额
	 */
	@RequestMapping(value = "getMoneyBySmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getMoneyBySmallClass(Integer companyId, Integer largeClassId, Integer type,
			Integer year, String num) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("smallClass", largeClassService.getSmallClass(largeClassId));
			result.getResultParm().put("InBySmallClass",
					smallClassService.getMoneyBySmallClass(companyId, largeClassId, type, year, num));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取所有的小类别的营业额
	 */
	@RequestMapping(value = "getAllMoneyBySmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllMoneyBySmallClass(Integer largeClassId, Integer type,
			Integer smallClassId, Integer year, String num) {
		ResultMessage result = new ResultMessage();
		List<Company> list = companyService.getCommodity();
		result.setResultInfo("查询成功");

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			if (smallClassId != null) {
				result.setResultInfo("查询成功");
				result.getResultParm().put("AllMoney",
						smallClassService.getMoneyBySmallClass(smallClassId, type, year, num));
				result.getResultParm().put("company", list);
			} else {
				result.setResultInfo("查询成功");
				result.getResultParm().put("company", list);
				result.getResultParm().put("AllMoney",
						smallClassService.getMoneyByLargeClass(largeClassId, type, year, num));
			}
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取进小类别的税率
	 */
	@RequestMapping(value = "getVatBySmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getVatBySmallClass(Integer companyId, Integer largeClassId, Integer type,
			Integer year, String num) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("smallClass", largeClassService.getSmallClass(largeClassId));
			result.getResultParm().put("InBySmallClass",
					smallClassService.getVatBySmallClass(companyId, largeClassId, type, year, num));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 添加小类别信息
	 */
	@RequestMapping(value = "addSmallClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addSmallClass(@RequestBody SmallClass smallClass) {
		ResultMessage result = new ResultMessage();

		try {
			Map<String, Object> map = new HashMap<String, Object>();
			smallClassService.addSmallClass(smallClass);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 添加小类别信息
	 */
	@RequestMapping(value = "updateSmallClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateSmallClass(@RequestBody SmallClass smallClass) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			smallClassService.updateSmallClass(smallClass);
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("更新失败");
		}
	}

	// 获取进销数量
	@RequestMapping(value = "getInSmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInSmallClass(Integer companyId, String data) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("SmallClass", smallClassService.getSmallClass());
			result.getResultParm().put("inSmallClass", smallClassService.getOutSmallClass(companyId, data));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 删除小类别信息
	 */
	@RequestMapping(value = "deleteSmallClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteSmallClass(Integer smallClassId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			smallClassService.deleteSmallClass(smallClassId);
			result.setResultInfo("删除成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}

	/*
	 * 预测小商品數量
	 */
	@RequestMapping(value = "forecast", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> forecast(Integer companyId, Integer smallClassId) {
		ResultMessage result = new ResultMessage();
		try {
			result.getResultParm().put("smallClass", smallClassService.findSmallClass(smallClassId));
			result.getResultParm().put("time", smallClassService.forecastMonth(companyId, smallClassId));
			result.getResultParm().put("forecast", smallClassService.forecast(companyId, smallClassId));
			result.setResultInfo("获取成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("获取失败");
		}
	}
}
