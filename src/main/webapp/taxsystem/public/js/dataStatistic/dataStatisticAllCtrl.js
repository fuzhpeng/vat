
tax.controller('dataStatisticAllCtrl',function($http,$scope){
	if (typeof sessionStorage.user === "undefined" || JSON.parse(sessionStorage.user).type!=1) {
		history.back();
	}
	//遍历已有年份
	$scope.getYear = function(){
		$http.get(getAllYearURL,{
			params:{
				// companyId:$stateParams.companyId
			}
		}).success(function(res){
			$scope.years = res.resultParm.year;
			$scope.selectYear = $scope.years[$scope.years.length-1];
		}).error(function(){

		});
	}
  $scope.getYear();
	//遍历商品大类别
	$http.get(getLargeClassURL)
		.success(function(res){
			if (res.serviceResult) {
				$scope.classes=res.resultParm.LargeClass;
				$scope.classPipe.largeClassId = $scope.classes[0].largeClassId;
				$scope.className = $scope.classes[0].largeClassName;
				$scope.largeClassName = $scope.className;
				$scope.getClass($scope.classes[0].largeClassId);
			}else {
				alertMes(res.serviceResult,res.resultInfo);
			}
		}).error(function(){
			alertMes('noHttp',null);
	});
	$scope.getClass = function(id) {
    $http.get(getSmallClassURL,{
      params:{
        largeClassId:id
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.smallclasses=res.resultParm.smallClass;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }

  /******* 营业额增长图表  **********/
	$scope.chart1 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:false,
		chartType:true,
		requestType:0,
		requestYear:0
	};
	//按月筛选
	$scope.checkByMonth1 = function(year,index){
		var temp = $scope.chart1.monthType;
		var temp2 = $scope.chart1.chartType;
		$scope.chart1 = {
			showByYear:false,
			showByMonth:true,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:temp,
			chartType:temp2
		};
		$scope.chart1.yearsArray[index]=true;
		$scope.chart1.requestYear = year;
		if($scope.chart1.monthType){$scope.chart1.requestType = 1;}
		else {$scope.chart1.requestType = 2;}
		$scope.getInitCharts1();
	}
	$scope.selMonthType1 = function(num){
		switch (num) {
			case 0:$scope.chart1.monthType = false;break;
			case 1:$scope.chart1.monthType = true;break
		}
		if($scope.chart1.monthType){$scope.chart1.requestType = 1;}
		else {$scope.chart1.requestType = 2;}
		$scope.getInitCharts1();
	}
	//按年筛选
	$scope.checkByYear1 = function (num) {
		var temp2 = $scope.chart1.chartType;
		$scope.chart1 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:false,
			chartType:temp2
		};
		$scope.chart1.conditions[num] = true;
		$scope.chart1.requestYear = num;
		$scope.chart1.requestType = 0;
		$scope.getInitCharts1();
	}
	//切换图标展示形式
	$scope.selChartType1 = function(num){
		switch (num) {
			case 0:$scope.chart1.chartType = true;break;
			case 1:$scope.chart1.chartType = false;break;
		}
		$scope.getInitCharts1();
	}
	$scope.getInitCharts1 = function(){
		var companys = [];//所有公司   图表的标注
		var xAxis = []; //横坐标
		var yAxis = [];//纵坐标
		var url;var name;
		if($scope.chart1.chartType){
			url = lineP_ALL_URL;
			name = "营业额";
		}
		else {
			url = lineV_ALL_URL;
			name ="税额";
		}
		$http.get(url,{
			params:{
				year:$scope.chart1.requestYear,
				type:$scope.chart1.requestType
			}
		})
		.success(function(res){
			if (res.serviceResult) {
				var dataArr= res.resultParm.TurnoverGrowth;
				for (var i = 0; i < res.resultParm.company.length; i++) {
					companys.push(res.resultParm.company[i].companyName);
					var obj ={
							name:res.resultParm.company[i].companyName,
							type:'line',
							stack: '总量',
							areaStyle: {normal: {}},
							data:dataArr[i]
					}
					yAxis.push(obj);
				}
				xAxis = res.resultParm.time;
				var option = {
				    title: {
				        text: name+'增长图表'
				    },
				    tooltip : {
				        trigger: 'axis',
				        axisPointer: {
				            type: 'cross',
				            label: {
				                backgroundColor: '#6a7985'
				            }
				        }
				    },
				    legend: {
				        data:companys
				    },
				    toolbox: {
				        feature: {
									dataView: {readOnly: false},
									magicType: {type: ['line', 'bar']},
									restore: {},
									saveAsImage: {}
				        }
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    xAxis : [
				        {
				            type : 'category',
				            boundaryGap : false,
				            data : xAxis
				        }
				    ],
				    yAxis : [
				        {
				            type : 'value'
				        }
				    ],
				    series : yAxis
				};
				echarts.init(document.getElementById('charts1')).setOption(option);
			}
		});
	}
	$scope.getInitCharts1();



	/******* 营业额、税额占比  **********/
	$scope.chart2 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:1,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		showSeason:false,
		showMonth:false
	};
	// $scope.all = true;
	// //选择商品大类别
	// $scope.selAll = function(){
	// 	$scope.all = true;$scope.classPipe.smallClassArray = [];
	// 	$scope.classPipe.smallClassId = null;
	// 	$scope.getInitPipeClass();
	// }
	//选择总计，最近几年
	$scope.checkByYear2 = function (num) {
		var temp = $scope.chart2.chartType;
		$scope.chart2 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp,
			requestType:0,
			requestYear:0,
			showSeason:false,
			showMonth:false
		};
		$scope.chart2.conditions[num] = true;
		$scope.chart2.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.chart2.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.chart2.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitCharts2();
	}

	//选择具体某一年
	$scope.checkByMonth2 = function(year,index){
		$scope.chart2.conditions = [];
		$scope.chart2.yearsArray = [];
		$scope.chart2.showByYear = false;$scope.chart2.showByMonth=true;
		$scope.chart2.yearsArray[index]=true;
		$scope.chart2.requestYear = year;
		$scope.getInitCharts2();
	}

	//选择具体某一年的年度统计
	$scope.selYear2 = function(){
		$scope.chart2.monthType = [true,false,false];
		$scope.chart2.showSeason = false;$scope.chart2.showMonth = false;
		$scope.chart2.requestType = 0;$scope.chart2.requestData = null;
		$scope.getInitCharts2();
	}

	//选择具体某一年的季度统计
	$scope.selSeason2 = function(){
		$scope.chart2.requestData = 1;
		$scope.chart2.season = [];
		$scope.chart2.season[0]=true;
		$scope.chart2.monthType = [false,true,false];
		$scope.chart2.showSeason = true;$scope.chart2.showMonth = false;
		$scope.chart2.requestType = 1;
		$scope.getInitCharts2();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason2 = function(num){
		$scope.chart2.requestData = num;
		$scope.chart2.season = [];
		$scope.chart2.season[num-1]=true;
		$scope.getInitCharts2();
	}

	//选择具体某一年的月度统计
	$scope.selMonth2 = function(){
		$scope.chart2.requestData = 1;
		$scope.chart2.month=[];
		$scope.chart2.month[0]=true;
		$scope.chart2.monthType = [false,false,true];
		$scope.chart2.showMonth = true;$scope.chart2.showSeason = false;
		$scope.chart2.requestType = 2;
		$scope.getInitCharts2();
	}
	//选择了某一个月
	$scope.selRequestMonth2 = function(num){
		$scope.chart2.requestData = num;
		$scope.chart2.month=[];
		$scope.chart2.month[num-1]=true;
		$scope.getInitCharts2();
	}

	$scope.getInitCharts2 = function(){
		$http.get(pipe_ALL_URL,{
			params:{
				year:$scope.chart2.requestYear,
				type:$scope.chart2.requestType,
				num:$scope.chart2.requestData
			}
		})
		.success(function(res){
			var source=[];var profit=[];var vat=[];
			for (var i = 0; i < res.resultParm.company.length; i++) {
				var objP = {
					value:res.resultParm.profit[i],
					name:res.resultParm.company[i].companyName
				};
				var objV = {
					value:res.resultParm.vat[i],
					name:res.resultParm.company[i].companyName
				};
				profit.push(objP);
				vat.push(objV);
				source.push(res.resultParm.company[i].companyName);
			}
			var option = {
					title: {
							text:'各子公司的销售额/税额占比图表',
							left: 'center'
					},
					tooltip: {
							trigger: 'item',
							formatter: "{a} <br/>{b}: {c} ({d}%)"
					},
					legend: {
							orient: 'vertical',
							x: 'left',
							data:source
					},
					toolbox: {
							show: true,
							orient: 'vertical',
							left: 'right',
							top: 'center',
							feature: {
									dataView: {readOnly: false},
									restore: {},
									saveAsImage: {}
							}
					},
					series: [
							{
									name:'营业额',
									type:'pie',
									selectedMode: 'single',
									radius: [0, '30%'],
									label: false,
									labelLine: {
											normal: {
													show: false
											}
									},
									data:profit
							},
							{
									name:'税额',
									type:'pie',
									radius: ['40%', '55%'],
									data:vat
							}
					]
			};
			echarts.init(document.getElementById('charts2')).setOption(option);
		});
	}
	$scope.getInitCharts2();



	/******* 进销对比图表  **********/
	// $scope.initProfitPipe = function(){
	// 	var option = {
	// 	    tooltip: {
	// 	        trigger: 'axis',
	// 	        axisPointer: {
	// 	            type: 'cross',
	// 	            crossStyle: {
	// 	                color: '#999'
	// 	            }
	// 	        }
	// 	    },
	// 	    toolbox: {
	// 	        feature: {
	// 	            dataView: {show: true, readOnly: false},
	// 	            magicType: {show: true, type: ['line', 'bar']},
	// 	            restore: {show: true},
	// 	            saveAsImage: {show: true}
	// 	        }
	// 	    },
	// 	    legend: {
	// 	        data:['进货量','销售量','库存']
	// 	    },
	// 	    xAxis: [
	// 	        {
	// 	            type: 'category',
	// 	            data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
	// 	            axisPointer: {
	// 	                type: 'shadow'
	// 	            }
	// 	        }
	// 	    ],
	// 	    yAxis: [
	// 	        {
	// 	            type: 'value',
	// 	            name: '金额',
	// 	            min: 0,
	// 	            max: 250,
	// 	            interval: 50,
	// 	            axisLabel: {
	// 	                formatter: '{value} 元'
	// 	            }
	// 	        },
	// 	        {
	// 	            type: 'value',
	// 	            name: '金额',
	// 	            min: 0,
	// 	            max: 25,
	// 	            interval: 5,
	// 	            axisLabel: {
	// 	                formatter: '{value} 元'
	// 	            }
	// 	        }
	// 	    ],
	// 	    series: [
	// 	        {
	// 	            name:'进货量',
	// 	            type:'bar',
	// 	            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
	// 	        },
	// 	        {
	// 	            name:'销售量',
	// 	            type:'bar',
	// 	            data:[2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
	// 	        },
	// 	        {
	// 	            name:'库存',
	// 	            type:'line',
	// 	            yAxisIndex: 1,
	// 	            data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
	// 	        }
	// 	    ]
	// 	};
	// 	echarts.init(document.getElementById('charts3')).setOption(option);
	// }
	// $scope.initProfitPipe();

	/************* 每种商品的各子公司对其的营业额贡献占比 ******************************/
	$scope.classPipe = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		largeClassArray:[true],
		smallClassArray:[],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:0,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		requestSource:0,//请求数据源，0表示营业额，1表示税额
		showSeason:false,
		showMonth:false,
		largeClassId:null,
		smallClassId:null
	}
	$scope.all = true;
	//选择商品大类别
	$scope.selAll = function(){
		$scope.all = true;
		$scope.classPipe.smallClassArray = [];
		$scope.classPipe.smallClassId = null;
		$scope.className = $scope.largeClassName;
		$scope.getInitPipeClass();
	}
	$scope.selClass4 = function(id,index,name) {
		$scope.getClass(id);
		$scope.all = true;
		$scope.classPipe.smallClassArray=[];
		$scope.classPipe.largeClassId = id;
		$scope.classPipe.smallClassId = null;
		$scope.classPipe.largeClassArray = [];
		$scope.classPipe.largeClassArray[index] = true;
		$scope.className = name;$scope.largeClassName = name;
		$scope.getInitPipeClass();
	}
	$scope.selSmallClass4 = function(id,index,name) {
		$scope.all = false;
		$scope.classPipe.smallClassId = id;
		$scope.classPipe.smallClassArray = [];
		$scope.classPipe.smallClassArray[index] = true;
		$scope.className = name;
		$scope.getInitPipeClass();
	}
	//选择总计，最近几年
	$scope.checkByYear3 = function (num) {
		var temp1 = $scope.classPipe.chartType;
		var temp2 = $scope.classPipe.largeClassArray;
		var temp3=$scope.classPipe.smallClassArray;
		var temp4 = $scope.classPipe.largeClassId;
		var temp5=$scope.classPipe.smallClassId;
		$scope.classPipe = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp1,
			requestType:0,
			requestYear:0,
			showSeason:false,
			showMonth:false,
			largeClassArray:temp2,
			smallClassArray:temp3,
			largeClassId:temp4,
			smallClassId:temp5
		};
		$scope.classPipe.conditions[num] = true;
		$scope.classPipe.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.classPipe.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.classPipe.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitPipeClass();
	}

	//选择具体某一年
	$scope.checkByMonth3 = function(year,index){
		$scope.classPipe.conditions = [];
		$scope.classPipe.yearsArray = [];
		$scope.classPipe.showByYear = false;$scope.classPipe.showByMonth=true;
		$scope.classPipe.yearsArray[index]=true;
		$scope.classPipe.requestYear = year;
		$scope.getInitPipeClass();
	}

	//选择具体某一年的年度统计
	$scope.selYear3 = function(){
		$scope.classPipe.monthType = [true,false,false];
		$scope.classPipe.showSeason = false;$scope.classPipe.showMonth = false;
		$scope.classPipe.requestType = 0;$scope.classPipe.requestData = null;
		$scope.getInitPipeClass();
	}

	//选择具体某一年的季度统计
	$scope.selSeason3 = function(){
		$scope.classPipe.requestData = 1;
		$scope.classPipe.season=[];
		$scope.classPipe.season[0]=true;
		$scope.classPipe.monthType = [false,true,false];
		$scope.classPipe.showSeason = true;$scope.classPipe.showMonth = false;
		$scope.classPipe.requestType = 1;
		$scope.getInitPipeClass();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason3 = function(num){
		$scope.classPipe.requestData = num;
		$scope.classPipe.season = [];
		$scope.classPipe.season[num-1]=true;
		$scope.getInitPipeClass();
	}

	//选择具体某一年的月度统计
	$scope.selMonth3 = function(){
		$scope.classPipe.requestData = 1;
		$scope.classPipe.month=[];
		$scope.classPipe.month[0]=true;
		$scope.classPipe.monthType = [false,false,true];
		$scope.classPipe.showMonth = true;$scope.classPipe.showSeason = false;
		$scope.classPipe.requestType = 2;
		$scope.getInitPipeClass();
	}
	//选择了某一个月
	$scope.selRequestMonth3 = function(num){
		$scope.classPipe.requestData = num;
	  $scope.classPipe.month=[];
		$scope.classPipe.month[num-1]=true;
		$scope.getInitPipeClass();
	}

	$scope.getInitPipeClass = function(){
		$http.get(pipeClass_ALL_URL,{
			params:{
				year:$scope.classPipe.requestYear,
				type:$scope.classPipe.requestType,
				num:$scope.classPipe.requestData,
				largeClassId:$scope.classPipe.largeClassId,
				smallClassId:$scope.classPipe.smallClassId
			}
		})
		.success(function(res){
			if (res.serviceResult) {
			var data=[];var source=[];
			for (var i = 0; i < res.resultParm.company.length; i++) {
				source.push(res.resultParm.company[i].companyName);
				var obj={value:res.resultParm.AllMoney[i], name:res.resultParm.company[i].companyName};
				data.push(obj);
			}
			var	option = {
						title : {
								text: '营业额占比图',
								subtext:'对于 '+$scope.className+' 商品各子公司的营业额占比饼图',
								x:'center'
						},
				    tooltip: {
				        trigger: 'item',
				        formatter: "{a} <br/>{b}: {c} ({d}%)"
				    },
				    legend: {
				        orient: 'vertical',
				        x: 'left',
				        data:source
				    },
						toolbox: {
				        show: true,
				        feature: {
				            dataView: {readOnly: false},
				            saveAsImage: {}
				        }
				    },
				    series: [
				        {
				            name:'营业额',
										type: 'pie',
				            radius : '55%',
				            center: ['50%', '60%'],
				            data:data
				        }
				    ],
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
				};
			}
	    echarts.init(document.getElementById('classPipe')).setOption(option);
		})
	}
	$scope.getInitPipeClass();

	/************* 客户/供货商分布图  ******************************/
	$scope.getMap = function(){
		var arr1 = [];var arr2 = [];
		$http.get(mapCP_ALL_URL)
		.success(function(res) {
			for (var i = 0; i < res.resultParm.client.length; i++) {
				var obj = {
					name:res.resultParm.client[i],
					value:res.resultParm.AcountCompanyByclient[i]
				};
				arr1.push(obj);
			}
			for (var j = 0; j < res.resultParm.provider.length; j++) {
				var obj = {
					name:res.resultParm.provider[j],
					value:res.resultParm.AcountCompanyByprovider[j]
				};
				arr2.push(obj);
			}
			var option = {
					title: {
							text:'客户/供货商分布图',
							left: 'center'
					},
					tooltip: {
							trigger: 'item'
					},
					legend: {
							orient: 'vertical',
							left: 'left',
							data:['客户','供货商']
					},
					visualMap: {
							min: 0,
							max: 2500,
							left: 'left',
							top: 'bottom',
							text: ['高','低'],           // 文本，默认为数值文本
							calculable: true
					},
					toolbox: {
							show: true,
							orient: 'vertical',
							left: 'right',
							top: 'center',
							feature: {
									dataView: {readOnly: false},
									restore: {},
									saveAsImage: {}
							}
					},
					series: [
							{
									name: '客户',
									type: 'map',
									mapType: 'china',
									roam: false,
									label: {
											normal: {
													show: true
											},
											emphasis: {
													show: true
											}
									},
									data:arr1
							},
							{
									name: '供货商',
									type: 'map',
									mapType: 'china',
									label: {
											normal: {
													show: true
											},
											emphasis: {
													show: true
											}
									},
									data:arr2
							}
					]
			};
			echarts.init(document.getElementById('map')).setOption(option);
		});
	}
	$scope.getMapCharts = function() {
		$http.get('json/china.json')
		.success(function(chinaJson){
			echarts.registerMap('china', chinaJson);
			$scope.getMap();
		});
	}
	$scope.getMapCharts();
});
