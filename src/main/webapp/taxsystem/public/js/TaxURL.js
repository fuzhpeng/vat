const PREV = "http://localhost:8080/vat/";

/*********** 分公司模块  **************/
const addCompanyURL = PREV + "companyHandler/addCompany";//添加分公司
const getCompanyURL = PREV + "companyHandler/getCompany";//获取所有公司（Get 请求  currentPage 页码 size 一页展示的数量）
const updateCompanyURL = PREV + "companyHandler/updateCompany";//更新公司信息
const delCompanyURL = PREV + "companyHandler/deleteCompany";//删除公司信息（GET请求 companyId ）


/*********** 大类别模块  **************/
const addLargeClassURL = PREV + "largeClassHandler/addLargeClass";//添加大类别信息
const getLargeClassURL = PREV + "largeClassHandler/getlargeClass";//获取所有大类别信息
const updateLargeClassURL = PREV + "largeClassHandler/updateLargeClass";//更新大类别信息
const delLargeClassURL = PREV + "largeClassHandler/deletelargeClass";//删除大类别信息（GET请求 largeClassId ）

/*********** 小类别模块  **************/
const addSmallClassURL = PREV + "smallClassHandler/addSmallClass";//添加小类别信息
const getSmallClassURL = PREV + "smallClassHandler/getSmallClass";//获取所有小类别信息
const updateSmallClassURL = PREV + "smallClassHandler/updateSmallClass";//更新小类别信息
const delSmallClassURL = PREV + "smallClassHandler/deleteSmallClass";//删除小类别信息（GET请求 smallClassId ）


/*********** 用户管理模块  **************/
const addUserURL = PREV + "userHandler/addUser";//用户注册
const delUserURL = PREV + "userHandler/deleteUser";//删除用户（GET请求，userId）
const getAllUserURL = PREV + "userHandler/getAllUser";//获取所有用户（GET请求，currentPage=0&size=5）
const getUserURL = PREV + "userHandler/getUser";//获取具体用户信息（GET请求 userId）
const updateUserURL = PREV + "userHandler/updateUser";//修改用户
const loginURL = PREV + "userHandler/login";

/*********** 发票模块  **************/
const addCommodityURL = PREV + "commodityHandler/addCommodity";//添加发票
const getAllYearURL = PREV + "commodityHandler/getAllYear";//获取所有年份
const addPhotoURL = PREV + "commodityHandler/addphoto";//上传发票图片
const excelURL = PREV + "commodityHandler/addAllCommodity";//上传excel表格文件
const getAllCommodityURL = PREV + "commodityHandler/getAllCommodity";//获取所有发票（GET请求 companyId=85&&date=2017&commodityType=1&startDate=2017&endDate=2018）
      //  companyId 分公司id   date 年份 可以为空   commodityType 进出发票 0为进 1为销
const delCommodityURL = PREV + "commodityHandler/delCommodity";//删除发票
const getCommodityURL = PREV + "commodityHandler/getCommodity";//获取具体发票(GET请求，commodityId)
const getCommodityByNumURL = PREV + "commodityHandler/getCommodityByNum";//通过发票代码获取具体发票(GET请求，vatNumber)
const getGoodsListURL = PREV + "listDataHandler/getAllListData";//
const updateCommodityURL = PREV + "commodityHandler/updateCommodity";//修改发票
const getInvoiceMoneyURL = PREV + "commodityHandler/getCommodityMoney";


/*********** 统计模块  **************/
//按公司统计
  //折线图
  const lineP_URL = PREV + "commodityHandler/getTurnoverGrowthByCompany";//营业额
  const lineV_URL = PREV + "commodityHandler/getVatGrowth";//税额
  //柱状图
  const bar_URL = PREV + "smallClassHandler/getInAndOutSmallClass";
  //饼图
  const pipeBP_URL = PREV + "largeClassHandler/getMoneyByLargeClass";//大分类营业额
  const pipeBV_URL = PREV + "largeClassHandler/getVatByLargeClass";//大分类税额
  const pipeSP_URL = PREV + "smallClassHandler/getMoneyBySmallClass";//小分类营业额
  const pipeSV_URL = PREV + "smallClassHandler/getVatBySmallClass";//小分类税额
  const pipeClient_URL = PREV + "InCompanyHandler/getTurnoverProportion";//客户贡献饼图
  const pipeProvider_URL = PREV + "outCompanyHandler/getTurnoverProportion";//供货商贡献饼图
  //地图
//按全部公司统计
  //折线图
  const lineP_ALL_URL = PREV + "commodityHandler/getAllTurnoverGrowth";//营业额
  const lineV_ALL_URL = PREV + "commodityHandler/getAllVatGrowth";//税额
  //柱状图
  //饼图
  const pipe_ALL_URL = PREV + "commodityHandler/getAllTurnover";
  const pipeClass_ALL_URL = PREV + "smallClassHandler/getAllMoneyBySmallClass";
  //地图
  const mapCP_ALL_URL = PREV + "InCompanyHandler/getCompanyPlce";//客户和供货商


/*********** 供应商模块  **************/
const getProviderURL = PREV + "InCompanyHandler/getAllInCompany";//获取所有客户/供应商（Get 请求  currentPage 页码 size 一页展示的数量）
const delProviderURL = PREV + "InCompanyHandler/deleteCompany";//get请求参数companyId
const updateProviderURL = PREV + "InCompanyHandler/updateCompany";
const getProviderByIdURL = PREV + "InCompanyHandler/getInCompanyById";
const getProviderByNameURL = PREV + "InCompanyHandler/getInCompany";
const addProviderURL = PREV + "InCompanyHandler/addCompany";


/*********** 客户模块 *************/
const getClientURL = PREV + "outCompanyHandler/getAllInCompany";//获取所有客户/供应商（Get 请求  currentPage 页码 size 一页展示的数量）
const delClientURL = PREV + "outCompanyHandler/deleteCompany";
const updateClientURL = PREV + "outCompanyHandler/updateCompany";
const getClientByIdURL = PREV + "outCompanyHandler/getInCompanyById";
const getClientByNameURL = PREV + "outCompanyHandler/getInCompany";
const addClientURL = PREV + "outCompanyHandler/addCompany";

/*********** 预计分析模块 *************/
const predictURL = PREV + "smallClassHandler/forecast";
