if(typeof sessionStorage.user === "undefined"){
	//还未登录
	location.href = "/vat/taxsystem/login.html";
}else {
	const USER = JSON.parse(sessionStorage.user);
}
// 主页面 的controller
tax.controller('taxCtrl',function($rootScope,$scope,$location,$http,$stateParams){
	$scope.tableTitle = "欢迎使用";

	$scope.$on('$stateChangeSuccess', function(){
		if($location.path() === "/welcome"){
			$scope.tableTitle = "欢迎使用";
		}else if($location.path()==="/recordsPage"){
			$scope.tableTitle = "发票记录 - "+$stateParams.companyName;
		}else if($location.path() === "/smallClassRecords"){
			$scope.tableTitle = "商品小分类记录 - "+$stateParams.companyName+" - "+$stateParams.className;
		}else if($location.path() === "/largeClassRecords"){
			$scope.tableTitle = "商品大分类记录 - "+$stateParams.companyName+" - "+$stateParams.className;
		}else if($location.path() === "/importVat"){
			$scope.tableTitle = "导入数据 - 填写发票信息";
		}else if($location.path() === "/dataStatistic"){
			$scope.tableTitle = "数据统计 - "+$stateParams.companyName;
		}else if($location.path() === "/dataStatisticAll"){
			$scope.tableTitle = "数据统计";
		}else if($location.path() === "/showCompanyVat"){
			$scope.tableTitle = "发票记录 - 公司入口";
		}else if($location.path() === "/vatInfo"){
			$scope.tableTitle = "发票详情";
		}else if($location.path() === "/companyInfo"){
			if ($stateParams.type==1) {
				$scope.tableTitle = "销售方详情";
			}else {
				$scope.tableTitle = "购买方详情";
			}
		}else if($location.path() === "/goodsList"){
			$scope.tableTitle = "进销清单";
		}else if($location.path() === "/clientInfo"){
			$scope.tableTitle = "客户信息管理";
		}else if($location.path() === "/providerInfo"){
			$scope.tableTitle = "供货商信息管理";
		}else if($location.path() === "/userInfo"){
			$scope.tableTitle = "个人中心";
		}else if($location.path() === "/classInfo"){
			$scope.tableTitle = "商品大类别管理";
		}else if($location.path() === "/smallClassInfo"){
			$scope.tableTitle = "商品小类别管理 - "+$stateParams.largeClassName;
		}else if($location.path() === "/systemUser"){
			$scope.tableTitle = "系统用户管理";
		}else if($location.path() === "/updateVat"){
			$scope.tableTitle = "修改发票";
		}else if($location.path() === "/companyManage"){
			$scope.tableTitle = "公司信息管理";
		}else if($location.path() === "/dataPredictive"){
			$scope.tableTitle = "预测评估";
		}
	});
	$scope.moreCompany = true;
	//获取所有公司信息
	$http.get(getCompanyURL,{
			params:{
				currentPage:0,
				size:10
			}
		})
		.success(function(res){
			$scope.companys = res.resultParm.list;
			if (res.resultParm.totalPages>1) {
				$scope.moreCompany = !$scope.moreCompany;
			}
		})
		.error(function(){
	});

	$scope.noshowrole = false;
	if(typeof sessionStorage.user != "undefined"){
		$scope.user = JSON.parse(sessionStorage.user);
		if ($scope.user.type===1) {
			$scope.noshowrole = true;
		}
	}
	$scope.exit = function(){
		sessionStorage.removeItem("user");
	}
});
