tax.controller('invoicesCtrl',function($scope,$http,$stateParams){
	$scope.invoices = null;// 发票记录

	$scope.invoicesMoneyIn = 0;// 总收入
  $scope.invoicesMoneyOut = 0;// 总支出
	$scope.vatMoney = 0; //总纳税金额

	$scope.totalPage = 1;//全部页数
	$scope.currentPage = 1;//当前页码
	$scope.size = 15;
	$scope.showModal = false;
	$scope.showUModal = false;

	$scope.startDate = null;
	$scope.endDate = null;
	$scope.vatNumber = null;

	//发票记录类型 ：0：进项 1：出项,2总记录
	$scope.recordType = 2;

	//公司的id和名字
	$scope.companyId = $stateParams.companyId;
	$scope.companyName = $stateParams.companyName;


	// ***********   发票记录的 阅删改查      ******************//

	/***获取发票记录start***/
		$scope.getInvoices = function(start,end,num){
			if(num>=1 && num<=$scope.totalPage){
					$http.get(getAllCommodityURL,
						{
							params:{
								startDate:start,
								endDate:end,
								date:$scope.selectYear,
								currentPage:num-1,
								size:$scope.size,
								companyId:$stateParams.companyId,
								commodityType:$scope.recordType,
								vatNumber:$scope.vatNumber
							}
						}
					)
					.success(function(response){
						$scope.invoices=response.resultParm.list;
						$scope.totalPage = response.resultParm.totalPages;
						$scope.currentPage = response.resultParm.currentPage+1;
					}).error(function(){
						alertMes('noHttp',null);
				});
			}
		}
		$scope.amountMoney = function(start,end){
			$http.get(getInvoiceMoneyURL,{
					params:{
						startDate:start,
						endDate:end,
						companyId:$stateParams.companyId,
						year:$scope.selectYear
					}
				})
				.success(function(response){
					$scope.recordMoney=response.resultParm;
				}).error(function(){
					alertMes('noHttp',null);
			});
		}
		$scope.getRecord = function(){
			$scope.getInvoices($scope.startDate,$scope.endDate,$scope.currentPage);
		}
		$scope.getInvoicesMoney = function(){
			$scope.amountMoney($scope.startDate,$scope.endDate);
		}
	/***获取发票记录end*****/


	/***删除发票记录start***/
		$scope.delRecord = function(id){
			$scope.showModal = !$scope.showModal;
			$scope.delRecordId = id;
		}
		$scope.sureDelRecord = function(){
			$http.get(delCommodityURL,{
				params:{commodityId:$scope.delRecordId}
			}).success(function(res){
				$scope.showModal = !$scope.showModal;
				if (res.serviceResult) {
					$scope.getRecord();
				}
				alertMes(res.serviceResult,res.resultInfo);
			}).error(function(){
				alertMes("noHttp",null);
			});
		}
	/***删除发票记录end*****/


	/***查看发票记录start***/
		//根据时间段查看
		$scope.checkByDate = function(){
			sessionStorage.currentPage = $scope.currentPage;
			$scope.clearResult1 = true;
			var date = $("#reservation").val();
			if(date!=''&&date!=null){
				var dateArr = date.split(" - ");
				$scope.totalPage = 1;//全部页数
				$scope.currentPage = 1;//当前页码
				$scope.startDate = dateArr[0];
				$scope.endDate = dateArr[1] + " 24:00:00";
				$scope.getRecord();
				$scope.getInvoicesMoney();
			}
		}
		$scope.keyupDate = function(){
			if(window.event.keyCode == 13){
				$scope.checkByDate();
			}
		}
		//根据年份查看
		$scope.$watch('selectYear',function(nv,ov) {
			if (ov!=undefined) {
				$scope.totalPage = 1;//全部页数
				$scope.currentPage = 1;//当前页码
				$scope.getRecord();
				$scope.getInvoicesMoney();
			}
		});
		//根据发票代码查查
		$scope.searchByNum = function(id){
			sessionStorage.currentPage = $scope.currentPage;
			$scope.clearResult2 = true;
			if(id!=null && id!='' && id!=' '){
				$http.get(getCommodityByNumURL,{params:{vatNumber:id}})
				.success(function(res) {
					if (res.serviceResult) {
						$scope.invoices=res.resultParm.list;
						$scope.totalPage = res.resultParm.totalPages;
						$scope.currentPage = res.resultParm.currentPage+1;
					}else {
						alertMes(res.serviceResult,res.resultInfo);
					}
				})
				.error(function(){
					alertMes('noHttp',null);
				});
			}
		}
		$scope.keyupId = function(){
			if(window.event.keyCode == 13){
				$scope.searchByNum($scope.checkVatId);
			}
		}
		$scope.clearCheck1 = function(){
	    $scope.clearResult1 = !$scope.clearResult1;
	    $scope.getInvoices(null,null,sessionStorage.currentPage);
			$scope.checkrecordDate = "";
	  }
		$scope.clearCheck2 = function(){
	    $scope.clearResult2 = !$scope.clearResult2;
	    $scope.getInvoices(null,null,sessionStorage.currentPage);
			$scope.checkVatId = "";
	  }
		//根据发票类型查看
		$scope.changeType = function(num){
			$scope.recordType = num;
			$scope.totalPage = 1;//全部页数
			$scope.currentPage = 1;//当前页码
			$scope.getRecord();
		}
	/***查看发票记录end*****/


	/******  遍历已有年份start  *********/
		$scope.getYear = function(){
			$http.get(getAllYearURL,{
				params:{
					companyId:$stateParams.companyId
				}
			}).success(function(res){
				$scope.years = res.resultParm.year;
			}).error(function(){

			});
		}
	/*****   遍历已有年份end   ****/


	$scope.getYear();
	$scope.getRecord();
	$scope.getInvoicesMoney()
});
