package com.hydx.vat.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CompanyDao;
import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.service.CompanyService;

@Service("CompanyService")
public class CompanyServiceImpl implements CompanyService {

	@Autowired
 	 private CompanyDao companyDao;
	
	@Override
	public List<Company> getCommodity() {
	return companyDao.findAll();
	}

	@Override
	public void addCommodity(Company company) {
		companyDao.save(company);
	}

	public Page<Company> getCommodity(Pageable pageable) {
		return companyDao.findAll(pageable);
	}

	@Override
	public void deleteCommodity(Integer id) {
		companyDao.delete(id);
	}

	@Override
	public void updateCommodity(Company company) {
		companyDao.saveAndFlush(company);
	}

}
