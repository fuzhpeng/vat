package com.hydx.vat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.PrivilegeDao;
import com.hydx.vat.entity.Privilege;
import com.hydx.vat.service.PrivilegeService;



@Service("privilegeService")
public class PrivilegeServiceImpl implements PrivilegeService{

	@Autowired
	private PrivilegeDao privilegeDao;

	/**
	 * 获取所有权限列表.
	 * @author Kellan
	 * @return
	 */
	public List<Privilege> getAllPrivilege() {
		return privilegeDao.findAll();
	}

	/**
	 * 获取某角色所拥有的权限列表.
	 * @author Kellan
	 * @param roleId
	 * @return
	 */
	public List<Privilege> getPrivilegeByRoleId(Integer roleId) {
		return privilegeDao.getPrivilegeByRoleId(roleId);
	}
	
	
	
	
}
