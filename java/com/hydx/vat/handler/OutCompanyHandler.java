package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.OutCompany;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.InCompanyService;
import com.hydx.vat.service.OutCompanyService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/outCompanyHandler")
public class OutCompanyHandler {
	@Autowired
	private OutCompanyService outCompanyService;

	@Autowired
	private CommodityService commodityService;

	@Autowired
	private InCompanyService inCommodityService;

	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getAllInCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllOutCompany(
			@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
			@RequestParam(value = "size", defaultValue = "5") Integer size) {
		ResultMessage result = new ResultMessage();

		try {
			if (currentPage != null && size != null) {
				Pageable pageable = new PageRequest(currentPage, size);
				Page<OutCompany> page = outCompanyService.getOutCompany(pageable);
				Map<String, Object> map = new HashMap<String, Object>();
				result.setResultInfo("查询成功");
				result.getResultParm().put("list", page.getContent());
				result.getResultParm().put("totalPages", page.getTotalPages());
				result.getResultParm().put("currentPage", page.getNumber());
				return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
			} else {
				result.getResultParm().put("list", outCompanyService.getOutCompany());
				return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getInCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInCompany(String InCompanyName) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("list", outCompanyService.getOutCompany(InCompanyName));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getInCompanyById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInCompanyById(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("InCompany", outCompanyService.getOutCompanyById(companyId));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取客户的分布
	 */
	@RequestMapping(value = "getCompanyPlce", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllInCompany(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			if (companyId != null) {
				result.getResultParm().put("Place", outCompanyService.getPlace());
				result.getResultParm().put("AcountCompanyByPlace", outCompanyService.AcountCompanyByPlace(companyId));
			} else {
				result.getResultParm().put("Place", outCompanyService.getPlace());
				result.getResultParm().put("AcountCompanyByPlace", outCompanyService.AcountCompanyByPlace());
			}
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 添加公司信息
	 */
	@RequestMapping(value = "addCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addCommodity(OutCompany company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			outCompanyService.addOutCompany(company);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 更新公司信息
	 */
	@RequestMapping(value = "updateCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateCompany(@RequestBody OutCompany company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			outCompanyService.updateOutCompany(company);
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("更新失败");
		}
	}

	// 客户对营业额的贡献占比
	@RequestMapping(value = "getTurnoverProportion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getTurnoverGrowth(Integer companyId, Integer year, Integer type, Integer num) {
		ResultMessage result = new ResultMessage();
		try {

			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverProportion",
					outCompanyService.getTurnoverProportion(companyId, year, type, num));
			result.getResultParm().put("outCompany", outCompanyService.getOutCompany());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 删除公司信息
	 */
	@RequestMapping(value = "deleteCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteCompany(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			outCompanyService.deleteOutCompany(companyId);
			result.setResultInfo("删除成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}
}
