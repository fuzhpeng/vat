package com.hydx.vat.service.impl;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.CompanyDao;
import com.hydx.vat.dao.InCompanyDao;
import com.hydx.vat.dao.LargeClassDao;
import com.hydx.vat.dao.ListDataDao;
import com.hydx.vat.dao.OutCompanyDao;
import com.hydx.vat.dao.SmallClassDao;
import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.ListData;
import com.hydx.vat.entity.OutCompany;
import com.hydx.vat.entity.SmallClass;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.tool.ExcelUtil;

@Service("CommodityService")
public class CommodityServiceImpl implements CommodityService {

	@Autowired
	CommodityDao commodityDao;

	@Autowired
	SmallClassDao smallClassDao;

	@Autowired
	LargeClassDao largeClassDao;

	@Autowired
	ListDataDao listDataDao;

	@Autowired
	InCompanyDao inCompanyDao;

	@Autowired
	OutCompanyDao outCompanyDao;

	@Autowired
	CompanyDao companyDao;

	@Override
	public void addCommodity(Commodity commodity) {
		double price = 0;
		double vatMoney = 0;
		double allPrice = 0;
		Commodity saveCommodity = commodityDao.saveAndFlush(commodity);
		System.out.println(commodity);
		System.out.println(commodity.getInCompany());
		System.out.println(commodity.getOutCompany());
		System.out.println(commodity.getListData());
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");// 设置日期格式
		String date = df.format(new Date());// new Date()为获取当前系统时间
		if (commodity.getInCompany().getCompanyId() == null) {
			InCompany InCompany1 = inCompanyDao.save(commodity.getInCompany());
			saveCommodity.setInCompanyId(InCompany1.getCompanyId());
		} else {
			commodity.setInCompanyId(commodity.getInCompany().getCompanyId());
		}
		if (commodity.getOutCompany().getCompanyId() == null) {
			OutCompany outCompany2 = outCompanyDao.save(commodity.getOutCompany());
			saveCommodity.setInCompanyId(outCompany2.getCompanyId());
		} else {
			commodity.setOutCompanyId(commodity.getOutCompany().getCompanyId());
		}

		List<ListData> list = commodity.getListData();
		for (ListData listdata : list) {
			if (commodity.getType() == 0) {
				SmallClass smallClass = smallClassDao.findOne(listdata.getSmallClassId());
				smallClass.setSmallClassRepertory(smallClass.getSmallClassRepertory() + listdata.getCommodityAmount());
			} else {
				SmallClass smallClass = smallClassDao.findOne(listdata.getSmallClassId());
				smallClass.setSmallClassRepertory(smallClass.getSmallClassRepertory() - listdata.getCommodityAmount());
			}
			listdata.setCommodityId(saveCommodity.getCommodityId());
			listdata.setVatdate(date);
			listDataDao.saveAndFlush(listdata);
			price += (listdata.getPrice() * listdata.getCommodityAmount());
			vatMoney += (listdata.getVatMoney() * listdata.getCommodityAmount());
			allPrice += (listdata.getAllPrice() * listdata.getCommodityAmount());
		}
		saveCommodity.setAllPrice(allPrice);
		saveCommodity.setPrice(price);
		saveCommodity.setVatMoney(vatMoney);
		saveCommodity.setVatDate(date);
		commodityDao.saveAndFlush(saveCommodity);
	}

	@Override
	public String addphoto(MultipartFile file, HttpServletRequest request) throws IOException {

		int numble = (int) (Math.random() * 100);

		String originalFilename = file.getOriginalFilename();
		String fileType = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
		String fileName = numble + file.getOriginalFilename();
		String path = request.getSession().getServletContext().getRealPath("/");
		File imagePath = new File(path + "taxsystem/image");
		if (!imagePath.exists())
			imagePath.mkdirs(); // 若文件不存在则创建一个
		File imageFile = new File(imagePath + "/", fileName);

		InputStream in;

		in = file.getInputStream();
		FileOutputStream os = new FileOutputStream(imageFile);
		byte[] buffer = new byte[1024];
		int len = 0;
		while ((len = in.read(buffer)) != -1) {
			os.write(buffer, 0, len);
		}
		os.close();

		return ".\\" + "image" + "\\" + fileName; // 返回相对路径

	}

	@Override
	public Page<Commodity> getCommodity(Pageable pageable, Integer commodityType, Integer CompanyId, String startDate,
			String endDate, String date) {
		Page<Commodity> haha = null;

		if (commodityType == null && startDate == null && endDate == null) {
			System.out.println("1");
			if (date == null) {
				System.out.println("2");
				haha = commodityDao.getAll(pageable, CompanyId);
			} else {
				System.out.println("3");
				haha = commodityDao.getAll(pageable, CompanyId, date);
			}
			setClassAndCompany(haha.getContent());
			return haha;
		} else {
			System.out.println("4");
			if (startDate != null && endDate != null) {
				System.out.println("5");
				if (date == null) {
					System.out.println("6");
					haha = commodityDao.getByCommodityData(pageable, commodityType, CompanyId, startDate, endDate);
				} else {
					System.out.println("7");
					if(commodityType==2){
						System.out.println("13");
						haha=commodityDao.getByCommodityData(pageable, CompanyId, startDate, endDate,date);
					}else{
					haha = commodityDao.getByCommodityData(pageable, commodityType, CompanyId, startDate, endDate,
							date);
				}
				}
			} else {
				System.out.println("8");
				if (date == null) {
					if(commodityType==2){
						System.out.println("12");
						haha=commodityDao.getAllCommodity(pageable, CompanyId);
					}
					else{
					haha = commodityDao.getByCommodityType(pageable, commodityType, CompanyId);}
				} else {
					System.out.println("10");
					System.out.println("=============");
					if(commodityType==2){
						System.out.println("13");
						haha=commodityDao.getAllCommodity(pageable, CompanyId,date);
					}else{
					haha = commodityDao.getByCommodityType(pageable, commodityType, CompanyId, date);
					}
				}
			}
			setClassAndCompany(haha.getContent());
			return haha;
		}
	}

	@Override
	public Commodity getCommodity(String numble) {
		return setClassAndCompany(commodityDao.findByVatNumber(numble));
	}

	@Override
	public Double getAllPrice(Integer companyId, Integer type, String date) {
		System.out.println("companyId" + companyId);
		System.out.println("date" + date);
		if (type == 1)
			if (date == null) {
				return commodityDao.getAllInPrice(companyId);
			} else {
				return commodityDao.getAllInPrice(companyId, date);
			}
		else {
			if (date == null) {
				return commodityDao.getAllOutPrice(companyId);
			} else {
				return commodityDao.getAllOutPrice(companyId, date);
			}
		}

	}

	@Override
	public Double getAllVatMoney(Integer companyId, String date) {
		if (date == null) {
			return commodityDao.getAllVatMoney(companyId);
		} else {
			return commodityDao.getAllVatMoney(companyId, date);
		}
	}

	@Override
	public List<Integer> getAllYear(Integer year) {
		List<Integer>years=commodityDao.getAllYear();
		Integer count=years.size();
		System.out.println(count+"count");
		if (year == 1 && count > 2) {
			return years.subList(count - 3, count);
		}
		if (year == 2 && count > 4) {
			return years.subList(count - 5, count);
		}
		if (year == 3 && count > 9) {
			return years.subList(count - 10, count);
		}
		return years;
	}

	public List<Commodity> setClassAndCompany(List<Commodity> list) {
		if (list.size() != 0) {
			for (Commodity record : list) {
				InCompany inCompany = inCompanyDao.findOne(record.getInCompanyId());
				OutCompany outCompany = outCompanyDao.findOne(record.getOutCompanyId());
				record.setInCompany(inCompany);
				record.setOutCompany(outCompany);
			}
		}
		return list;
	}

	public Commodity setClassAndCompany(Commodity list) {
		InCompany inCompany = inCompanyDao.findOne(list.getInCompanyId());
		OutCompany outCompany = outCompanyDao.findOne(list.getOutCompanyId());
		list.setInCompany(inCompany);
		list.setOutCompany(outCompany);
		List<ListData> listDatas = listDataDao.findByCommodityId(list.getCommodityId());
		list.setListData(listDatas);
		return list;
	}

	@Override
	public List<Double> getInTurnoverByCompanyId(Integer companyId) {
		return commodityDao.getInTurnoverByCompanyId(companyId);
	}

	@Override
	public List<Double> getOutTurnoverByCompanyId(Integer companyId) {
		return commodityDao.getOutTurnoverByCompanyId(companyId);

	}

	@Override
	public List<Double> getAllVatByCompanyId(Integer companyId) {
		return commodityDao.getAllVatByCompanyId(companyId);

	}

	@Override
	public List<List<Double>> getTurnoverGrowth() {
		List<List<Double>> b = new ArrayList<List<Double>>();
		List<Company> list = companyDao.findAll();
		for (Company company : list) {
			List<Double> list3 = new ArrayList<Double>();
			System.out.println("fuhzipeng");
			List<Double> inLists = getInTurnoverByCompanyId(company.getCompanyId());
			List<Double> outLists = getOutTurnoverByCompanyId(company.getCompanyId());
			System.out.println(inLists.size() + "----------------");
			for (int i = 0; i < inLists.size(); i++) {
				Double in = inLists.get(i);
				Double out = outLists.get(i);
				System.out.println(out - in + "========");
				list3.add((out - in));
			}
			System.out.println(list3);
			b.add(list3);
		}
		return b;
	}

	public List<List<Double>> getTurnoverGrowthByMonth(String data) {
		List<List<Double>> b = new ArrayList<List<Double>>();
		List<Company> list = companyDao.findAll();
		for (Company company : list) {
			List<Double> list3 = new ArrayList<Double>();
			List<Double> outLists = commodityDao.getInTurnoverByMonth(company.getCompanyId(), data);
			System.out.println("outLists" + outLists);
			List<Double> inLists = commodityDao.getOutTurnoverByMonth(company.getCompanyId(), data);
			for (int i = 0; i < inLists.size(); i++) {
				Double in = inLists.get(i);
				Double out = outLists.get(i);
				System.out.println(out - in + "========");
				list3.add((out - in));
			}
			System.out.println(list3);
			b.add(list3);
		}
		return b;
	}

	@Override
	public List<List<Double>> getVatGrowth() {
		List<List<Double>> b = new ArrayList<List<Double>>();
		List<Company> list = companyDao.findAll();
		for (Company company : list) {
			List<Double> vats = getAllVatByCompanyId(company.getCompanyId());
			b.add(vats);
		}
		return b;
	}

	public List<List<Double>> getVatGrowthByMonth(String data) {
		List<List<Double>> b = new ArrayList<List<Double>>();
		List<Company> list = companyDao.findAll();
		for (Company company : list) {
			List<Double> vats = commodityDao.getVatByMonth(company.getCompanyId(), data);
			b.add(vats);
		}
		return b;
	}

	public List<Double> getAllVatByCompanyId(Integer companyId, String data) {
		List<Double> vats = commodityDao.getVatByMonth(companyId, data);
		return vats;
	}

	@Override
	public List<Double> getGoodProportion(String data, Integer companyId) {
		List<Double> b = new ArrayList<Double>();
		List<Integer> list = commodityDao.getGoodAllComodityYear(companyId, data);
		System.out.println(list.get(0) + "=======");
		List<SmallClass> smallClassList = smallClassDao.findAll();
		for (SmallClass smallClass : smallClassList) {
			System.out.println(smallClass);
			Double GoodMoney = 0.0;
			for (Integer a : list) {
				System.out.println(a);
				Double price = listDataDao.getGoodProportion(a, smallClass.getSmallClassId());
				if (price != null) {
					GoodMoney = +price;
				} else {
					GoodMoney = +0.0;
				}
				System.out.println(GoodMoney);
			}
			b.add(GoodMoney);
		}
		return b;
	}

	@Override
	public List<Double> getGoodVatProportion(String data, Integer companyId) {
		List<Double> b = new ArrayList<Double>();
		List<Integer> list = commodityDao.getGoodAllComodityYear(companyId, data);
		List<SmallClass> smallClassList = smallClassDao.findAll();
		for (SmallClass smallClass : smallClassList) {
			System.out.println(smallClass);
			Double GoodMoney = 0.0;
			for (Integer a : list) {
				Double price = listDataDao.getGoodVatProportion(a, smallClass.getSmallClassId());
				if (price != null) {
					GoodMoney = +price;
				} else {
					GoodMoney = +0.0;
				}
			}
			b.add(GoodMoney);
		}
		return b;
	}

	@Override
	public List<Double> getTurnoverGrowthByCompany(Integer companyId, Integer year, Integer type) {
		List<Double> list = new ArrayList<Double>();
		if (type == 0) {// 年度统计
			List<Double> Out = getInTurnoverByCompanyId(companyId);
			List<Double> In = getOutTurnoverByCompanyId(companyId);
			System.out.println(In.size());
			if (year > 10) {
				Out = commodityDao.getInTurnoverByCompanyId(companyId, year);
				System.out.println(Out);
			}
			for (Integer i = 0; i < Out.size(); i++) {
				list.add(Out.get(i));
			}
			Integer count = list.size();
			if (year == 0) {
				return list;
			}
			if (year == 1 && count > 2) {
				return list.subList(count - 3, count);
			}
			if (year == 2 && count > 4) {
				return list.subList(count - 5, count);
			}
			if (year == 3 && count > 9) {
				return list.subList(count - 10, count);
			}
		}
		if (type == 1) {// 季度统计
			List<Double> Out = commodityDao.getInTurnoverByQuarter(companyId);
			if (year > 10) {
				Out = commodityDao.getInTurnoverByQuarter(companyId, year);
			}
			for (Integer i = 0; i < Out.size(); i++) {
				list.add(Out.get(i));
			}
			Integer count = list.size();
			if (type == 0) {
				return list;
			}
			if (year == 1 && count > 11) {
				return list.subList(count - 12, count);
			}
			if (year == 2 && count > 19) {
				return list.subList(count - 5, count);
			}
			if (year == 3 && count > 39) {
				return list.subList(count - 10, count);
			}
		}
		if (type == 2) {// 月份统计
			List<Double> Out = commodityDao.getInTurnoverByMonth(companyId);
			List<Double> In = commodityDao.getOutTurnoverByMonth(companyId);
			if (year > 10) {
				Out = commodityDao.getInTurnoverByMonth(companyId, year);
			}
			System.out.println(In.size());
			for (Integer i = 0; i < Out.size(); i++) {
				list.add(Out.get(i));
			}
			Integer count = list.size();
			if (type == 0) {
				return list;
			}
			if (year == 1 && count > 35) {
				return list.subList(count - 36, count);
			}
			if (year == 2 && count > 59) {
				return list.subList(count - 60, count);
			}
			if (year == 3 && count > 119) {
				return list.subList(count - 120, count);
			}
		}
		return list;
	}

	@Override
	public List<Double> getCompanyTurnoverByMonth(Integer companyId, String data) {
		List<Double> list3 = new ArrayList<Double>();
		List<Double> outLists = commodityDao.getInTurnoverByMonth(companyId, data);
		System.out.println(companyId + data);
		List<Double> inLists = commodityDao.getOutTurnoverByMonth(companyId, data);
		System.out.println(inLists);
		for (int i = 0; i < inLists.size(); i++) {
			Double in = inLists.get(i);
			Double out = outLists.get(i);
			System.out.println(out - in + "========");
			list3.add((out - in));
		}
		return list3;
	}

	@Override
	public List<Integer> getMonth(String data) {
		return commodityDao.getMonth(data);
	}

	@Override
	public List<String> getMonthBycompanyId(Integer companyId, String data) {
		// TODO Auto-generated method stub
		return commodityDao.getMonthBycompanyId(companyId, data);
	}

	@Override
	public List<String> getMonthBycompanyId(Integer companyId) {
		return commodityDao.getMonthBycompanyId(44,""+companyId);
	}

	@Override
	public List<String> getQuarter(Integer companyId) {
		// TODO Auto-generated method stub
		return commodityDao.getQuarter(44,companyId);
	}

	@Override
	public List<Double> getVatGrowth(Integer companyId, Integer year, Integer type) {
		List<Double> list = new ArrayList<Double>();
		if (type == 0) {// 年度统计
			list = getAllVatByCompanyId(companyId);
			if (year > 10) {
				list = commodityDao.getAllVatByCompanyId(companyId, year);
			}
			Integer count = list.size();
			if (year == 1 && count > 2) {
				return list.subList(count - 3, count);
			}
			if (year == 2 && count > 4) {
				return list.subList(count - 5, count);
			}
			if (year == 3 && count > 9) {
				return list.subList(count - 10, count);
			}
		}
		if (type == 1) {// 季度统计
			list = commodityDao.getVatByQuarter(companyId);

			Integer count = list.size();
			if (year > 10) {
				list = commodityDao.getVatByQuarter(companyId, year);
			}
			if (year == 0) {
				return list;
			}
			if (year == 1 && count > 11) {
				return list.subList(count - 12, count);
			}
			if (year == 2 && count > 19) {
				return list.subList(count - 5, count);
			}
			if (year == 3 && count > 39) {
				return list.subList(count - 10, count);
			}
		}
		if (type == 2) {// 月份统计
			list = commodityDao.getVatByMonth(companyId);

			Integer count = list.size();
			if (year > 10) {
				list = commodityDao.getVatByMonth(companyId, year);
			}
			if (year == 0) {
				return list;
			}
			if (year == 1 && count > 35) {
				return list.subList(count - 36, count);
			}
			if (year == 2 && count > 59) {
				return list.subList(count - 60, count);
			}
			if (year == 3 && count > 119) {
				return list.subList(count - 120, count);
			}
		}
		return list;
	}

	@Override
	public void addAllCommodity(MultipartFile commodityfile, MultipartFile listfile) throws IOException {
		ArrayList<ArrayList<Object>> result = ExcelUtil.readExcel(commodityfile.getInputStream());
		for (int i = 1; i < result.size(); i++) {
			Commodity commodity = new Commodity(); // 添加发票清单
			for (int j = 0; j < result.get(i).size(); j++) {
				System.out.println(i + "行 " + j + "列  " + result.get(i).get(j).toString());
				if (j == 0) {
					double numble = Double.parseDouble(result.get(i).get(j).toString());
					Integer inCompanyId = (int) numble;
					String vatNumble=""+inCompanyId;
					commodity.setVatNumber(vatNumble);
				}
				if (j == 1) {
					commodity.setVatDate(result.get(i).get(j).toString());
				}
				if (j == 2) {
					double numble = Double.parseDouble(result.get(i).get(j).toString());
					Integer inCompanyId = (int) numble;
					commodity.setInCompanyId(inCompanyId);
				}
				if (j == 3) {
					double numble = Double.parseDouble(result.get(i).get(j).toString());
					Integer outCompanyId = (int) numble;
					commodity.setOutCompanyId(outCompanyId);
				}
				if (j == 4) {
					Double price = Double.parseDouble(result.get(i).get(j).toString());
					commodity.setPrice(price);
				}
				if (j == 5) {
					Double vatprice = Double.parseDouble(result.get(i).get(j).toString());
					commodity.setPrice(vatprice);
				}
				if (j == 6) {
					Double allprice = Double.parseDouble(result.get(i).get(j).toString());
					commodity.setPrice(allprice);
				}
				if (j == 7) {
					commodity.setVatUser(result.get(i).get(j).toString());
				}
				if (j == 8) {
					commodity.setCheckUser(result.get(i).get(j).toString());
				}
				if (j == 9) {
					commodity.setReceiverUser(result.get(i).get(j).toString());
				}
				if (j == 10) {
					double numble = Double.parseDouble(result.get(i).get(j).toString());
					Integer type = (int) numble;
					commodity.setType(type);
				}
				if (j == 11) {
					commodity.setRemarks(result.get(i).get(j).toString());
				}
				if (j == 12) {
					double numble = Double.parseDouble(result.get(i).get(j).toString());
					Integer userId = (int) numble;
					commodity.setUserId(userId);
				}
			}
			commodityDao.saveAndFlush(commodity);
		}
		ArrayList<ArrayList<Object>> listResult = ExcelUtil.readExcel(listfile.getInputStream());
		for (int a = 1; a < listResult.size(); a++) {
			ListData listData = new ListData();
			for (int b = 0; b < listResult.get(a).size(); b++) {
				switch (b) {
				case 0:
					listData.setCommodityName(listResult.get(a).get(b).toString());
					break;
				case 1:
					listData.setCommodityType(listResult.get(a).get(b).toString());
					break;
				case 2:
					listData.setUnit(listResult.get(a).get(b).toString());
					break;
				case 3:
					double numble = Double.parseDouble(listResult.get(a).get(b).toString());
					Integer amount = (int) numble;
					listData.setCommodityAmount(amount);
					break;
				case 4:
					double price = Double.parseDouble(listResult.get(a).get(b).toString());
					listData.setPrice(price);
					break;
				case 5:
					double allPrice = Double.parseDouble(listResult.get(a).get(b).toString());
					listData.setAllPrice(allPrice);
					break;
				case 6:
					double taxRate = Double.parseDouble(listResult.get(a).get(b).toString());
					listData.setTaxRate(taxRate);
					break;
				case 7:
					double vatMoney = Double.parseDouble(listResult.get(a).get(b).toString());
					listData.setVatMoney(vatMoney);
					break;
				case 8:
					double Numble = Double.parseDouble(listResult.get(a).get(b).toString());
					Integer vatNumble = (int) Numble;
					Commodity commodity = commodityDao.findByVatNumber(""+vatNumble);
					System.out.println(vatNumble+"vatNumblevatNumblevatNumblevatNumblevatNumble");
					System.out.println(commodity+"commoditycommoditycommoditycommodity");
					if (commodity == null) {
						listData.setCommodityId(0);
					} else {
						listData.setCommodityId(commodity.getCommodityId());
					}
					break;
				case 9:
					double largeClassId = Double.parseDouble(listResult.get(a).get(b).toString());
					Integer largeclassId = (int) largeClassId;
					listData.setLargeClassId(largeclassId);
					break;
				case 10:
					double smallClassId = Double.parseDouble(listResult.get(a).get(b).toString());
					Integer smallclassId = (int) smallClassId;
					listData.setSmallClassId(smallclassId);
					break;
				case 11:
					listData.setVatdate(result.get(a).get(b).toString());
					break;
				default:
					break;
				}
			}
			listDataDao.saveAndFlush(listData);
		}
	}

	@Override
	public List<List<Double>> getTurnoverGrowthByCompany(Integer year, Integer type) {
		List<List<Double>>allTurnoverGrowth=new ArrayList<List<Double>>();
		List<Company>companys=companyDao.findAll();
		for(Company company:companys){
			List<Double>turnoverGrowth=getTurnoverGrowthByCompany(company.getCompanyId(),year,type);
			allTurnoverGrowth.add(turnoverGrowth);
		}
		return allTurnoverGrowth;
	}

	@Override
	public List<String> getQuarter() {
		List<Company>companys=companyDao.findAll();
		Company company=companys.get(0); 
		System.out.println(company);
		return commodityDao.getQuarter(company.getCompanyId());
	}

	@Override
	public List<String> getMonthBycompanyId() {
		List<Company>companys=companyDao.findAll();
		Company company=companys.get(0);
		System.out.println(company);
		return commodityDao.getMonthBycompanyId(company.getCompanyId());
	}

	@Override
	public List<List<Double>> getVatGrowth(Integer year, Integer type) {
		List<List<Double>>allTurnoverGrowth=new ArrayList<List<Double>>();
		List<Company>companys=companyDao.findAll();
		for(Company company:companys){
			List<Double>turnoverGrowth=getVatGrowth(company.getCompanyId(),year,type);
			allTurnoverGrowth.add(turnoverGrowth);
		}
		return allTurnoverGrowth;
	}

	@Override
	public List<Double> getAllTurnover(Integer year, Integer type) {
		List<Double>allTurnover=new ArrayList<Double>();
		List<Company>companys=companyDao.findAll();
		for(Company company:companys){
			List<Double>turnoverGrowth=getTurnoverGrowthByCompany(company.getCompanyId(),year,type);
			Double money=0.0;
			for(Double a:turnoverGrowth){
				money+=a;
			}
			allTurnover.add(money);
		}
		return allTurnover;
	}

	@Override
	public List<Double> getAllVat(Integer year, Integer type) {
		List<Double>allVatTurnover=new ArrayList<Double>();
		List<Company>companys=companyDao.findAll();
		for(Company company:companys){
			List<Double>turnoverGrowth=getVatGrowth(company.getCompanyId(),year,type);
			Double money=0.0;
			for(Double a:turnoverGrowth){
				money+=a;
			}
			allVatTurnover.add(money);
		}
		return allVatTurnover;
	}

	@Override
	public void delCommodity(Integer commodityId) {
		// TODO Auto-generated method stub
		commodityDao.delete(commodityId);
	}

	@Override
	public Commodity getCommodity(Integer companyId) {
		// TODO Auto-generated method stub
		Commodity commodity=commodityDao.findOne(companyId);
		setClassAndCompany(commodity);
		return commodity;
	}

	@Override
	public List<Integer> getAllYear() {
		// TODO Auto-generated method stub
		return commodityDao.getAllYear();
	}

	@Override
	public List<String> getMonthBycompanyId(Integer companyId, Integer year) {
		// TODO Auto-generated method stub
		if(year>4){
			return commodityDao.getMonthBycompanyId(companyId, ""+year);
		}else{
		return commodityDao.getMonthBycompanyId(companyId);
		}
	}

	@Override
	public List<String> getQuarter(Integer companyId, Integer year) {
		if(year>4){
			return commodityDao.getQuarter(companyId,year);
		}else{
		return commodityDao.getQuarter(companyId);
		}
	}

	@Override
	public Page<Commodity> getCommodityByNum(Pageable pageable, String vatNumber) {
		Page<Commodity> commoditys=commodityDao.getCommodityByNum(pageable,vatNumber);
		setClassAndCompany(commoditys.getContent());
		return commoditys;
	}

}
