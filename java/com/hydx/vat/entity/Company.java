package com.hydx.vat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Company {

	private int companyId;
	private String companyName; // 公司名称
	private String companyAddress;// 公司详细地址
	private String companyRegion;// 公司区域
	private String companyUser;// 公司负责人
	private String companyPhone;// 公司电话

	@Id
	@GeneratedValue
	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyRegion() {
		return companyRegion;
	}

	public void setCompanyRegion(String companyRegion) {
		this.companyRegion = companyRegion;
	}

	public String getCompanyUser() {
		return companyUser;
	}

	public void setCompanyUser(String companyUser) {
		this.companyUser = companyUser;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public Company(int companyId, String companyName, String companyAddress, String companyRegion) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyAddress = companyAddress;
		this.companyRegion = companyRegion;
	}

	public Company() {
		super();
	}

}
