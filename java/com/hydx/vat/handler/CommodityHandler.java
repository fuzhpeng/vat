package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.ListData;
import com.hydx.vat.entity.OutCompany;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.CompanyService;
import com.hydx.vat.service.LargeClassService;
import com.hydx.vat.service.SmallClassService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/commodityHandler")
public class CommodityHandler {

	@Autowired
	private CommodityService commodityService;
	@Autowired
	private SmallClassService smallClassService;
	@Autowired
	private LargeClassService largeClassService;
	@Autowired
	private CompanyService companyService;

	// 获取所有增值税统计金额
	@RequestMapping(value = "getCommodityMoney", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getCommodityMoney(Integer companyId, String year) {
		ResultMessage result = new ResultMessage();
		try {
			result.setResultInfo("查询成功");
			result.getResultParm().put("inMoney", commodityService.getAllPrice(companyId, 0, year));
			result.getResultParm().put("outMoney", commodityService.getAllPrice(companyId, 1, year));
			result.getResultParm().put("allVat", commodityService.getAllVatMoney(companyId, year));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取增值税信息
	@RequestMapping(value = "getCommodity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getCommodity(Integer commodityId) {
		ResultMessage result = new ResultMessage();

		try {
			Commodity commodity = commodityService.getCommodity(commodityId);
			result.setResultInfo("查询成功");
			result.getResultParm().put("vat", commodity);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取增值税信息
		@RequestMapping(value = "getCommodityByNum", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<ResultMessage> getCommodityByNum(@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
				@RequestParam(value = "size", defaultValue = "5") Integer size,String date,String vatNumber) {
			ResultMessage result = new ResultMessage();
			Pageable pageable = new PageRequest(currentPage, size);
			try {
				Page<Commodity> page = commodityService.getCommodityByNum(pageable,vatNumber);
				System.out.println(page.getContent());
				result.setResultInfo("查询成功");
				result.getResultParm().put("list", page.getContent());
				result.getResultParm().put("totalPages", page.getTotalPages());
				result.getResultParm().put("currentPage", page.getNumber());
				return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
			} catch (Exception e) {
				e.printStackTrace();
				throw new SecurityException("查询失败");
			}
		}
	
	// 删除增值税信息
	@RequestMapping(value = "delCommodity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> delCommodity(Integer commodityId) {
		ResultMessage result = new ResultMessage();

		try {
			commodityService.delCommodity(commodityId);
			result.setResultInfo("删除成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}

	// 获取所有增值税信息
	@RequestMapping(value = "getAllCommodity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllCommodity(
			@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
			@RequestParam(value = "size", defaultValue = "5") Integer size, Integer commodityType, Integer companyId,
			String startDate, String endDate, String date) {
		ResultMessage result = new ResultMessage();
		Pageable pageable = new PageRequest(currentPage, size);
		try {
			Page<Commodity> page = commodityService.getCommodity(pageable, commodityType, companyId, startDate, endDate,
					date);
			System.out.println(page.getContent());
			result.setResultInfo("查询成功");
			result.getResultParm().put("list", page.getContent());
			result.getResultParm().put("totalPages", page.getTotalPages());
			result.getResultParm().put("currentPage", page.getNumber());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 添加增值税信息
	@RequestMapping(value = "addCommodity", method = RequestMethod.POST)
	public ResponseEntity<ResultMessage> addCommodity(@RequestBody Commodity commodity) {
		ResultMessage result = new ResultMessage();
		try {
			commodityService.addCommodity(commodity);
			result.setResultInfo("添加成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	// 上传图片
	@RequestMapping(value = "addphoto")
	public ResponseEntity<ResultMessage> addphoto(@RequestParam("file") MultipartFile file,
			HttpServletRequest request) {
		ResultMessage result = new ResultMessage();
		try {
			System.out.println(file);
			String address = commodityService.addphoto(file, request);
			result.setResultInfo("添加成功");
			result.getResultParm().put("address", address);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	// 批量添加信息
	@RequestMapping(value = "addAllCommodity")
	public ResponseEntity<ResultMessage> addAllCommodity(@RequestParam("commodityfile") MultipartFile commodityfile,
			@RequestParam("listfile") MultipartFile listfile, HttpServletRequest request) {
		ResultMessage result = new ResultMessage();
		try {
			commodityService.addAllCommodity(commodityfile, listfile);
			result.setResultInfo("添加成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	// 获取所有年份
	@RequestMapping(value = "getAllYear", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllYear() {
		ResultMessage result = new ResultMessage();
		try {
			result.getResultParm().put("year", commodityService.getAllYear());
			result.setResultInfo("查询成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取所有公司年度营业额增长
	@RequestMapping(value = "getAllTurnoverGrowth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllTurnoverGrowth(Integer year, Integer type) {
		ResultMessage result = new ResultMessage();
		try {
			if (type == 0) {
				result.getResultParm().put("time", commodityService.getAllYear(year));
			}
			if (type == 1) {
				result.getResultParm().put("time", commodityService.getQuarter(year));
			}
			if (type == 2) {
				result.getResultParm().put("time", commodityService.getMonthBycompanyId(year));
			}
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverGrowth", commodityService.getTurnoverGrowthByCompany(year, type));
			result.getResultParm().put("company", list);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取所有公司营业额和税额占比
	@RequestMapping(value = "getAllTurnover", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllTurnover(Integer year, Integer type) {
		ResultMessage result = new ResultMessage();
		try {

			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("profit", commodityService.getAllTurnover(year, type));
			result.getResultParm().put("vat", commodityService.getAllVat(year, type));
			result.getResultParm().put("company", list);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取公司年度营业额增长
	@RequestMapping(value = "getTurnoverGrowthByCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getTurnoverGrowthByCompany(Integer companyId, Integer year, Integer type) {
		ResultMessage result = new ResultMessage();
		try {
			if (type == 0) {
				result.getResultParm().put("time", commodityService.getAllYear(year));
			}
			if (type == 1) {
				result.getResultParm().put("time", commodityService.getQuarter(companyId, year));
			}
			if (type == 2) {
				result.getResultParm().put("time", commodityService.getMonthBycompanyId(companyId, year));
			}
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverGrowth",
					commodityService.getTurnoverGrowthByCompany(companyId, year, type));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取公司年度税率增长
	@RequestMapping(value = "getVatGrowth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getVatGrowth(Integer companyId, Integer year, Integer type) {
		ResultMessage result = new ResultMessage();
		try {
			if (type == 0) {
				result.getResultParm().put("time", commodityService.getAllYear(year));
			}
			if (type == 1) {
				result.getResultParm().put("time", commodityService.getQuarter(companyId, year));
			}
			if (type == 2) {
				result.getResultParm().put("time", commodityService.getMonthBycompanyId(companyId, year));
			}
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverGrowth", commodityService.getVatGrowth(companyId, year, type));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取所有公司年度税率增长
	@RequestMapping(value = "getAllVatGrowth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllVatGrowth(Integer year, Integer type) {
		ResultMessage result = new ResultMessage();
		try {
			if (type == 0) {
				result.getResultParm().put("time", commodityService.getAllYear(year));
			}
			if (type == 1) {
				result.getResultParm().put("time", commodityService.getQuarter(year));
			}
			if (type == 2) {
				result.getResultParm().put("time", commodityService.getMonthBycompanyId(year));
			}
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverGrowth", commodityService.getVatGrowth(year, type));
			result.getResultParm().put("company", list);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取月度营业额增长
	@RequestMapping(value = "getInTurnoverByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInTurnoverByMonth(String data) {
		ResultMessage result = new ResultMessage();
		try {
			result.getResultParm().put("company", companyService.getCommodity());
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("InTurnoverByMonth", commodityService.getTurnoverGrowthByMonth(data));
			result.getResultParm().put("Month", commodityService.getMonth(data));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 月度税额增长图表
	@RequestMapping(value = "getVatGrowthByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getVatGrowthByMonth(Integer companyId, String data) {
		ResultMessage result = new ResultMessage();
		try {
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("vatByMonth", commodityService.getAllVatByCompanyId(companyId, data));
			result.getResultParm().put("Month", commodityService.getMonth(data));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取公司月度营业额增长
	@RequestMapping(value = "getCompanyTurnoverByMonth", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getCompanyTurnoverByMonth(Integer companyId, String data) {
		ResultMessage result = new ResultMessage();
		try {
			result.getResultParm().put("Month", commodityService.getMonthBycompanyId(companyId, data));
			List<Company> list = companyService.getCommodity();
			result.setResultInfo("查询成功");
			result.getResultParm().put("InTurnoverByMonth",
					commodityService.getCompanyTurnoverByMonth(companyId, data));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取所有商品的占营业额
	@RequestMapping(value = "getGoodProportion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getGoodProportion(String data, Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			List<Company> list = companyService.getCommodity();
			result.getResultParm().put("GoodProportion", commodityService.getGoodProportion(data, companyId));
			result.getResultParm().put("Good", smallClassService.getSmallClass());
			result.setResultInfo("查询成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 获取所有商品的占税率
	@RequestMapping(value = "getGoodVatProportion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getGoodVatProportion(String data, Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			List<Company> list = companyService.getCommodity();
			result.getResultParm().put("GoodProportion", commodityService.getGoodVatProportion(data, companyId));
			result.getResultParm().put("Good", smallClassService.getSmallClass());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

}
