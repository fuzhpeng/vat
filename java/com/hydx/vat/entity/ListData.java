package com.hydx.vat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

//清单表
@Entity
public class ListData {

	private Integer listId;
	private Integer largeClassId;//大类别
	private Integer smallClassId;//小类别
	private String commodityName; //服务名称
	private String commodityType;//规格型号
	private String unit;//单位
	private Integer commodityAmount; //数量
	private double price; //单价
	private double allPrice; //金额
	private double taxRate; //税率
	private double vatMoney; //税额
	private Integer commodityId;
	private String vatdate; //日期

	@Id
	@GeneratedValue
	public Integer getListId() {
		return listId;
	}

	public void setListId(Integer listId) {
		this.listId = listId;
	}

	public Integer getLargeClassId() {
		return largeClassId;
	}

	public void setLargeClassId(Integer largeClassId) {
		this.largeClassId = largeClassId;
	}

	public Integer getSmallClassId() {
		return smallClassId;
	}

	public void setSmallClassId(Integer smallClassId) {
		this.smallClassId = smallClassId;
	}

	public String getCommodityName() {
		return commodityName;
	}

	public void setCommodityName(String commodityName) {
		this.commodityName = commodityName;
	}

	public String getCommodityType() {
		return commodityType;
	}

	public void setCommodityType(String commodityType) {
		this.commodityType = commodityType;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getCommodityAmount() {
		return commodityAmount;
	}

	public void setCommodityAmount(Integer commodityAmount) {
		this.commodityAmount = commodityAmount;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getAllPrice() {
		return allPrice;
	}

	public void setAllPrice(double allPrice) {
		this.allPrice = allPrice;
	}

	public double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(double taxRate) {
		this.taxRate = taxRate;
	}

	public double getVatMoney() {
		return vatMoney;
	}

	public void setVatMoney(double vatMoney) {
		this.vatMoney = vatMoney;
	}

	public Integer getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(Integer commodityId) {
		this.commodityId = commodityId;
	}

	public ListData() {
		super();
	}


	public String getVatdate() {
		return vatdate;
	}

	public void setVatdate(String vatdate) {
		this.vatdate = vatdate;
	}

	public ListData(Integer listId, Integer largeClassId, Integer smallClassId, String commodityName,
			String commodityType, String unit, Integer commodityAmount, double price, double allPrice, double taxRate,
			double vatMoney, Integer commodityId) {
		super();
		this.listId = listId;
		this.largeClassId = largeClassId;
		this.smallClassId = smallClassId;
		this.commodityName = commodityName;
		this.commodityType = commodityType;
		this.unit = unit;
		this.commodityAmount = commodityAmount;
		this.price = price;
		this.allPrice = allPrice;
		this.taxRate = taxRate;
		this.vatMoney = vatMoney;
		this.commodityId = commodityId;
	}

}
