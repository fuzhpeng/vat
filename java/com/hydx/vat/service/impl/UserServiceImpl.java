package com.hydx.vat.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CompanyDao;
import com.hydx.vat.dao.RoleDao;
import com.hydx.vat.dao.UserDao;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.Role;
import com.hydx.vat.entity.User;
import com.hydx.vat.service.UserService;
import com.hydx.vat.tool.MD5Util;

@Service("UserService")
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private CompanyDao companyDao;

	@Autowired
	private RoleDao roleDao;

	@Override
	public void AddUser(User user) throws Exception {
		String acount = user.getAccount();
		User exitUser = userDao.findByAccount(acount);
		if (exitUser == null) {
			Company company = companyDao.findOne(user.getCompanyId());
			user.setCompanyName(company.getCompanyName());
			userDao.save(user);
			String MD5Password = MD5Util.md5(user.getPassword());
			user.setPassword(MD5Password);
		} else {
			throw new Exception("账号已经存在");
		}
	}

	@Override
	public Page<User> getAllUser(Pageable pageable) {
		return userDao.findAll(pageable);
	}

	@Override
	public User getUser(Integer id) {
		return userDao.findOne(id);
	}

	@Override
	public void UpdateUser(User user) {
		userDao.saveAndFlush(user);
	}

	@Override
	public void DeteleUser(Integer id) {
		userDao.delete(id);
	}

	@Override
	public void setType(Integer userId, Integer type) {
		User user = userDao.findOne(userId);
		user.setType(type);
		userDao.saveAndFlush(user);
	}

	/**
	 * 根据系统用户名获取用户信息
	 */
	public User findOneByName(String username) {
		return userDao.findByAccount(username);
	}

	/**
	 * 根据系统用户Id获取用户权限
	 * 
	 * @author 宋
	 */
	public List<String> findPrivilegeCode(int id) {
		return userDao.findPrivilegeCode(id);
	}

	/**
	 * 根据系统用户id获取用户角色.
	 * 
	 * @author 宋
	 */
	public String findRoleName(int id) {
		return userDao.findRoleName(id);
	}

	/**
	 * 删除用户
	 * 
	 * @author Kellan
	 * @param user
	 */
	public void delete(User user) {
		User User = userDao.findOne(user.getUserId());
		if (User == null) {
			throw new SecurityException("该用户不存在");
		}
		userDao.delete(user);
		User = userDao.findOne(user.getUserId());
		if (User != null) {
			throw new SecurityException("删除失败");
		}
	}

	/**
	 * 修改用户密码
	 * 
	 * @author Kellan
	 * @param username
	 * @param oldPassword
	 * @param newPassword
	 */
	public void updatePassword(String username, String oldPassword, String newPassword) {
		String md5OldPassword = MD5Util.md5(oldPassword);
		User dbUser = userDao.findByAccount(username);
		if (!dbUser.getPassword().equals(md5OldPassword)) {
			throw new SecurityException("旧密码错误");
		}
		String md5NewPassword = MD5Util.md5(newPassword);
		dbUser.setPassword(md5NewPassword);
		userDao.saveAndFlush(dbUser);
	}

	/**
	 * 修改用户角色
	 * 
	 * @author Kellan
	 * @param user
	 */
	public void updateUserRole(User user) {
		Role role = roleDao.findOne(user.getRoleId());
		if (role == null) {
			throw new SecurityException("不存在该角色");
		}
		User dbUser = userDao.findOne(user.getUserId());
		if (dbUser == null) {
			throw new SecurityException("不存在该用户");
		}
		dbUser.setRoleId(user.getRoleId());
		userDao.saveAndFlush(dbUser);
	}

	/**
	 * 获取所有系统用户
	 * 
	 * @author Kellan
	 * @return
	 */
	public List<User> getAllUser() {
		List<Role> roleList = roleDao.findAll();
		List<User> userList = userDao.findAll();
		for (User user : userList) {
			for (Role role : roleList) {
				if (user.getRoleId().equals(role.getId())) {
					user.setRolename(role.getRolename());
				}
			}
		}
		return userList;
	}

	@Override
	public String login(String acount, String password) throws Exception {
		User user = userDao.findByAccount(acount);
		if (user != null && MD5Util.md5(password) == user.getPassword()) {
			return "success";
		}
		throw new Exception("密码错误");
	}

}
