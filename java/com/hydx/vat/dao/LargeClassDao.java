package com.hydx.vat.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hydx.vat.entity.LargeClass;

public interface LargeClassDao extends JpaRepository<LargeClass,Integer> {

}
