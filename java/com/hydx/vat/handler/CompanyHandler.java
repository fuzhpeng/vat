package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.service.CompanyService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/companyHandler")
public class CompanyHandler {
	@Autowired
	private CompanyService companyService;
	
	
	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getCommodity(
			@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
			@RequestParam(value = "size", defaultValue = "5") Integer size) {
		ResultMessage result = new ResultMessage();
		Pageable pageable = new PageRequest(currentPage, size);
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			Page<Company> page= companyService.getCommodity(pageable);
			result.setResultInfo("查询成功");
			result.getResultParm().put("list", page.getContent());
			result.getResultParm().put("totalPages", page.getTotalPages());
			result.getResultParm().put("currentPage", page.getNumber());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}
	
	
	
	/*
	 * 添加公司信息
	 */
	@RequestMapping(value = "addCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addCommodity(@RequestBody Company company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			System.out.println(company);
			companyService.addCommodity(company);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}
	
	/*
	 * 更新公司信息
	 */
	@RequestMapping(value = "updateCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateCompany(@RequestBody Company company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			companyService.updateCommodity(company);
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("更新失败");
		}
	}
	
	/*
	 * 删除公司信息
	 */
	@RequestMapping(value = "deleteCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteCompany(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			companyService.deleteCommodity(companyId);
			result.setResultInfo("删除成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}
}
