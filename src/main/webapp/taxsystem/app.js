const path = require('path');
const echarts = require('echarts');

const express = require('express');
const app = new express();
const port = process.env.PORT || 3500;

app.use(express.static('public'));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '', 'index.html'));
});
app.get('/login', (req, res) => {
  res.sendFile(path.join(__dirname, '', 'login.html'));
});
app.get('/register', (req, res) => {
  res.sendFile(path.join(__dirname, '', 'register.html'));
});
app.post('/post/:name', (req, res) => {
  const jsonFileName = req.params.name;
  const jsonFilePath = './public/json/' + jsonFileName + '.json';
  res.send(require(jsonFilePath));
});
app.post('/loginURL', (req, res) => {
  res.redirect('/');
});
app.listen(port, (error) => {
  if (error) {
    console.error(error);
  } else {
    console.info('==> Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port);
  }
});
