 package com.hydx.vat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hydx.vat.entity.SmallClass;

public interface SmallClassDao extends JpaRepository<SmallClass,Integer>{

	List<SmallClass> findByLargeClassId(Integer largeClassId);

}
