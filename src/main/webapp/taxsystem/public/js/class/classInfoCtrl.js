tax.controller('classInfoCtrl',function($scope,$http){

  $scope.fixedNum = function(num){
		return (num*100).toFixed(0);
	}

	/****** 获取商品大类别表start ********/
  $scope.getClass = function() {
    $http.get(getLargeClassURL)
  		.success(function(res){
        if (res.serviceResult) {
          $scope.classes=res.resultParm.LargeClass;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
 /****** 获取商品大类别表end ********/


 /********* 删除商品大类别start  *************/
 $scope.showModal = false;
 $scope.showDelModel = function(id){
   $scope.showModal = true;
   $scope.deleteId = id;
 }
 $scope.closeModel = function(){
   $scope.showModal = false;
 }
 $scope.deleteGoods = function(){
   $http.get(delLargeClassURL,{
     params:{largeClassId:$scope.deleteId}
   })
   .success(function(response){
     alertMes(response.serviceResult,response.resultInfo);
     $scope.showModal = false;
     if (response.serviceResult) {
       $scope.getClass();
     }
   }).error(function(){
     alertMes('noHttp',null);
     $scope.showModal = false;
   });
 }
 /********* 删除商品大类别end  *************/


  /****** 修改商品大类别start *********/
  $scope.updateGoods = function(index,id){
		$scope.classes[index].editActive = !$scope.classes[index].editActive;
	}
	$scope.sureUpdateGoods = function(index,id){
		$http.post(updateLargeClassURL,{
			largeClassId:id,
			largeClassName:$scope.classes[index].newLargeClassName,
			largeClassVat:($scope.classes[index].newLargeClassVat/100).toFixed(3)
		})
		.success(function(response){
			alertMes(response.serviceResult,response.resultInfo);
      if (response.serviceResult) {
        $scope.getClass();
      }
		}).error(function(){
			alertMes('noHttp',null);
		});
	}
  /****** 修改商品大类别end *********/


  /****** 新增商品类别start *********/
  $scope.newLargeClass = {};
	$scope.addLargeClass = function(){
		$scope.showAddClass = !$scope.showAddClass;
	}
	$scope.sureAddClass = function(){
		$http.post(addLargeClassURL,$scope.newLargeClass)
			.success(function(response){
				alertMes(response.serviceResult,response.resultInfo);
				$scope.showAddClass = !$scope.showAddClass;
        $scope.getRecord();
			}).error(function(){
				alertMes('noHttp',null);
		});
	}
  /****** 新增商品类别end *********/


  $scope.getClass();
});
