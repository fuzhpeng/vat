const PREV = "json/";
const POST = "/post/post";
const DEL = "json/post.json";
/*********** 分公司模块  **************/
const addCompanyURL = POST;//添加分公司
const getCompanyURL = PREV + "companyHandler/getCompany.json";//获取所有公司（Get 请求  currentPage 页码 size 一页展示的数量）
const updateCompanyURL = POST;//更新公司信息
const delCompanyURL = "json/post.json";//删除公司信息（GET请求 companyId ）


/*********** 大类别模块  **************/
const addLargeClassURL = POST;//添加大类别信息
const getLargeClassURL = PREV + "largeClassHandler/getlargeClass.json";//获取所有大类别信息
const updateLargeClassURL = POST;//更新大类别信息
const delLargeClassURL = "json/post.json";//删除大类别信息（GET请求 largeClassId ）


/*********** 小类别模块  **************/
const addSmallClassURL = POST;//添加小类别信息
const getSmallClassURL = PREV + "smallClassHandler/getSmallClass.json";//获取所有小类别信息
const updateSmallClassURL = POST;//更新小类别信息
const delSmallClassURL = "json/post.json";//删除小类别信息（GET请求 smallClassId ）


/*********** 用户管理模块  **************/
const addUserURL = POST;//用户注册
const delUserURL = "json/post.json";//删除用户（GET请求，userId）
const getAllUserURL = PREV + "userHandler/getAllUser.json";//获取所有用户（GET请求，currentPage=0&size=5）
const getUserURL = PREV + "userHandler/getUser.json";//获取具体用户信息（GET请求 userId）
const updateUserURL = POST;//修改用户


/*********** 发票模块  **************/
const addCommodityURL = POST;//添加发票
const getAllYearURL = PREV + "commodityHandler/getAllYear.json";//获取所有年份
const addPhotoURL = "/post/addPhoto";//上传发票图片
const getAllCommodityURL = PREV + "commodityHandler/getAllCommodity.json";//获取所有发票（GET请求 companyId=85&&date=2017&commodityType=1&startDate=2017&endDate=2018）
const excelURL = POST;
      //  companyId 分公司id   date 年份 可以为空   commodityType 进出发票 0为进 1为销
const delCommodityURL = POST;//删除发票
const getCommodityURL = PREV + "commodityHandler/getCommodity.json";//通过发票ID获取具体发票(GET请求，commodityId)
const getCommodityByNumURL = PREV + "commodityHandler/getAllCommodity.json";//通过发票代码获取具体发票(GET请求，vatNumber)
const updateCommodityURL = PREV + "commodityHandler/updateCommodity.json";//修改发票
const getInvoiceMoneyURL = PREV + "commodityHandler/getCommodity.json";


/*********** 统计模块  **************/
//按公司统计
  //折线图
  const lineP_URL = PREV + "dataStatic/lineProfit.json";//营业额
  const lineV_URL = PREV + "dataStatic/lineVat.json";//税额
  //柱状图
  const bar_URL = PREV + "dataStatic/bar.json";
  //饼图
  const pipeBP_URL = PREV + "dataStatic/pipeClassBP.json";//大分类营业额
  const pipeBV_URL = PREV + "dataStatic/pipeClassBV.json";//大分类税额
  const pipeSP_URL = PREV + "dataStatic/pipeClassSP.json";//小分类营业额
  const pipeSV_URL = PREV + "dataStatic/pipeClassSV.json";//小分类税额
  const pipeClient_URL = PREV + "dataStatic/pipeClient.json";//客户贡献饼图
  const pipeProvider_URL = PREV + "dataStatic/pipeProvider.json";//供货商贡献饼图
  //地图
  const mapClient_URL = PREV + "dataStatic/mapClient.json";//客户
  const mapProvider_URL = PREV + "dataStatic/mapProvider.json";//供货商
//按全部公司统计
  //折线图
  const lineP_ALL_URL = PREV + "dataStaticAll/lineProfit.json";//营业额
  const lineV_ALL_URL = PREV + "dataStaticAll/lineVat.json";//税额
  //柱状图
  //饼图
  const pipe_ALL_URL = PREV + "dataStaticAll/pipe.json";
  const pipeClass_ALL_URL = PREV + "dataStaticAll/pipeClass.json";
  //地图
  const mapCP_ALL_URL = PREV + "dataStaticAll/map.json";//客户和供货商

/*********** 供应商模块  **************/
const getProviderURL = PREV + "inOutCompany/getAllInCompany.json";//获取所有客户/供应商（Get 请求  currentPage 页码 size 一页展示的数量）
const delProviderURL = DEL;
const updateProviderURL = POST;
const getProviderByIdURL = PREV + "inOutCompany/getInCompanyById.json";
const getProviderByNameURL = PREV + "inOutCompany/getInCompanyByName.json";//get请求 参数InCompanyName
const addProviderURL = POST;


/*********** 客户模块 *************/
const getClientURL = PREV + "inOutCompany/getAllInCompany.json";//获取所有客户/供应商（Get 请求  currentPage 页码 size 一页展示的数量）
const delClientURL = DEL;
const updateClientURL = POST;
const getClientByIdURL = PREV + "inOutCompany/getInCompanyById.json";
const getClientByNameURL = PREV + "inOutCompany/getInCompanyByName.json";
const addClientURL = POST;
