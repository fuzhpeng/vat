package com.hydx.vat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hydx.vat.entity.User;

public interface UserDao extends JpaRepository<User, Integer>{

	User findByAccount(String acount);
	
	@Query("select p.privilegeCode from  Privilege p, RolePrivilege rp,User u where u.id = ?1 and rp.roleId = u.roleId and p.id = rp.privilegeId")
	List<String> findPrivilegeCode(Integer userId);

	@Query("select r.rolename from Role r ,User u where u.id = ?1 and u.roleId = r.id")
	String findRoleName(Integer userId);

	/**
	 * 根据角色id查询用户
	 * @author Kellan
	 * @param id
	 * @return
	 */
	@Query("select u from User u where u.roleId = ?1")
	List<User> fingUserByRole(Integer roleId);
}
