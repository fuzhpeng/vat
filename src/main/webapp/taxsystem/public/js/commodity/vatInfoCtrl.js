tax.controller('vatInfoCtrl',function($scope,$http,$stateParams){
  /******  根据发票ID获取发票内容start ***********/
    $scope.getVat = function(id){
        $http.get(getCommodityURL,{
          params:{
            commodityId:id
          }
        })
        .success(function(res){
          $scope.vat = res.resultParm.vat;
          $scope.lists = res.resultParm.vat.listData;
        })
        .error(function(){
          alertMes("noHttp",null);
        });
    }
  /******  根据发票ID获取发票内容end ***********/

  $scope.getVat($stateParams.commodityId);
});
