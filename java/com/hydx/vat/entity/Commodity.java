package com.hydx.vat.entity;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * @author 知鹏
 * @date2017年5月13日
 * @param Commodity.java
 * @returnCommodity
 */
@Entity
public class Commodity {

	private int commodityId;// 发票id
	private String vatNumber;
	private Integer inCompanyId;
	private Integer outCompanyId;
	private String vatDate;
	private String remarks;
	private String vatUser; // 开票人
	private String checkUser;// 复核人
	private String receiverUser;// 收票人
	private Integer userId;
	private User user;
	private InCompany inCompany;
	private OutCompany outCompany;
	private List<ListData> listData;
	private String photoAdress; //图片地址
	private Integer type;
	private double price;
	private double vatMoney;
	private double allPrice;

	@Id
	@GeneratedValue
	public int getCommodityId() {
		return commodityId;
	}

	public void setCommodityId(int commodityId) {
		this.commodityId = commodityId;
	}

	public Integer getInCompanyId() {
		return inCompanyId;
	}

	public void setInCompanyId(Integer inCompanyId) {
		this.inCompanyId = inCompanyId;
	}

	public Integer getOutCompanyId() {
		return outCompanyId;
	}

	public void setOutCompanyId(Integer outCompanyId) {
		this.outCompanyId = outCompanyId;
	}

	@Transient
	public OutCompany getOutCompany() {
		return outCompany;
	}

	public String getVatDate() {
		return vatDate;
	}

	public void setVatDate(String vatDate) {
		this.vatDate = vatDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getVatUser() {
		return vatUser;
	}

	public void setVatUser(String vatUser) {
		this.vatUser = vatUser;
	}

	@Transient
	public InCompany getInCompany() {
		return inCompany;
	}

	public void setInCompany(InCompany integer) {
		this.inCompany = integer;
	}

	@Transient
	public void setOutCompany(OutCompany outCompany) {
		this.outCompany = outCompany;
	}

	

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Transient
	public User getUser() {
		return user;
	}

	public String getCheckUser() {
		return checkUser;
	}

	public void setCheckUser(String checkUser) {
		this.checkUser = checkUser;
	}

	public String getReceiverUser() {
		return receiverUser;
	}

	public void setReceiverUser(String receiverUser) {
		this.receiverUser = receiverUser;
	}

	@Transient
	public List<ListData> getListData() {
		return listData;
	}

	public void setListData(List<ListData> listData) {
		this.listData = listData;
	}

	public String getPhotoAdress() {
		return photoAdress;
	}

	public void setPhotoAdress(String photoAdress) {
		this.photoAdress = photoAdress;
	}



	public String getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	@Transient
	public void setUser(User user) {
		this.user = user;
	}

	public Commodity() {
		super();
	}

	
	
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getVatMoney() {
		return vatMoney;
	}

	public void setVatMoney(double vatMoney) {
		this.vatMoney = vatMoney;
	}

	public double getAllPrice() {
		return allPrice;
	}

	public void setAllPrice(double allPrice) {
		this.allPrice = allPrice;
	}

	public Commodity(int commodityId, Integer inCompanyId, Integer outCompanyId, Integer companyId, String vatDate,
			String remarks, String vatUser, String checkUser, String receiverUser, String userId, User user,
			InCompany inCompany, OutCompany outCompany, List<ListData> listData, String photoAdress, Integer type) {
		super();
		this.commodityId = commodityId;
		this.inCompanyId = inCompanyId;
		this.outCompanyId = outCompanyId;
		this.vatDate = vatDate;
		this.remarks = remarks;
		this.vatUser = vatUser;
		this.checkUser = checkUser;
		this.receiverUser = receiverUser;
		this.user = user;
		this.inCompany = inCompany;
		this.outCompany = outCompany;
		this.listData = listData;
		this.photoAdress = photoAdress;
		this.type = type;
	}

	@Override
	public String toString() {
		return "Commodity [commodityId=" + commodityId + ", inCompanyId=" + inCompanyId + ", outCompanyId="
				+ outCompanyId  + ", vatDate=" + vatDate + ", remarks=" + remarks
				+ ", vatUser=" + vatUser + ", checkUser=" + checkUser + ", receiverUser=" + receiverUser + ", userId="
				+ userId + ", user=" + user + ", inCompany=" + inCompany + ", outCompany=" + outCompany + ", listData="
				+ listData + ", photoAdress=" + photoAdress + ", type=" + type + "]";
	}

}
