
tax.controller('userCtrl',function($scope,$http){
  if (typeof sessionStorage.user === "undefined" || JSON.parse(sessionStorage.user).type!=1) {
		history.back();
	}
  $scope.currentPage = 1;
  $scope.totalPage = 1;
  $scope.size = 15;
	/****** 获取用户表start ********/
  $scope.getUser = function(page,size) {
    $http.get(getAllUserURL,{
      params:{
        currentPage:page-1,
        size:size
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.users=res.resultParm.list;
          $scope.currentPage = res.resultParm.currentPage+1;
          $scope.totalPage = res.resultParm.totalPages;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
  $scope.getRecord = function(){
    $scope.getUser($scope.currentPage,$scope.size);
  }
 /****** 获取用户表end ********/

 /********* 删除用户start  *************/
 $scope.showModal = false;
 $scope.deleteUser = function(id){
   $scope.showModal = !$scope.showModal;
   $scope.deleteUserId = id;
 };
 $scope.sureDeleteUser =function(){
   $http.get(delUserURL,{
     params:{userId:$scope.deleteUserId}
   })
   .success(function(res){
     alertMes(res.serviceResult,res.resultInfo);
     $scope.getRecord();
   }).error(function(){
       alertMes('noHttp',null);
   });
   $scope.showModal = !$scope.showModal;
 }
 /********* 删除用户end  *************/

  /****** 修改用户start *********/
  $scope.userTypeNum;
  $scope.updateUser = function(index,id){
    $scope.users[index].editActive = !$scope.users[index].editActive;
  }
  $scope.changeSelect = function(){
    console.log($scope.userTypeNum);
  }
  $scope.sureUpdateUser = function(index,id){
    $http.post(updateUserURL,{
      userId:id,
      type:$scope.users[index].userTypeNum
    })
    .success(function(res){
      if (!res.serviceResult) {
        alertMes(res.serviceResult,res.resultInfo);
      }else {
        $scope.users[index].type = parseInt($scope.users[index].userTypeNum);
      }
    }).error(function(){
      alertMes('noHttp',null);
    });
    $scope.users[index].editActive = !$scope.users[index].editActive;
  }
  /****** 修改用户end *********/

  $scope.getRecord();
});
