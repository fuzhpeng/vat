package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.SmallClass;

public interface SmallClassService {

	List<SmallClass> getSmallClass();// 获取所有小类别信息

	SmallClass findSmallClass(Integer SmallClassId);

	public void addSmallClass(SmallClass smallClass); // 添加小类别信息

	public void updateSmallClass(SmallClass smallClass); // 更新小类别信息

	public void deleteSmallClass(Integer id);// 删除小类别信息

	List<Integer> getInSmallClass(Integer companyId, Integer largeClassId,Integer smallClassId, Integer type, Integer year, String num);// 获取进销对比图

	List<Integer> getOutSmallClass(Integer companyId, Integer largeClassId,Integer smallClassId, Integer type, Integer year, String num);// 获取进销对比图

	List<Integer> getInBySmallClass(Integer companyId, Integer smallClassId, Integer year);// 获取进销对比图

	List<Integer> getOutBySmallClass(Integer companyId, Integer smallClassId, Integer year);// 获取进销对比图

	List<Double> getMoneyBySmallClass(Integer companyId, Integer largeClassId, Integer type, Integer year, String num);// 获取进小类别的营业额

	List<Double> getMoneyBySmallClass(Integer smallClassId, Integer type, Integer year, String num);// 获取进小类别的营业额

	List<Double> getMoneyByLargeClass(Integer largeClassId, Integer type, Integer year, String num);// 获取进小类别的营业额

	List<Double> getVatBySmallClass(Integer companyId, Integer largeClassId, Integer type, Integer year, String num);// 获取进小类别的税率

	List<List<Integer>> getOutSmallClass(Integer companyId, String data);

	List<Integer> forecast(Integer companyId, Integer smallClassId);// 预测小类别的销售量

	List<String> forecastMonth(Integer companyId, Integer smallClassId);// 预测小类别的月份
}
