package com.hydx.vat.service;

import com.hydx.vat.entity.RolePrivilege;

public interface RolePrivilegeService {

	/**
	 * 更新角色权限
	 */
	public Boolean updateRolePrivilege(RolePrivilege rp);
	
}
