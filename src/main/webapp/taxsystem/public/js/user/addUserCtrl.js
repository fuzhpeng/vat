var app = angular.module('registerPart', []);
app.controller('registerCtrl', function($scope,$http) {
  $http.get(getCompanyURL)
    .success(function(res){
      $scope.companys = res.resultParm.list;
    })
    .error(function(){
  });
  //手机号码验证
  $scope.phoneTest = function (num){
    var re =/^1[3|7|5|8]\d{9}$/;
     if (re.test(num)) {
         return true;
     } else {
         return false;
     }
  }
  $scope.addUser = function(){
    if ($scope.phoneTest($scope.user.phone)) {
      if ($scope.user.password!=$scope.password) {
        Notify("两次密码输入不一致", 'top-right', '5000', 'error', 'fa-times', true);
      }else {
        $http.post(addUserURL,$scope.user)
        .success(function(res){
          if(res.serviceResult){
            alert("注册成功！");
            location.href="/vat/taxsystem/login.html";
          }else {
            Notify(res.resultInfo, 'top-right', '5000', 'danger', 'fa-times', true);
          }
        })
        .error(function(){
          Notify("出现网络问题，注册失败", 'top-right', '5000', 'warning', 'fa-bolt', true);
        });
      }
    }else {
      Notify("请输入正确的手机号码", 'top-right', '5000', 'error', 'fa-times', true);
    }
  }
});
