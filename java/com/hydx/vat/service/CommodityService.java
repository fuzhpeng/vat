package com.hydx.vat.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.hydx.vat.entity.Commodity;

public interface CommodityService {

	Page<Commodity> getCommodity(Pageable pageable, Integer commodityType, Integer CompanyId, String startDate,
			String endDate, String date); // 获取所有增值税信息

	void addCommodity(Commodity commodity);// 添加信息

	String addphoto(MultipartFile file, HttpServletRequest request) throws IOException;// 上传图片
	
	void addAllCommodity(MultipartFile commodityfile,MultipartFile listfile) throws IOException;//批量添加

	Double getAllPrice(Integer companyId, Integer type, String date);// 获取进销金额

	Double getAllVatMoney(Integer companyId, String date);// 获取纳税金额

	List<Integer> getAllYear(Integer year);// 获取所有年份\
	
	List<Integer> getAllYear();// 获取所有年份
	
	List<String> getMonthBycompanyId(Integer companyId);//获取所有的年份
	
	List<String> getMonthBycompanyId(Integer companyId,Integer year);//获取所有的年份
	
	List<String> getMonthBycompanyId();//获取所有的年份

	List<String> getQuarter(Integer companyId);//获取所有的季度
	
	List<String> getQuarter(Integer companyId,Integer year);//获取所有的季度
	
	List<String> getQuarter();//获取所有的季度
	
	Commodity getCommodity(String numble); // 获取具体订单信息
	
	Commodity getCommodity(Integer companyId); // 获取具体订单信息

	List<List<Double>> getTurnoverGrowth();// 获取年度营业额增长

	List<List<Double>> getTurnoverGrowthByMonth(String data);// 获取月度营业额增长

	List<Double> getCompanyTurnoverByMonth(Integer companyId, String data);

	List<Double> getInTurnoverByCompanyId(Integer companyId);// 获取公司年度进项总价

	List<Double> getOutTurnoverByCompanyId(Integer companyId);// 获取公司年度出项总价

	List<Double> getAllVatByCompanyId(Integer companyId, String data);// 公司年度税额

	List<Double> getAllVatByCompanyId(Integer companyId);// 公司年度税额

	List<List<Double>> getVatGrowth();// 年度税额增长图表
	List<Double> getVatGrowth(Integer companyId, Integer year, Integer type);// 年度税额增长图表

	List<List<Double>> getVatGrowth(Integer year, Integer type);// 年度税额增长图表
	
	List<List<Double>> getVatGrowthByMonth(String data);// 月度税额增长图表

	List<Double> getGoodProportion(String data, Integer companyId);// 商品的占营业额比例

	List<Double> getGoodVatProportion(String data, Integer companyId);// 商品的占税比例

	List<Double> getTurnoverGrowthByCompany(Integer companyId, Integer year, Integer type);// 公司年度营业额
	List<List<Double>> getTurnoverGrowthByCompany(Integer year, Integer type);// 所有公司年度营业额
	
	List<Integer> getMonth(String data);//获取月份

	List<String> getMonthBycompanyId(Integer companyId, String data);//获取月份

	List<Double> getAllTurnover(Integer year, Integer type); //获取所有公司的营业总额

	List<Double> getAllVat(Integer year, Integer type);//获取所有公司的纳税总额

	void delCommodity(Integer commodityId);//删除

	Page<Commodity> getCommodityByNum(Pageable pageable, String vatNumber); //模糊查询
}
