package com.hydx.vat.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.CompanyDao;
import com.hydx.vat.dao.LargeClassDao;
import com.hydx.vat.dao.ListDataDao;
import com.hydx.vat.dao.SmallClassDao;
import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.ListData;
import com.hydx.vat.entity.SmallClass;
import com.hydx.vat.service.SmallClassService;

@Service("SmallClassService")
public class SmallClassServiceImpl implements SmallClassService {

	@Autowired
	private SmallClassDao smallClassDao;

	@Autowired
	private LargeClassDao largeClassDao;

	@Autowired
	private ListDataDao listDataDao;

	@Autowired
	private CommodityDao commodityDao;

	@Autowired
	private CompanyDao companyDao;

	@Override
	public List<SmallClass> getSmallClass() {
		return smallClassDao.findAll();
	}

	@Override
	public void addSmallClass(SmallClass smallClass) {
		smallClassDao.save(smallClass);
	}

	@Override
	public void deleteSmallClass(Integer id) {
		smallClassDao.delete(id);
	}

	@Override
	public List<Integer> getInSmallClass(Integer companyId, Integer largeClassId, Integer smallClassId, Integer type, Integer year,
			String num) {
		List<Integer> allSmallClassYearUnit = new ArrayList<Integer>();
		List<Integer> years = commodityDao.getAllYear();
		Integer Allunit = 0;
		Integer unit=0;
		List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);

		if (type == 1) {
			commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + " " + num);
		}
		if (type == 2) {
			commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "-" + num);
		}
		System.out.println(commoditys + "commoditys");
		for (Integer commodityId : commoditys) {
			System.out.println(commodityId + "commodityId");
			System.out.println(smallClassId+"smallClassId");
			if(largeClassId!=null){
				 unit = listDataDao.getLargeClassAccount(commodityId, largeClassId);
			}else{
			 unit = listDataDao.getSmallClassAccount(commodityId, smallClassId);
			}
			System.out.println(unit + "unit");
			if (unit == null) {// 如何判断没有支出则为0
				unit = 0;
			}
			Allunit += unit;
		}
		allSmallClassYearUnit.add(Allunit);

		Integer count = allSmallClassYearUnit.size();
		System.out.println(count + "count");

		return allSmallClassYearUnit;
	}
	/*
	 * List<Integer> allSmallClassYearUnit; List<List<Integer>> b = new
	 * ArrayList<List<Integer>>(); List<Integer> years =
	 * commodityDao.getAllYear(); List<SmallClass> smallClassList =
	 * smallClassDao.findAll();
	 * 
	 * for (SmallClass smallClass : smallClassList) { Integer Allunit = 0;
	 * allSmallClassYearUnit = new ArrayList<Integer>(); // 小类别商品每年的库存
	 * 
	 * for (Integer data : years) { List<Integer> list =
	 * commodityDao.getGoodAllComodityYear(companyId, "" + data); for (Integer a
	 * : list) { Integer unit = listDataDao.getSmallClassUnit(a,
	 * smallClass.getSmallClassId()); System.out.println(unit + "unit"); if
	 * (unit == null) {// 如何判断没有支出则为0 unit = 0; } Allunit += unit; }
	 * allSmallClassYearUnit.add(Allunit); } b.add(allSmallClassYearUnit); }
	 * return b;
	 */

	@Override
	public List<Integer> getOutSmallClass(Integer companyId, Integer largeClassId, Integer smallClassId, Integer type, Integer year,
			String num) {
		List<Integer> allSmallClassYearUnit = new ArrayList<Integer>();
		Integer unit=0;
		Integer Allunit = 0;
		List<Integer> commoditys = commodityDao.getOutGoodAllComodityYear(companyId);

		if (type == 1) {
			commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + "");
			System.out.println(commoditys + "commoditys111111111111111111111111111111");
		}
		if (type == 2) {
			commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "");
			System.out.println(commoditys + "commoditys");
		}
		System.out.println(commoditys + "commoditys");
		for (Integer commodityId : commoditys) {
			System.out.println(commodityId + "commodityId");
			if(largeClassId!=null){
				 unit = listDataDao.getLargeClassAccount(commodityId, largeClassId);
			}else{
			 unit = listDataDao.getSmallClassAccount(commodityId, smallClassId);
			}
			System.out.println(unit + "unit");
			if (unit == null) {// 如何判断没有支出则为0
				unit = 0;
			}
			Allunit += unit;
			allSmallClassYearUnit.add(Allunit);
		}

		Integer count = allSmallClassYearUnit.size();
		System.out.println(count + "count");
		return allSmallClassYearUnit;
	}

	@Override
	public void updateSmallClass(SmallClass smallClass) {
		smallClassDao.saveAndFlush(smallClass);
	}

	@Override
	public List<List<Integer>> getOutSmallClass(Integer companyId, String data) {
		List<Integer> allSmallClassYearUnit;
		List<List<Integer>> b = new ArrayList<List<Integer>>();
		List<SmallClass> smallClassList = smallClassDao.findAll();
		for (SmallClass smallClass : smallClassList) {
			Integer Allunit = 0;
			allSmallClassYearUnit = new ArrayList<Integer>(); // 小类别商品每年的库存
			List<Integer> list = commodityDao.getInGoodAllComodity(companyId, "" + data);
			for (Integer a : list) {
				List<Integer> unit = listDataDao.getSmallClassUnit(a, smallClass.getSmallClassId(), data);
			}
			b.add(allSmallClassYearUnit);
		}
		return b;
	}

	@Override
	public List<Integer> getInBySmallClass(Integer companyId, Integer smallClassId, Integer year) {
		List<Integer> allSmallClassYearUnit;
		List<Integer> years = commodityDao.getAllYear();

		Integer Allunit = 0;
		allSmallClassYearUnit = new ArrayList<Integer>(); // 小类别商品每年的库存
		for (Integer data : years) {
			List<Integer> commoditys = commodityDao.getInGoodAllComodity(companyId, "" + data);
			for (Integer commodityId : commoditys) {
				Integer unit = listDataDao.getSmallClassUnit(commodityId, smallClassId);
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Allunit += unit;
				allSmallClassYearUnit.add(Allunit);
			}
		}
		Integer count = Allunit.hashCode();
		if (year == 1 && count > 2) {
			return allSmallClassYearUnit.subList(count - 3, count);
		}
		if (year == 2 && count > 4) {
			return allSmallClassYearUnit.subList(count - 5, count);
		}
		if (year == 3 && count > 9) {
			return allSmallClassYearUnit.subList(count - 10, count);
		}
		return allSmallClassYearUnit;
	}

	@Override
	public List<Integer> getOutBySmallClass(Integer companyId, Integer smallClassId, Integer year) {
		List<Integer> allSmallClassYearUnit;
		List<Integer> years = commodityDao.getAllYear();

		Integer Allunit = 0;
		allSmallClassYearUnit = new ArrayList<Integer>(); // 小类别商品每年的库存
		for (Integer data : years) {
			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId, "" + data);
			for (Integer commodityId : commoditys) {
				Integer unit = listDataDao.getSmallClassUnit(commodityId, smallClassId);
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Allunit += unit;
				allSmallClassYearUnit.add(Allunit);
			}
		}
		Integer count = Allunit.hashCode();
		if (year == 1 && count > 2) {
			return allSmallClassYearUnit.subList(count - 3, count);
		}
		if (year == 2 && count > 4) {
			return allSmallClassYearUnit.subList(count - 5, count);
		}
		if (year == 3 && count > 9) {
			return allSmallClassYearUnit.subList(count - 10, count);
		}
		return allSmallClassYearUnit;
	}

	@Override
	public List<Double> getMoneyBySmallClass(Integer companyId, Integer largeClassId, Integer type, Integer year,
			String num) {
		List<Double> allSmallClassYearUnit = new ArrayList<Double>();
		
		List<SmallClass> smallClasses = smallClassDao.findByLargeClassId(largeClassId);
		
			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);
			if (year > 4) {
				commoditys = commodityDao.getGoodAllComodityYear(companyId, "" + year);
			}
			if (type == 1) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + " " + num);
			}
			if (type == 2) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (SmallClass smallClass : smallClasses) {
				Double Allunit = 0.0;
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityIdcommodityIdcommodityIdcommodityIdcommodityId");
				Double unit = listDataDao.getSmallClassMoney(commodityId, smallClass.getSmallClassId());
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0.0;
				}
				Allunit += unit;
			}
			allSmallClassYearUnit.add(Allunit);
		}

		Integer count = allSmallClassYearUnit.size();
		System.out.println(count + "count");

		return allSmallClassYearUnit;
	}

	@Override
	public List<Double> getVatBySmallClass(Integer companyId, Integer largeClassId, Integer type, Integer year,
			String num) {
		List<Double> allSmallClassYearUnit = new ArrayList<Double>();
		List<Integer> years = commodityDao.getAllYear();
		List<SmallClass> smallClasses = smallClassDao.findByLargeClassId(largeClassId);
		Double place = 0.0;

		
			Double Allunit = 0.0;
			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);
			if (year > 4) {
				commoditys = commodityDao.getGoodAllComodityYear(companyId, "" + year);
			}

			if (type == 1) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + " " + num);
			}
			if (type == 2) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (SmallClass smallClass : smallClasses) {
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityId");
				Double unit = listDataDao.getSmallClassVat(commodityId, smallClass.getSmallClassId());
				place = listDataDao.getSmallClassPlace(commodityId, smallClass.getSmallClassId());
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0.0;
				}
				Allunit += unit;
			}
			allSmallClassYearUnit.add(Allunit);
		}

		Integer count = allSmallClassYearUnit.size();
		System.out.println(count + "count");

		return allSmallClassYearUnit;
	}

	@Override
	public List<Double> getMoneyBySmallClass(Integer largeClassId, Integer type, Integer year, String num) {
		List<Double> allSmallClassYearUnit = new ArrayList<Double>();
		List<Integer> years = commodityDao.getAllYear();
		Double Allunit = 0.0;
		Double place = 0.0;
		List<Company> lists = companyDao.findAll();
		for (Company company : lists) {

			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(company.getCompanyId());

			if (type == 1) {
				commoditys = commodityDao.getGoodAllComodityQuter(company.getCompanyId(), year + " " + num);
			}
			if (type == 2) {
				commoditys = commodityDao.getGoodAllComodityMonth(company.getCompanyId(), year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityIdcommodityIdcommodityIdcommodityId");
				Integer unit = listDataDao.getSmallClassAccount(commodityId, largeClassId);
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Double aa = unit.doubleValue();
				System.out.println(unit + "unit");
				Allunit += aa;
				System.out.println("AllunitAllunitAllunitAllunitAllunit" + Allunit);
			}
			allSmallClassYearUnit.add(Allunit);
		}
		return allSmallClassYearUnit;
	}

	@Override
	public List<Double> getMoneyByLargeClass(Integer largeClassId, Integer type, Integer year, String num) {
		List<Double> allSmallClassYearUnit = new ArrayList<Double>();
		List<Integer> years = commodityDao.getAllYear();
		Double Allunit = 0.0;
		Double place = 0.0;
		List<Company> lists = companyDao.findAll();
		for (Company company : lists) {

			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(company.getCompanyId());

			if (type == 1) {
				commoditys = commodityDao.getGoodAllComodityQuter(company.getCompanyId(), year + " " + num);
			}
			if (type == 2) {
				commoditys = commodityDao.getGoodAllComodityMonth(company.getCompanyId(), year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityIdcommodityIdcommodityIdcommodityId");
				Integer unit = listDataDao.getLargeClassMoney(commodityId, largeClassId);
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Double aa = unit.doubleValue();
				System.out.println(unit + "unit");
				Allunit += aa;
				System.out.println("AllunitAllunitAllunitAllunitAllunit" + Allunit);
			}
			allSmallClassYearUnit.add(Allunit);
		}
		return allSmallClassYearUnit;
	}

	@Override
	public List<Integer> forecast(Integer companyId, Integer smallClassId) {
		System.out.println("companyId"+companyId);
		System.out.println("smallClassId"+smallClassId);
		List<Integer> forecastUnit = new ArrayList<Integer>();
		List<BigDecimal> smallClassAccountByMonth = new ArrayList<BigDecimal>();
		List<Integer> years = commodityDao.getAllYear();
		Integer size = years.size();
		smallClassAccountByMonth = listDataDao.getSmallClassAccountByMonth(companyId, smallClassId);
		System.out.println(smallClassAccountByMonth+"smallClassAccountByMonth");
		for (BigDecimal a : smallClassAccountByMonth) {
			Integer aa = a.intValue();
			Integer ss = (aa/size);
			forecastUnit.add(ss);
		}

		return forecastUnit;
	}

	@Override
	public List<String> forecastMonth(Integer companyId, Integer smallClassId) {
		// TODO Auto-generated method stub
		return listDataDao.getSmallClassMonth(companyId, smallClassId);
	}

	@Override
	public SmallClass findSmallClass(Integer SmallClassId) {
		// TODO Auto-generated method stub
		return smallClassDao.findOne(SmallClassId);
	}
}