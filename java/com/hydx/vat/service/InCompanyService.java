package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;

public interface InCompanyService {

	List<InCompany> getInCompany();// 获取所有公司

	void addInCompany(InCompany company);// 添加公司

	Page<InCompany> getInCompany(Pageable pageable);// 获取所有公司

	void deleteInCompany(Integer id);// 删除公司

	void updateInCompany(InCompany company);// 更新

	List<InCompany> getInCompany(String InCompanyName);

	List<Double> getTurnoverProportion(Integer companyId, Integer year,Integer type,Integer num); // 客户对营业额的贡献占比

	InCompany getInCompanyById(Integer companyId);

	List<Integer> AcountCompanyByPlace(); // 通过地点统计数量

	List<String> getPlace();
	
	List<Integer> AcountCompanyByPlace(Integer companyId); // 通过地点统计数量

	List<String> getPlace(Integer companyId);
}
