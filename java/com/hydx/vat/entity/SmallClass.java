package com.hydx.vat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SmallClass {
	private int smallClassId;
	private String smallClassName;
	private Integer smallClassRepertory=0;// 商品库存初始化为0
	private Integer largeClassId;// 商品大类别Id

	@Id
	@GeneratedValue
	public int getSmallClassId() {
		return smallClassId;
	}

	public void setSmallClassId(int smallClassId) {
		this.smallClassId = smallClassId;
	}

	public String getSmallClassName() {
		return smallClassName;
	}

	public void setSmallClassName(String smallClassName) {
		this.smallClassName = smallClassName;
	}
	public Integer getSmallClassRepertory() {
		return smallClassRepertory;
	}

	public void setSmallClassRepertory(Integer smallClassRepertory) {
		this.smallClassRepertory = smallClassRepertory;
	}

	public Integer getLargeClassId() {
		return largeClassId;
	}

	public void setLargeClassId(Integer largeClassId) {
		this.largeClassId = largeClassId;
	}

	

	public SmallClass() {
		super();
	}

}
