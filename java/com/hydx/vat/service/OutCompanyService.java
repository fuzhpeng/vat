package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.OutCompany;

public interface OutCompanyService {

	List<OutCompany> getOutCompany();// 获取所有公司

	void addOutCompany(OutCompany company);// 添加公司

	Page<OutCompany> getOutCompany(Pageable pageable);// 获取所有公司

	void deleteOutCompany(Integer id);// 删除公司

	void updateOutCompany(OutCompany company);// 更新

	List<OutCompany> getOutCompany(String inCompanyName);

	List<Double> getTurnoverProportion(Integer companyId,Integer year,Integer type,Integer num);// 供货商对进货总价的占比

	OutCompany getOutCompanyById(Integer companyId);

	List<Integer> AcountCompanyByPlace(); // 通过地点统计数量

	List<String> getPlace();
	
	List<Integer> AcountCompanyByPlace(Integer companyId); // 通过地点统计数量

	List<String> getPlace(Integer companyId);
}
