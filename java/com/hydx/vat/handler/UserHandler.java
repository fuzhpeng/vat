package com.hydx.vat.handler;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.User;
import com.hydx.vat.service.UserService;
import com.hydx.vat.tool.MD5Util;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/userHandler")
public class UserHandler {

	@Autowired
	private UserService userService;

	/*
	 * 添加用户
	 */
	@RequestMapping(value = "addUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addUser(@RequestBody User user) {
		ResultMessage result = new ResultMessage();
		try {
			String passWord=MD5Util.md5(user.getPassword());
			user.setPassword(passWord);
			userService.AddUser(user);
			result.setResultInfo("注册成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("注册失败");
		}
	}

	/*
	 * 删除用户
	 */
	@RequestMapping(value = "deleteUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteUser(Integer userId) {
		ResultMessage result = new ResultMessage();
		try {
			userService.DeteleUser(userId);
			result.setResultInfo("删除成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}

	/*
	 * 修改用户
	 */
	@RequestMapping(value = "updateUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateUser(@RequestBody User user) {
		ResultMessage result = new ResultMessage();
		try {
			userService.UpdateUser(user);
			result.setResultInfo("修改成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("修改失败");
		}
	}

	/*
	 * 获取所有用户
	 */
	@RequestMapping(value = "getAllUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllUser(
			@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
			@RequestParam(value = "size", defaultValue = "5") Integer size) {
		ResultMessage result = new ResultMessage();
		Pageable pageable = new PageRequest(currentPage, size);
		try {
			Page<User> page = userService.getAllUser(pageable);
			result.setResultInfo("获取成功");
			result.getResultParm().put("list", page.getContent());
			result.getResultParm().put("totalPages", page.getTotalPages());
			result.getResultParm().put("currentPage", page.getNumber());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("获取失败");
		}
	}

	/*
	 * 设置管理员还是普通用户
	 */
	@RequestMapping(value = "setType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> setType(Integer userId, Integer type) {
		ResultMessage result = new ResultMessage();

		try {
			userService.setType(userId, type);
			result.setResultInfo("设置成功");
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("设置失败");
		}
	}

	/*
	 * 获取用户
	 */
	@RequestMapping(value = "getUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getUser(Integer userId) {
		ResultMessage result = new ResultMessage();
		try {
			result.setResultInfo("获取成功");
			result.getResultParm().put("User", userService.getUser(userId));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("获取失败");
		}
	}

	/*
	 * 登录
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> login(String account, String password) {
		ResultMessage result = new ResultMessage();
		try {
			result.setResultInfo("登录成功");
			result.getResultParm().put("User", userService.login(account, password));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("获取失败");
		}
	}

	/**
	 * 系统登录
	 * 
	 * @return
	 */
	@RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> login(User user) {
		ResultMessage result = new ResultMessage();
		if (StringUtils.isBlank(user.getAccount())) {
			throw new SecurityException("账号不能为空");
		}
		if (StringUtils.isBlank(user.getPassword())) {
			throw new SecurityException("密码不能为空");
		}
		User usera = userService.findOneByName(user.getAccount());
		if (usera == null) {
			throw new SecurityException("该用户不存在");
		}
		String MD5Password = MD5Util.md5(user.getPassword());
		SecurityUtils.getSubject().login(new UsernamePasswordToken(user.getAccount(), MD5Password));
		result.getResultParm().put("user", usera);
		result.setResultInfo("登录成功");
		return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
	}
}
