package com.hydx.vat.handler;

import java.awt.List;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.ListData;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.InCompanyService;
import com.hydx.vat.service.ListDataService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/listDataHandler")
public class ListDataHandler {
	@Autowired
	private ListDataService listDataService;
	@Autowired
	private CommodityService commodityService;

	/*
	 * 获取所有项目信息
	 */
	@RequestMapping(value = "getAllListData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllListData(Integer commodityId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("list", listDataService.getListData(commodityId));
			result.getResultParm().put("commodity", commodityService.getCommodity(commodityId));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 添加项目信息
	 */
	@RequestMapping(value = "addListData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addListData(ListData listData) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			listDataService.addListData(listData);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 更新项目信息
	 */
	@RequestMapping(value = "updateListData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateListData(ListData listData) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			listDataService.updateListData(listData);
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("更新失败");
		}
	}

	/*
	 * 删除项目信息
	 */
	@RequestMapping(value = "deleteListData", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteCompany(Integer id) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			listDataService.deleteListData(id);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}
}
