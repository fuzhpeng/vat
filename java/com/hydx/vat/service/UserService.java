package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.hydx.vat.entity.User;

public interface UserService {

	void AddUser(User user) throws Exception;// 添加用户

	Page<User> getAllUser(Pageable pageable); // 获取所有的用户

	User getUser(Integer id); // 获取用户

	void UpdateUser(User user); // 更新用户

	void DeteleUser(Integer id); // 删除用户

	void setType(Integer userId, Integer type);// 设置管理员还是普通用户

	public User findOneByName(String username);// 根据系统用户名获取用户信息

	public List<String> findPrivilegeCode(int id);// 根据系统用户Id获取用户权限

	public String findRoleName(int id);// 根据系统用户id获取用户角色.

	public void delete(User user);// 删除用户

	public void updatePassword(String username, String oldPassword, String newPassword); // 修改用户密码

	public void updateUserRole(User user); // 修改用户角色

	public List<User> getAllUser(); // 获取所有系统用户

	String login(String account, String password) throws Exception; // 登录

}
