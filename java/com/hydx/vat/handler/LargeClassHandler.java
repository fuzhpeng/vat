package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.LargeClassService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/largeClassHandler")
public class LargeClassHandler {
	@Autowired
	private LargeClassService largeClassService;

	@Autowired
	private CommodityService commodityService;

	/*
	 * 获取所有大类别
	 */
	@RequestMapping(value = "getlargeClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getlargeClass() {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("LargeClass", largeClassService.getLargeClass());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 添加大类别信息
	 */
	@RequestMapping(value = "addLargeClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addLargeClass(@RequestBody LargeClass largeClass) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			largeClassService.addLargeClass(largeClass);
			;
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 更新大类别信息
	 */
	@RequestMapping(value = "updateLargeClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateLargeClass(@RequestBody LargeClass largeClass) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			largeClassService.addLargeClass(largeClass);
			;
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 获取进大类别的营业额
	 */
	@RequestMapping(value = "getMoneyByLargeClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getMoneyByLargeClass(Integer companyId, Integer type, Integer year,
			String num) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("AllLargeClass", largeClassService.getLargeClass());
			result.getResultParm().put("InByLargeClass",
					largeClassService.getMoneyByLargeClass(companyId, type, year, num));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取进大类别的税额
	 */
	@RequestMapping(value = "getVatByLargeClass", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getVatByLargeClass(Integer companyId, Integer type, Integer year, String num) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("AllLargeClass", largeClassService.getLargeClass());
			result.getResultParm().put("InByLargeClass",
					largeClassService.getVatByLargeClass(companyId, type, year, num));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 删除大类别信息
	 */
	@RequestMapping(value = "deletelargeClass", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteLargeClass(Integer largeClassId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			largeClassService.deleteLargeClass(largeClassId);
			result.setResultInfo("删除成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}
}
