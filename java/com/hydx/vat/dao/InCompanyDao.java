package com.hydx.vat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hydx.vat.entity.InCompany;

public interface InCompanyDao extends JpaRepository<InCompany, Integer> {

	InCompany findByCompanyName(String inCompanyName);

	//通过名称模糊搜索
	@Query(value = "select * from incompany  where companyName like %?1%", nativeQuery = true)
	List<InCompany> findByCompanyNameLike(String inCompanyName);

	// 通过地点统计数量
	@Query(value = "SELECT COUNT(companyId) FROM incompany GROUP BY companyRegion", nativeQuery = true)
	List<Integer> AcountCompanyByPlace();

	// 通过地点统计数量
	@Query(value = "SELECT companyRegion FROM incompany GROUP BY companyRegion", nativeQuery = true)
	List<String> getPlace();
}
