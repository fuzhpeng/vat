package com.hydx.vat.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder.In;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.LargeClassDao;
import com.hydx.vat.dao.ListDataDao;
import com.hydx.vat.dao.SmallClassDao;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.SmallClass;
import com.hydx.vat.service.LargeClassService;

@Service("LargeClassService")
public class LargeClassServiceImpl implements LargeClassService {

	@Autowired
	private LargeClassDao largeClassDao;

	@Autowired
	private ListDataDao listDataDao;

	@Autowired
	private SmallClassDao smallClassDao;

	@Autowired
	private CommodityDao commodityDao;

	@Override
	public List<LargeClass> getLargeClass() {
		return largeClassDao.findAll();
	}

	@Override
	public void addLargeClass(LargeClass largeClass) {
		largeClassDao.save(largeClass);
	}

	@Override
	public void deleteLargeClass(Integer id) {
		largeClassDao.delete(id);
	}

	@Override
	public void updateLargeClass(LargeClass largeClass) {
		largeClassDao.saveAndFlush(largeClass);
	}

	@Override
	public List<Integer> getMoneyByLargeClass(Integer companyId, Integer type, Integer year, String num) {
		List<Integer> allSmallClassYearUnit;
		List<Integer> years = commodityDao.getAllYear();
	
		List<LargeClass> largeClasses = largeClassDao.findAll();
		allSmallClassYearUnit = new ArrayList<Integer>();

		
			
			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);
			if (year > 4) {
				commoditys = commodityDao.getGoodAllComodityYear(companyId, "" + year);
			}

			if (type == 1) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + " " + num);
			}
			if (type == 2) {
				System.out.println(2+"22222222222");
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				System.out.println(year + "-" + num+"11111111111");
				commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (LargeClass largeClass : largeClasses) {
				Integer Allunit = 0;
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityId");
				System.out.println(largeClass.getLargeClassId()+"largeClass.getLargeClassId()");
				Integer unit = listDataDao.getLargeClassMoney(commodityId,largeClass.getLargeClassId());
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Allunit += unit;
			}
			allSmallClassYearUnit.add(Allunit);
		}

		Integer count = allSmallClassYearUnit.size();
		System.out.println(count + "count");

		return allSmallClassYearUnit;
	}

	@Override
	public List<Integer> getVatByLargeClass(Integer companyId, Integer type, Integer year, String num) {
		List<Integer> allSmallClassYearUnit;
		List<Integer> years = commodityDao.getAllYear();
		
		List<LargeClass> largeClasses = largeClassDao.findAll();
		allSmallClassYearUnit = new ArrayList<Integer>();

		
			Integer Allunit = 0;
			List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);
			if (year > 4) {
				commoditys = commodityDao.getGoodAllComodityYear(companyId, "" + year);
			}

			Integer count = commoditys.size();
			if (year == 1 && count > 2) {
				commoditys.subList(count - 3, count);
			}
			if (year == 2 && count > 4) {
				commoditys.subList(count - 5, count);
			}
			if (year == 3 && count > 9) {
				commoditys.subList(count - 10, count);
			}
			if (type == 1) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityQuter(companyId, year + " " + num);
			}
			if (type == 2) {
				Integer nums=Integer.valueOf(num);
				if(nums<10){
					num="0"+num;
				}
				commoditys = commodityDao.getGoodAllComodityMonth(companyId, year + "-" + num);
			}
			System.out.println(commoditys + "commoditys");
			for (LargeClass largeClass : largeClasses) {
			for (Integer commodityId : commoditys) {
				System.out.println(commodityId + "commodityId");
				Integer unit = listDataDao.getLargeClassVat(commodityId, largeClass.getLargeClassId());
				System.out.println(unit + "unit");
				if (unit == null) {// 如何判断没有支出则为0
					unit = 0;
				}
				Allunit += unit;
			}
			allSmallClassYearUnit.add(Allunit);
		}
		return allSmallClassYearUnit;
	}

	@Override
	public List<SmallClass> getSmallClass(Integer largeClassId) {
		// TODO Auto-generated method stub
		return smallClassDao.findByLargeClassId(largeClassId);
	}

}
