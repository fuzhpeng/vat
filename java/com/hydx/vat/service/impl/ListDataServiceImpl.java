package com.hydx.vat.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.ListDataDao;
import com.hydx.vat.entity.Commodity;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.ListData;
import com.hydx.vat.service.ListDataService;

@Service("ListDataService")
public class ListDataServiceImpl implements ListDataService {

	@Autowired
	private ListDataDao listDataDao;

	@Autowired
	private CommodityDao commodityDao;

	@Override
	public List<ListData> getListData(Integer commodityId) {
		return listDataDao.findByCommodityId(commodityId);
	}

	@Override
	public void addListData(ListData listData) {
		listDataDao.save(listData);
	}

	@Override
	public void deleteListData(Integer id) {
		listDataDao.delete(id);
	}

	@Override
	public void updateListData(ListData listData) {
		listDataDao.saveAndFlush(listData);
	}

	@Override
	public List<String> getMonthBycompanyId(Integer companyId, Integer smallClassId) {                                                
		List<Integer> commoditys = commodityDao.getGoodAllComodityYear(companyId);
		List<String> years = commodityDao.getMonthBycompanyId(companyId);
	
		return years;
	}

}
