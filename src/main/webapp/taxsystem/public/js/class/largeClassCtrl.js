
tax.controller('largeClassCtrl',function($scope,$http,$stateParams){
	$scope.records = null;// 发票记录
	$scope.recordsMoneyIn = 0;// 总收入
  $scope.recordsMoneyOut = 0;// 总支出
	$scope.vatMoney = 0; //总纳税金额

	$scope.totalPage = 1;//全部页数
	$scope.currentPage = 1;//当前页码
	$scope.pageArr;// 页码数组
	$scope.size = 15;

	$scope.startDate = null;
	$scope.endDate = null;
  $scope.className = $stateParams.className;
	$scope.classId = $stateParams.classId;
	$scope.companyId = $stateParams.companyId;
	$scope.companyName = $stateParams.companyName;
	//发票记录类型 ：0：进项 1：出项,2总记录
	$scope.recordType = 2;

	//获取发票记录
	$scope.getRecord = function(){
		$scope.invoiceRecords($scope.startDate,$scope.endDate,$scope.currentPage);
	}
	$scope.getRecordsMoney = function(){
		$scope.amountMoney($scope.startDate,$scope.endDate);
	}
	//获取   第n页的发票记录
	$scope.invoiceRecords = function(start,end,num){
		if(num>=1 && num<=$scope.totalPage){
				$http.get(getRecordsByLargeClassURL,
					{
						params:{
							startDate:start,
							endDate:end,
							year:$scope.selectYear,
							currentPage:num-1,
							size:$scope.size,
							// companyId:$stateParams.companyId,
							commodityType:$scope.recordType
						}
					}
				)
				.success(function(response){
					$scope.records=response.resultParm.list;
					$scope.totalPage = response.resultParm.totalPages;
					$scope.currentPage = response.resultParm.currentPage+1;
				}).error(function(){
					alertMes('noHttp',null);
			});
		}
	}
	//获取总订单 的金额信息
	$scope.amountMoney = function(start,end){
		$http.get(getMoneyByLargeClassURL,{
				params:{
					startDate:start,
					endDate:end,
					// companyId:$stateParams.companyId,
					year:$scope.selectYear
				}
			})
			.success(function(response){
				$scope.recordMoney=response.resultParm;
			}).error(function(){
				alertMes('noHttp',null);
		});
	}

	//通过时间段查询 第n页的  订单
	$scope.checkByDate = function(){
		var date = $("#reservation").val();
		if(date!=''&&date!=null){
			var dateArr = date.split(" - ");
			$scope.totalPage = 1;//全部页数
			$scope.currentPage = 1;//当前页码
			$scope.startDate = dateArr[0];
			$scope.endDate = dateArr[1] + " 24:00:00";
			$scope.getRecord();
			$scope.getRecordsMoney();
		}
	}
	$scope.keyupDate = function(){
		if(window.event.keyCode == 13){
			$scope.checkByDate();
		}
	}


	//改变筛查发票记录类型
	$scope.changeType = function(num){
		$scope.recordType = num;
		$scope.totalPage = 1;//全部页数
		$scope.currentPage = 1;//当前页码
		$scope.getRecord();
	}

	//遍历已有年份
	$scope.getYear = function(){
		$http.get(getYearByLargeClassURL,{
			params:{
				// companyId:$stateParams.companyId
			}
		}).success(function(res){
			$scope.years = res.resultParm.years;
			$scope.selectYear = $scope.years[$scope.years.length-1]
		}).error(function(){

		});
	}
	$scope.$watch('selectYear',function(nv,ov) {
		if (ov!=undefined) {
			$scope.totalPage = 1;//全部页数
			$scope.currentPage = 1;//当前页码
			$scope.getRecord();
			$scope.getRecordsMoney();
		}
	});

	$scope.showModal = false;
	//删除记录
	$scope.delRecord = function(id){
		$scope.showModal = !$scope.showModal;
		$scope.delRecordId = id;
	}
	$scope.sureDelRecord = function(){
		$http.post(delRecordIdURL,{
			commodityId:$scope.delRecordId
		}).success(function(res){
			$scope.showModal = !$scope.showModal;
			if (res.serviceResult) {
				$scope.getRecord();
			}
			alertMes(res.serviceResult,res.resultInfo);
		}).error(function(){
			alertMes("noHttp",null);
		});
	}


	$scope.getYear();
	$scope.getRecord();
	$scope.getRecordsMoney();

});
