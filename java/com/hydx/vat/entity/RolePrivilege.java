package com.hydx.vat.entity;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 角色-权限  映射实体
 * @author Kellan
 *
 */

@Entity
public class RolePrivilege implements Serializable {
	
    private Integer id;

    private Integer roleId;

    private Integer privilegeId;

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
	@Id
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }

	public Integer getRoleId() {
		return roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Integer getPrivilegeId() {
		return privilegeId;
	}

	public void setPrivilegeId(Integer privilegeId) {
		this.privilegeId = privilegeId;
	}

	@Override
	public String toString() {
		return "RolePrivilege [id=" + id + ", roleId=" + roleId
				+ ", privilegeId=" + privilegeId + "]";
	}

}