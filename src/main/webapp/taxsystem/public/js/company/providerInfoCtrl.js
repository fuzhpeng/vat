tax.controller('providerInfoCtrl',function($scope,$http){

  $scope.size = 15;
  $scope.currentPage = 1;
  $scope.totalPage = 1;

	/****** 获取供应商表start ********/
  $scope.getProvider = function(page,size) {
    $http.get(getProviderURL,{
      params:{
        currentPage:page-1,
        size:size
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.providers=res.resultParm.list;
          $scope.currentPage = res.resultParm.currentPage+1;
          $scope.totalPage = res.resultParm.totalPages;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
  $scope.getRecord = function(){
    $scope.getProvider($scope.currentPage,$scope.size);
  }
 /****** 获取供应商表end ********/

 /********* 删除供应商start  *************/
 $scope.showModal = false;
 $scope.delProvider = function(id){
   $scope.showModal = !$scope.showModal;
   $scope.deleteproviderId = id;
 };
 $scope.sureDelProvider =function(){
   $http.get(delProviderURL,{
     params:{companyId:$scope.deleteproviderId}
   })
   .success(function(res){
     alertMes(res.serviceResult,res.resultInfo);
     $scope.getRecord();
   }).error(function(){
       alertMes('noHttp',null);
   });
   $scope.showModal = !$scope.showModal;
 }
 /********* 删除供应商end  *************/

  /****** 修改供应商start *********/
  $scope.showUModel = false;
  $scope.updateProviderFun = function(obj){
    $scope.updateProvider = obj;
    $scope.showUModel = !$scope.showUModel;
  }
  $scope.sureUpdateProvider = function(){
    $http.post(updateProviderURL,$scope.updateProvider)
    .success(function(res){
      if (!res.serviceResult) {
        alertMes(res.serviceResult,res.resultInfo);
      }else {
        $scope.getRecord();
        $scope.showUModel = !$scope.showUModel;
      }
    }).error(function(){
      alertMes('noHttp',null);
    });
  }
  $scope.hideUModel = function() {
    $scope.showUModel = !$scope.showUModel;
    $scope.getRecord();
  }
/****** 修改供应商end *********/

  /*********  按名称查找供应商start ****************/
  $scope.checkProviderByName = function(id){
    sessionStorage.currentPage = $scope.currentPage;
    $scope.clearResult = !$scope.clearResult;
    if(id!=null && id!='' && id!=' '){
      $http.get(getProviderByNameURL,{params:{OutCompanyName:id}})
      .success(function(res) {
        if (res.serviceResult) {
          $scope.providers=res.resultParm.list;
          $scope.totalPage = res.resultParm.totalPages;
          $scope.currentPage = res.resultParm.currentPage+1;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
      })
      .error(function(){
        alertMes('noHttp',null);
      });
    }
  }
  $scope.keyupId = function(){
    if(window.event.keyCode == 13){
      $scope.checkProviderByName($scope.checkProviderName);
    }
  }
  $scope.clearCheck = function(){
    $scope.getProvider(sessionStorage.currentPage,$scope.size);
    $scope.clearResult = !$scope.clearResult;
    $scope.checkProviderName = "";
  }
  /*********  按名称查找供应商end ****************/


  /**********   添加供货商start***************/
  $scope.newProvider = {};
  $scope.showAModel = false;
  $scope.addProviderBtn = function() {
    $scope.showAModel = !$scope.showAModel;
  }
  $scope.addProvider = function() {
    $http.post(addProviderURL,$scope.newProvider)
    .success(function(res){
      if (!res.serviceResult) {
        alertMes(res.serviceResult,res.resultInfo);
      }else {
        $scope.getRecord();
        $scope.showAModel = !$scope.showAModel;
      }
    }).error(function(){
      alertMes('noHttp',null);
    });
  }
  /**********   添加供货商end***************/


  $scope.getRecord();
});
