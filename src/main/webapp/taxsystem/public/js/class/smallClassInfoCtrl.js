tax.controller('smallClassInfoCtrl',function($scope,$http,$stateParams){

	/****** 获取商品小类别表start ********/
  $scope.getClass = function() {
    $http.get(getSmallClassURL,{
      params:{
        largeClassId:$stateParams.largeClassId
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.classes=res.resultParm.smallClass;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
 /****** 获取商品小类别表end ********/


 /********* 删除商品小类别start  *************/
 $scope.showModal = false;
 $scope.showDelModel = function(id){
   $scope.showModal = true;
   $scope.deleteId = id;
 }
 $scope.closeModel = function(){
   $scope.showModal = false;
 }
 $scope.deleteGoods = function(){
   $http.get(delSmallClassURL,{
     params:{smallClassId:$scope.deleteId}
   })
   .success(function(response){
     alertMes(response.serviceResult,response.resultInfo);
     $scope.showModal = false;
     if (response.serviceResult) {
       $scope.getClass();
     }
   }).error(function(){
     alertMes('noHttp',null);
     $scope.showModal = false;
   });
 }
 /********* 删除商品小类别end  *************/


  /****** 修改商品小类别start *********/
  $scope.updateGoods = function(index,id){
		$scope.classes[index].editActive = !$scope.classes[index].editActive;
	}
	$scope.sureUpdateGoods = function(index,id){
		$http.post(updateSmallClassURL,{
			smallClassId:id,
			smallClassName:$scope.classes[index].newSmallClassName,
			smallClassRepertory:$scope.classes[index].newSmallClassRepertory
		})
		.success(function(response){
			alertMes(response.serviceResult,response.resultInfo);
      if (response.serviceResult) {
        $scope.getClass();
      }
		}).error(function(){
			alertMes('noHttp',null);
		});
	}
  /****** 修改商品小类别end *********/


  /****** 新增商品类别start *********/
  $scope.newSmallClass = {};
	$scope.addSmallClass = function(){
		$scope.showAddClass = !$scope.showAddClass;
	}
	$scope.sureAddClass = function(){$scope.newSmallClass
		$http.post(addSmallClassURL,{
      smallClassName:$scope.newSmallClass.smallClassName,  //小类别名称
      largeClassId:$stateParams.largeClassId,  //大类别id
      smallClassRepertory:$scope.newSmallClass.smallClassRepertory  //小类别的库存，默认值为0,
    })
			.success(function(response){
				alertMes(response.serviceResult,response.resultInfo);
				$scope.showAddClass = !$scope.showAddClass;
        $scope.getClass();
			}).error(function(){
				alertMes('noHttp',null);
		});
	}
  /****** 新增商品类别end *********/


  $scope.getClass();
});
