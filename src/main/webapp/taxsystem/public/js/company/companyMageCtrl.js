
tax.controller('companyMageCtrl',function($scope,$http){

  $scope.currentPage = 1;
  $scope.totalPage = 1;
  $scope.size = 15;
  /********* 获取公司信息start  *****************/
  $scope.getCompany = function(page,size){
    $http.get(getCompanyURL,{
      params:{
        currentPage:page-1,
        size:size
      }
    })
    .success(function(res){
      if (res.serviceResult) {
        $scope.companys = res.resultParm.list;
        $scope.currentPage = res.resultParm.currentPage+1;
        $scope.totalPage = res.resultParm.totalPages;
      }else {
        alertMes(res.serviceResult,res.resultInfo);
      }
    })
    .error(function(){
      alertMes('noHttp',null);
    });
  }
  $scope.getRecord = function(){
    $scope.getCompany($scope.currentPage,$scope.size);
  }
  /********* 获取公司信息end  *****************/

  /********* 删除公司信息start  *****************/
  $scope.delCompany = function(id) {
      $scope.delId = id;
      $scope.showModal = !$scope.showModal;
  }
  $scope.sureDeleteCompany = function(){
    $http.get(delCompanyURL,{
      params:{companyId:$scope.delId}
    })
    .success(function(res){
      alertMes(res.serviceResult,res.resultInfo);
      $scope.showModal = !$scope.showModal;
      $scope.getRecord();
    })
    .error(function() {
      alertMes('noHttp',null);
    });
  }
  /********* 删除公司信息end  *****************/


  /********* 修改公司信息start  *****************/
    $scope.companyEdit = {};
  	$scope.updateCompany = function(index,id){
  		$scope.companys[index].editActive = !$scope.companys[index].editActive;
  	}
  	$scope.sureUpdateCompany = function(index,id){
      $scope.companyEdit.companyId = id;
  		$http.post(updateCompanyURL,$scope.companyEdit)
  		.success(function(response){
        if (response.serviceResult) {
          $scope.getRecord();
        }else {
            alertMes(response.serviceResult,response.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  		});
  		$scope.companys[index].editActive = !$scope.companys[index].editActive;
  	}
  /********* 修改公司信息end  *****************/


  /********* 添加公司信息start  *****************/
	$scope.newCompany = {};
	$scope.addCompany = function(){
		$scope.showAddCompany = !$scope.showAddCompany;
	}
	$scope.sureAddCompany = function(){
		$http.post(addCompanyURL,$scope.newCompany)
			.success(function(response){
				alertMes(response.serviceResult,response.resultInfo);
				$scope.showAddCompany = !$scope.showAddCompany;
        $scope.getRecord();
			}).error(function(){
				alertMes('noHttp',null);
		});
	}
  /********* 添加公司信息end  *****************/

  $scope.getRecord();
  $http.get(getCompanyURL)
  .success(function(res) {
    $scope.allCompanys = res.resultParm.list;
  })
});
