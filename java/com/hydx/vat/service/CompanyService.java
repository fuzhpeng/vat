package com.hydx.vat.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hydx.vat.entity.Company;

public interface CompanyService {

	List<Company> getCommodity();//获取所有公司
	void addCommodity(Company company);//添加公司
    Page<Company> getCommodity(Pageable pageable);//获取所有公司
    void deleteCommodity(Integer id);//删除公司
	void updateCommodity(Company company);//更新
	
}
