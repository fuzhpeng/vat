package com.hydx.vat.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.OutCompanyDao;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.OutCompany;
import com.hydx.vat.service.OutCompanyService;

@Service("OutCompanyService")
public class OutCompanyServiceImpl implements OutCompanyService {

	@Autowired
	private OutCompanyDao outCompanyDao;

	@Autowired
	private CommodityDao commodityDao;

	@Override
	public List<OutCompany> getOutCompany() {
		return outCompanyDao.findAll();
	}

	@Override
	public void addOutCompany(OutCompany company) {
		outCompanyDao.save(company);
	}

	@Override
	public Page<OutCompany> getOutCompany(Pageable pageable) {
		return outCompanyDao.findAll(pageable);
	}

	@Override
	public void deleteOutCompany(Integer id) {
		outCompanyDao.delete(id);
	}

	@Override
	public void updateOutCompany(OutCompany company) {
		outCompanyDao.saveAndFlush(company);
	}

	@Override
	public List<OutCompany> getOutCompany(String outCompanyName) {
		return outCompanyDao.findByCompanyNameLike(outCompanyName);
	}

	@Override
	public List<Double> getTurnoverProportion(Integer companyId, Integer year, Integer type, Integer num) {
		List<Double> allMoney = new ArrayList<Double>();
		Double bb = 0.0;
		List<OutCompany> list = outCompanyDao.findAll();
		for (OutCompany company : list) {
			List<Double> money = commodityDao.getInProportionByCompanyId(companyId, company.getCompanyId());
			Integer count = money.size();
			if (year > 3) {
				if (type == 1) {
					money = commodityDao.getInProportionByQuarter(companyId, company.getCompanyId(), year + " " + num);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				}
				if (type == 2) {
					money = commodityDao.getInProportionByMonth(companyId, company.getCompanyId(), year + "-" + num);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				} else {
					money = commodityDao.getInProportionByCompanyId(companyId, company.getCompanyId(), year);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				}
			}

			if (year == 1 && count > 2) {
				money = money.subList(count - 3, count);
				for (Double aa : money) {
					bb += aa;
				}
			}
			if (year == 2 && count > 4) {
				money = money.subList(count - 5, count);
				for (Double aa : money) {
					bb += aa;
				}
			}
			if (year == 0) {
				for (Double aa : money) {
					bb += aa;
				}
			}
			allMoney.add(bb);
		}

		return allMoney;
	}

	@Override
	public OutCompany getOutCompanyById(Integer companyId) {
		return outCompanyDao.findOne(companyId);
	}

	@Override
	public List<Integer> AcountCompanyByPlace() {
		// TODO Auto-generated method stub
		return outCompanyDao.AcountCompanyByPlace();
	}

	@Override
	public List<String> getPlace() {
		// TODO Auto-generated method stub
		return outCompanyDao.getPlace();
	}

	@Override
	public List<Integer> AcountCompanyByPlace(Integer companyId) {
		List<String> place = new ArrayList<String>();
		List<Integer> count = new ArrayList<Integer>();
		List<Integer> incompanyIds = commodityDao.getInCompanyId(companyId);
		for (Integer incompanyId : incompanyIds) {
			System.out.println(incompanyId+"incompanyId");
			place.add(outCompanyDao.findOne(incompanyId).getCompanyRegion());
		}
		Set<String> set = new HashSet<String>();
		set.addAll(place);// 给set填充
		for (String a : set) {
			count.add(Collections.frequency(place, a));
		}
		return count;
	}

	@Override
	public List<String> getPlace(Integer companyId) {
		List<String> place = new ArrayList<String>();
		List<Integer> incompanyIds = commodityDao.getOutCompanyId(companyId);
		for (Integer incompanyId : incompanyIds) {
			place.add(outCompanyDao.findOne(incompanyId).getCompanyRegion());
		}
		return outCompanyDao.getPlace();
	}
}
