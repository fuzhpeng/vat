
tax.controller('dataStatisticCtrl',function($http,$scope,$stateParams){
	if (typeof sessionStorage.user === "undefined" || JSON.parse(sessionStorage.user).type!=1) {
		history.back();
	}
	//遍历已有年份
	$scope.getYear = function(){
		$http.get(getAllYearURL,{
			params:{
				// companyId:$stateParams.companyId
			}
		}).success(function(res){
			$scope.years = res.resultParm.year;
			$scope.selectYear = $scope.years[$scope.years.length-1];
		}).error(function(){

		});
	}
  $scope.getYear();
	//遍历商品大类别
	$http.get(getLargeClassURL)
		.success(function(res){
			if (res.serviceResult) {
				$scope.classes=res.resultParm.LargeClass;
				$scope.chart3.largeClassId = $scope.classes[0].largeClassId;
				$scope.chart4.largeClassId = $scope.classes[0].largeClassId;
				$scope.getClass($scope.classes[0].largeClassId);
				$scope.getInitCharts4();
				$scope.getInitCharts3();
			}else {
				alertMes(res.serviceResult,res.resultInfo);
			}
		}).error(function(){
			alertMes('noHttp',null);
	});
	$scope.getClass = function(id) {
    $http.get(getSmallClassURL,{
      params:{
        largeClassId:id
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.smallclasses=res.resultParm.smallClass;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
	/*******************  营业额/税额增长图表  ********************/
	$scope.chart1 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:false,
		chartType:true,
		requestType:0,
		requestYear:0
	};
	//按月筛选
	$scope.checkByMonth1 = function(year,index){
		var temp = $scope.chart1.monthType;
		var temp2 = $scope.chart1.chartType;
		$scope.chart1 = {
			showByYear:false,
			showByMonth:true,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:temp,
			chartType:temp2
		};
		$scope.chart1.yearsArray[index]=true;
		$scope.chart1.requestYear = year;
		if($scope.chart1.monthType){$scope.chart1.requestType = 1;}
		else {$scope.chart1.requestType = 2;}
		$scope.getInitCharts1();
	}
	$scope.selMonthType1 = function(num){
		switch (num) {
			case 0:$scope.chart1.monthType = false;break;
			case 1:$scope.chart1.monthType = true;break
		}
		if($scope.chart1.monthType){$scope.chart1.requestType = 1;}
		else {$scope.chart1.requestType = 2;}
		$scope.getInitCharts1();
	}
	//按年筛选
	$scope.checkByYear1 = function (num) {
		var temp2 = $scope.chart1.chartType;
		$scope.chart1 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:false,
			chartType:temp2
		};
		$scope.chart1.conditions[num] = true;
		$scope.chart1.requestYear = num;
		$scope.chart1.requestType = 0;
		$scope.getInitCharts1();
	}
	//切换数据展示形式
	$scope.selChartType1 = function(num){
		switch (num) {
			case 0:$scope.chart1.chartType = true;break;
			case 1:$scope.chart1.chartType = false;break;
		}
		$scope.getInitCharts1();
	}
	$scope.getInitCharts1 = function(){
		var companys = [];//所有公司   图表的标注
		var companyName = $stateParams.companyName;
		companys.push(companyName);
		var xAxis = []; //横坐标
		var yAxis = [];//纵坐标
		var url;var data;
		if($scope.chart1.chartType){
			url = lineP_URL; //营业额
			data = "营业额";
		}
		else {
			url = lineV_URL; //税额
			data = "税额";
		}
		$http.get(url,{
			params:{
				year:$scope.chart1.requestYear,
				type:$scope.chart1.requestType,
				companyId:$stateParams.companyId
			}
		})
		.success(function(res){
			if (res.serviceResult) {
				var dataArr = res.resultParm.TurnoverGrowth;
		    var option = {
				    title: {
				        text: $stateParams.companyName+data+'增长折线图'
				    },
				    tooltip: {
				        trigger: 'axis'
				    },
				    legend: {
				        data:companys
				    },
				    grid: {
				        left: '3%',
				        right: '4%',
				        bottom: '3%',
				        containLabel: true
				    },
				    toolbox: {
							show : true,
							feature : {
									dataView : {show: true, readOnly: false},
									restore : {show: true},
									magicType: {type: ['line', 'bar']},
									saveAsImage : {show: true}
							}
				    },
				    xAxis: {
				        type: 'category',
				        boundaryGap: false,
				        data:res.resultParm.time
				    },
				    yAxis: {
				        type: 'value'
				    },
				    series: [
				        {
				            name:$stateParams.companyName,
				            type:'line',
				            stack: data,
				            data:dataArr,
										markPoint: {
				                data: [
				                    {type: 'max', name: '最大值'},
				                    {type: 'min', name: '最小值'}
				                ]
				            },
				            markLine: {
				                data: [
				                    {type: 'average', name: '平均值'}
				                ]
				            }
				        }
				    ]
				};
				var myChart = echarts.init(document.getElementById('charts1'));
		    // 绘制图表
		    myChart.setOption(option);
			}
		});
	}
	$scope.getInitCharts1();
	/*******************  营业额/税额增长图表  ********************/


	/***********  商品大分类的营业额/税额饼图    *******************/
	$scope.chart2 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:1,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		requestSource:0,//请求数据源，0表示营业额，1表示税额
		showSeason:false,
		showMonth:false
	};
	//选择总计，最近几年
	$scope.checkByYear2 = function (num) {
		var temp = $scope.chart2.chartType;
		$scope.chart2 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp,
			requestType:0,
			requestYear:0,
			showSeason:false,
			showMonth:false
		};
		$scope.chart2.conditions[num] = true;
		$scope.chart2.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.chart2.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.chart2.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitCharts2();
	}

	//选择具体某一年
	$scope.checkByMonth2 = function(year,index){
		$scope.chart2.conditions = [];
		$scope.chart2.yearsArray = [];
		$scope.chart2.showByYear = false;$scope.chart2.showByMonth=true;
		$scope.chart2.yearsArray[index]=true;
		$scope.chart2.requestYear = year;
		$scope.getInitCharts2();
	}

	//选择具体某一年的年度统计
	$scope.selYear2 = function(){
		$scope.chart2.monthType = [true,false,false];
		$scope.chart2.showSeason = false;$scope.chart2.showMonth = false;
		$scope.chart2.requestType = 0;$scope.chart2.requestData = null;
		$scope.getInitCharts2();
	}

	//选择具体某一年的季度统计
	$scope.selSeason2 = function(){
		$scope.chart2.requestData = 1;
		$scope.chart2.season = [];
		$scope.chart2.season[0]=true;
		$scope.chart2.monthType = [false,true,false];
		$scope.chart2.showSeason = true;$scope.chart2.showMonth = false;
		$scope.chart2.requestType = 1;
		$scope.getInitCharts2();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason2 = function(num){
		$scope.chart2.requestData = num;
		$scope.chart2.season = [];
		$scope.chart2.season[num-1]=true;
		$scope.getInitCharts2();
	}

	//选择具体某一年的月度统计
	$scope.selMonth2 = function(){
		$scope.chart2.requestData = 1;
		$scope.chart2.month=[];
		$scope.chart2.month[0]=true;
		$scope.chart2.monthType = [false,false,true];
		$scope.chart2.showMonth = true;$scope.chart2.showSeason = false;
		$scope.chart2.requestType = 2;
		$scope.getInitCharts2();0
	}
	//选择了某一个月
	$scope.selRequestMonth2 = function(num){
		$scope.chart2.requestData = num;
	  $scope.chart2.month=[];
		$scope.chart2.month[num-1]=true;
		$scope.getInitCharts2();
	}

	//切换请求数据源
	$scope.selChartType2 = function(num){
		switch (num) {
			case 0:$scope.chart2.chartType = true;break;
			case 1:$scope.chart2.chartType = false;break;
		}
		$scope.getInitCharts2();
	}

	$scope.getInitCharts2 = function(){
		var url;var name;
		if($scope.chart2.chartType){
			url=pipeBP_URL;
			name="营业额";
		}
		else {
				url=pipeBV_URL;
				name="税额";
		}
		$http.get(url,{
			params:{
				companyId:$stateParams.companyId,
				year:$scope.chart2.requestYear,
				type:$scope.chart2.requestType,
				num:$scope.chart2.requestData
			}
		})
		.success(function(res){
			if (res.serviceResult) {
				var data=[];var source=[];
				for (var i = 0; i < res.resultParm.InByLargeClass.length; i++) {
					source.push(res.resultParm.AllLargeClass[i].largeClassName);
					var obj={value:res.resultParm.InByLargeClass[i], name:res.resultParm.AllLargeClass[i].largeClassName};
					data.push(obj);
				}
				var	option = {
					    title : {
					        text: '商品大分类'+name+'饼图',
					        subtext: $stateParams.companyName+'所有商品大分类'+name+'占比饼图',
					        x:'center'
					    },
					    tooltip : {
					        trigger: 'item',
					        formatter: "{a} <br/>{b} : {c} ({d}%)"
					    },
					    legend: {
					        orient: 'vertical',
					        left: 'left',
					        data:source
					    },
							toolbox: {
					        show: true,
					        feature: {
					            dataView: {readOnly: false},
					            saveAsImage: {}
					        }
					    },
					    series : [
					        {
					            name:name,
					            type: 'pie',
					            radius : '55%',
					            center: ['50%', '60%'],
					            data:data,
					            itemStyle: {
					                emphasis: {
					                    shadowBlur: 10,
					                    shadowOffsetX: 0,
					                    shadowColor: 'rgba(0, 0, 0, 0.5)'
					                }
					            }
					        }
					    ]
					};

			}
	    echarts.init(document.getElementById('charts2')).setOption(option);
		})
	}
	$scope.getInitCharts2();
	/***********  商品大分类的营业额/税额饼图    *******************/


	/***********  商品小分类的营业额/税额饼图    *******************/
	$scope.chart3 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		classArray:[true],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:0,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		requestSource:0,//请求数据源，0表示营业额，1表示税额
		showSeason:false,
		showMonth:false
	}
	//选择某一商品大分类
	$scope.selClass = function(id,index) {
		$scope.chart3.largeClassId = id;
		$scope.chart3.classArray = [];
		$scope.chart3.classArray[index] = true;
		$scope.getInitCharts3();
	}
	//选择总计，最近几年
	$scope.checkByYear3 = function (num) {
		var temp = $scope.chart3.chartType;
		var temp1 = $scope.chart3.classArray;
		var temp2 = $scope.chart3.largeClassId;
		$scope.chart3 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			classArray:temp1,
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp,
			requestType:0,
			requestYear:0,
			largeClassId:temp2,
			showSeason:false,
			showMonth:false
		};
		$scope.chart3.conditions[num] = true;
		$scope.chart3.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.chart3.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.chart3.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitCharts3();
	}

	//选择具体某一年
	$scope.checkByMonth3 = function(year,index){
		$scope.chart3.conditions = [];
		$scope.chart3.yearsArray = [];
		$scope.chart3.showByYear = false;$scope.chart3.showByMonth=true;
		$scope.chart3.yearsArray[index]=true;
		$scope.chart3.requestYear = year;
		$scope.getInitCharts3();
	}

	//选择具体某一年的年度统计
	$scope.selYear3 = function(){
		$scope.chart3.monthType = [true,false,false];
		$scope.chart3.showSeason = false;$scope.chart3.showMonth = false;
		$scope.chart3.requestType = 0;$scope.chart3.requestData = null;
		$scope.getInitCharts3();
	}

	//选择具体某一年的季度统计
	$scope.selSeason3 = function(){
		$scope.chart3.requestData = 1;
		$scope.chart3.season=[];
		$scope.chart3.season[0]=true;
		$scope.chart3.monthType = [false,true,false];
		$scope.chart3.showSeason = true;$scope.chart3.showMonth = false;
		$scope.chart3.requestType = 1;
		$scope.getInitCharts3();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason3 = function(num){
		$scope.chart3.requestData = num;
		$scope.chart3.season = [];
		$scope.chart3.season[num-1]=true;
		$scope.getInitCharts3();
	}

	//选择具体某一年的月度统计
	$scope.selMonth3 = function(){
		$scope.chart3.requestData = 1;
		$scope.chart3.month=[];
		$scope.chart3.month[0]=true;
		$scope.chart3.monthType = [false,false,true];
		$scope.chart3.showMonth = true;$scope.chart3.showSeason = false;
		$scope.chart3.requestType = 2;
		$scope.getInitCharts3();
	}
	//选择了某一个月
	$scope.selRequestMonth3 = function(num){
		$scope.chart3.requestData = num;
	  $scope.chart3.month=[];
		$scope.chart3.month[num-1]=true;
		$scope.getInitCharts3();
	}

	//切换请求数据源
	$scope.selChartType3 = function(num){
		switch (num) {
			case 0:$scope.chart3.chartType = true;break;
			case 1:$scope.chart3.chartType = false;break;
		}
		$scope.getInitCharts3();
	}

	$scope.getInitCharts3 = function(){
		var url;var name;
		if($scope.chart3.chartType){
			url=pipeSP_URL;
			name="营业额";
		}
		else {
				url=pipeSV_URL;
				name="税额";
		}
		$http.get(url,{
			params:{
				companyId:$stateParams.companyId,
				year:$scope.chart3.requestYear,
				type:$scope.chart3.requestType,
				num:$scope.chart3.requestData,
				largeClassId:$scope.chart3.largeClassId
			}
		})
		.success(function(res){
			if (res.serviceResult) {
			var data=[];var source=[];
			for (var i = 0; i < res.resultParm.InBySmallClass.length; i++) {
				source.push(res.resultParm.smallClass[i].smallClassName);
				var obj={value:res.resultParm.InBySmallClass[i], name:res.resultParm.smallClass[i].smallClassName};
				data.push(obj);
			}
			var	option = {
						title : {
								text: '商品小分类'+name+'饼图',
								subtext: $stateParams.companyName+'商品小分类'+name+'占比饼图',
								x:'center'
						},
				    tooltip: {
				        trigger: 'item',
				        formatter: "{a} <br/>{b}: {c} ({d}%)"
				    },
				    legend: {
				        orient: 'vertical',
				        x: 'left',
				        data:source
				    },
						toolbox: {
				        show: true,
				        feature: {
				            dataView: {readOnly: false},
				            saveAsImage: {}
				        }
				    },
				    series: [
				        {
				            name:name,
				            type:'pie',
				            radius: ['40%', '55%'],
				            data:data
				        }
				    ]
				};
			}
	    echarts.init(document.getElementById('charts3')).setOption(option);
		})
	}
	/***********  商品小分类的营业额/税额饼图    *******************/


	/***********  每种商品的进销对比图表    *******************/
	$scope.chart4 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		largeClassArray:[true],
		smallClassArray:[],
		monthType:false,
		chartType:true,
		requestType:0,
		requestYear:0,
		largeClassId:null,
		smallClassId:null
	};
	$scope.all = true;
	//选择商品大类别
	$scope.selAll = function(){
		$scope.all = true;$scope.chart4.smallClassArray = [];
		$scope.chart4.smallClassId = null;
		$scope.getInitCharts4();
	}
	$scope.selClass4 = function(id,index) {
		$scope.getClass(id);
		$scope.all = true;$scope.chart4.smallClassArray=[];
		$scope.chart4.largeClassId = id;
		$scope.chart4.smallClassId = null;
		$scope.chart4.largeClassArray = [];
		$scope.chart4.largeClassArray[index] = true;
		$scope.getInitCharts4();
	}
	$scope.selSmallClass4 = function(id,index) {
		$scope.all = false;
		$scope.chart4.smallClassId = id;
		$scope.chart4.smallClassArray = [];
		$scope.chart4.smallClassArray[index] = true;
		$scope.getInitCharts4();
	}
	//按月筛选
	$scope.checkByMonth4 = function(year,index){
		var temp0 = $scope.chart4.monthType;
		var temp1 = $scope.chart4.chartType;
		var temp2 = $scope.chart4.largeClassArray;
		var temp3=$scope.chart4.smallClassArray;
		var temp4 = $scope.chart4.largeClassId;
		var temp5=$scope.chart4.smallClassId;
		$scope.chart4 = {
			showByYear:false,
			showByMonth:true,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:temp0,
			chartType:temp1,
			largeClassArray:temp2,
			smallClassArray:temp3,
			largeClassId:temp4,
			smallClassId:temp5
		};
		$scope.chart4.yearsArray[index]=true;
		$scope.chart4.requestYear = year;
		if($scope.chart4.monthType){$scope.chart4.requestType = 1;}
		else {$scope.chart4.requestType = 2;}
		$scope.getInitCharts4();
	}
	$scope.selMonthType4 = function(num){
		switch (num) {
			case 0:$scope.chart4.monthType = false;break;
			case 1:$scope.chart4.monthType = true;break
		}
		if($scope.chart4.monthType){$scope.chart4.requestType = 1;}
		else {$scope.chart4.requestType = 2;}
		$scope.getInitCharts4();
	}
	//按年筛选
	$scope.checkByYear4 = function (num) {
		var temp0 = $scope.chart4.chartType;
		var temp1 = $scope.chart4.largeClassArray;
		var temp2=$scope.chart4.smallClassArray;
		var temp3 = $scope.chart4.largeClassId;
		var temp4=$scope.chart4.smallClassId;
		$scope.chart4 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:false,
			chartType:temp0,
			largeClassArray:temp1,
			smallClassArray:temp2,
			largeClassId:temp3,
			smallClassId:temp4
		};
		$scope.chart4.conditions[num] = true;
		$scope.chart4.requestYear = num;
		$scope.chart4.requestType = 0;
		$scope.getInitCharts4();
	}
	$scope.getInitCharts4 = function(){
		$http.get(bar_URL,{
			params:{
				year:$scope.chart4.requestYear,
				type:$scope.chart4.requestType,
				largeClassId:$scope.chart4.largeClassId,
				smallClassId:$scope.chart4.smallClassId,
				companyId:$stateParams.companyId
			}
		})
		.success(function(res){
			if (res.serviceResult) {
				var option = {
						title : {
								text: name+'进销对比图表',
						},
						tooltip : {
								trigger: 'axis'
						},
						legend: {
								data:['进货量','销售量']
						},
						toolbox: {
								show : true,
								feature : {
										dataView : {show: true, readOnly: false},
										magicType : {show: true, type: ['line', 'bar']},
										restore : {show: true},
										saveAsImage : {show: true}
								}
						},
						calculable : true,
						xAxis : [
								{
										type : 'category',
										data :res.resultParm.time
								}
						],
						yAxis : [
								{
										type : 'value'
								}
						],
						series : [
								{
										name:'进货量',
										type:'bar',
										data:res.resultParm.inSmallClass,
										markPoint : {
												data : [
														{type : 'max', name: '最大值'},
														{type : 'min', name: '最小值'}
												]
										},
										markLine : {
												data : [
														{type : 'average', name: '平均值'}
												]
										}
								},
								{
										name:'销售量',
										type:'bar',
										data:res.resultParm.outSmallClass,
										markPoint : {
												data : [
														{type : 'max', name: '最大值'},
														{type : 'min', name: '最小值'}
												]
										},
										markLine : {
												data : [
														{type : 'average', name : '平均值'}
												]
										}
								}
						]
				};
				echarts.init(document.getElementById('charts4')).setOption(option);
			}
		});
	}
	/***********  每种商品的进销对比图表   *******************/


	/***********  客户对营业额的贡献占比    *******************/
	$scope.chart5 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:0,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		requestSource:0,//请求数据源，0表示营业额，1表示税额
		showSeason:false,
		showMonth:false
	};
	//选择总计，最近几年
	$scope.checkByYear5 = function (num) {
		var temp = $scope.chart5.chartType;
		$scope.chart5 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp,
			requestType:0,
			requestYear:0,
			showSeason:false,
			showMonth:false
		};
		$scope.chart5.conditions[num] = true;
		$scope.chart5.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.chart5.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.chart5.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitCharts5();
	}

	//选择具体某一年
	$scope.checkByMonth5 = function(year,index){
		$scope.chart5.conditions = [];
		$scope.chart5.yearsArray = [];
		$scope.chart5.showByYear = false;$scope.chart5.showByMonth=true;
		$scope.chart5.yearsArray[index]=true;
		$scope.chart5.requestYear = year;
		$scope.getInitCharts5();
	}

	//选择具体某一年的年度统计
	$scope.selYear5 = function(){
		$scope.chart5.monthType = [true,false,false];
		$scope.chart5.showSeason = false;$scope.chart5.showMonth = false;
		$scope.chart5.requestType = 0;$scope.chart5.requestData = null;
		$scope.getInitCharts5();
	}

	//选择具体某一年的季度统计
	$scope.selSeason5 = function(){
		$scope.chart5.requestData = 1;
		$scope.chart5.season=[];
		$scope.chart5.season[0]=true;
		$scope.chart5.monthType = [false,true,false];
		$scope.chart5.showSeason = true;$scope.chart5.showMonth = false;
		$scope.chart5.requestType = 1;
		$scope.getInitCharts5();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason5 = function(num){
		$scope.chart5.requestData = num;
		$scope.chart5.season = [];
		$scope.chart5.season[num-1]=true;
		$scope.getInitCharts5();
	}

	//选择具体某一年的月度统计
	$scope.selMonth5 = function(){
		$scope.chart5.requestData = 1;
		$scope.chart5.month=[];
		$scope.chart5.month[0]=true;
		$scope.chart5.monthType = [false,false,true];
		$scope.chart5.showMonth = true;$scope.chart5.showSeason = false;
		$scope.chart5.requestType = 2;
		$scope.getInitCharts5();
	}
	//选择了某一个月
	$scope.selRequestMonth5 = function(num){
		$scope.chart5.requestData = num;
		$scope.chart5.month=[];
		$scope.chart5.month[num-1]=true;
		$scope.getInitCharts5();
	}

	$scope.getInitCharts5 = function(){
		$http.get(pipeClient_URL,{
			params:{
				companyId:$stateParams.companyId,
				year:$scope.chart5.requestYear,
				type:$scope.chart5.requestType,
				num:$scope.chart5.requestData
			}
		})
		.success(function(res){
			if (res.serviceResult) {
				var dataArr = [];var source = [];
				for (var i = 0; i < res.resultParm.TurnoverProportion.length; i++) {
					var obj = {
						value:res.resultParm.TurnoverProportion[i],
						name:res.resultParm.inCompany[i].companyName
					};
					dataArr.push(obj);
					source.push(res.resultParm.inCompany[i].companyName);
				}
				var option = {
				    title : {
				        text: '各类客户对营业额的贡献占比',
								subtext: $stateParams.companyName+'所有客户的贡献占比饼图',
				        x:'center'
				    },
				    tooltip : {
				        trigger: 'item',
				        formatter: "{a} <br/>{b} : {c} ({d}%)"
				    },
						legend: {
								orient: 'vertical',
								left: 'left',
								data:source
						},
						toolbox: {
								show: true,
								feature: {
										dataView: {readOnly: false},
										saveAsImage: {}
								}
						},
						roseType:'angle',
				    series : [
				        {
				            name:'营业额',
				            type:'pie',
				            radius : [30, 110],
				            center : ['50%', '50%'],
				            roseType : 'area',
				            data:dataArr,
										itemStyle: {
												emphasis: {
														shadowBlur: 10,
														shadowOffsetX: 0,
														shadowColor: 'rgba(0, 0, 0, 0.5)'
												}
										}
				        }
				    ]
				};
			}
			echarts.init(document.getElementById('charts5')).setOption(option);
		});
	}
	$scope.getInitCharts5();
	/***********  客户对营业额的贡献占比    *******************/


/***********  供货商对进货总价的贡献占比    *******************/
	$scope.chart6 = {
		showByYear:true,
		showByMonth:false,
		conditions:[true,false,false,false],
		yearsArray:[],
		monthType:[true,false,false],
		season:[true],
		month:[true],
		chartType:true,
		requestType:0,//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		requestYear:0,//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年，20XX表示具体年份
		requestData:0,//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		requestSource:0,//请求数据源，0表示营业额，1表示税额
		showSeason:false,
		showMonth:false
	};
	//选择总计，最近几年
	$scope.checkByYear6 = function (num) {
		var temp = $scope.chart6.chartType;
		$scope.chart6 = {
			showByYear:true,
			showByMonth:false,
			conditions:[false,false,false,false],
			yearsArray:[],
			monthType:[true,false,false],
			season:[true],
			month:[true],
			chartType:temp,
			requestType:0,
			requestYear:0,
			showSeason:false,
			showMonth:false
		};
		$scope.chart6.conditions[num] = true;
		$scope.chart6.requestYear = num;//请求年份，0表示总计，1表示最近3年，2表示最近5年，3表示最近10年
		$scope.chart6.requestData = null;//请求具体时间，1-4（表示季度），1-12表示月份，为空表示年度统计
		$scope.chart6.requestType = 0;//请求类型，0表示年度统计，1表示季度统计，2表示月度统计
		$scope.getInitCharts6();
	}

	//选择具体某一年
	$scope.checkByMonth6 = function(year,index){
		$scope.chart6.conditions = [];
		$scope.chart6.yearsArray = [];
		$scope.chart6.showByYear = false;$scope.chart6.showByMonth=true;
		$scope.chart6.yearsArray[index]=true;
		$scope.chart6.requestYear = year;
		$scope.getInitCharts6();
	}

	//选择具体某一年的年度统计
	$scope.selYear6 = function(){
		$scope.chart6.monthType = [true,false,false];
		$scope.chart6.showSeason = false;$scope.chart6.showMonth = false;
		$scope.chart6.requestType = 0;$scope.chart6.requestData = null;
		$scope.getInitCharts6();
	}

	//选择具体某一年的季度统计
	$scope.selSeason6 = function(){
		$scope.chart6.requestData = 1;
		$scope.chart6.season = [];
		$scope.chart6.season[0]=true;
		$scope.chart6.monthType = [false,true,false];
		$scope.chart6.showSeason = true;$scope.chart6.showMonth = false;
		$scope.chart6.requestType = 1;
		$scope.getInitCharts6();
	}
	//选择了具体某一个季度
	$scope.selRequestSeason6 = function(num){
		$scope.chart6.requestData = num;
		$scope.chart6.season = [];
		$scope.chart6.season[num-1]=true;
		$scope.getInitCharts6();
	}

	//选择具体某一年的月度统计
	$scope.selMonth6 = function(){
		$scope.chart6.requestData = 1;
		$scope.chart6.month = [];
		$scope.chart6.month[0]=true;
		$scope.chart6.monthType = [false,false,true];
		$scope.chart6.showMonth = true;$scope.chart6.showSeason = false;
		$scope.chart6.requestType = 2;
		$scope.getInitCharts6();
	}
	//选择了某一个月
	$scope.selRequestMonth6 = function(num){
		$scope.chart6.requestData = num;
		$scope.chart6.month=[];
		$scope.chart6.month[num-1]=true;
		$scope.getInitCharts6();
	}

	$scope.getInitCharts6 = function(){
		$http.get(pipeProvider_URL,{
			params:{
				companyId:$stateParams.companyId,
				year:$scope.chart6.requestYear,
				type:$scope.chart6.requestType,
				num:$scope.chart6.requestData
			}
		})
		.success(function(res){
			var dataArr = [];var source = [];
			for (var i = 0; i < res.resultParm.TurnoverProportion.length; i++) {
				var obj = {
					value:res.resultParm.TurnoverProportion[i],
					name:res.resultParm.outCompany[i].companyName
				};
				dataArr.push(obj);
				source.push(res.resultParm.outCompany[i].companyName);
			}
			var option = {
					title: {
							text: '各类供货商对进货总价的贡献占比',
							subtext: $stateParams.companyName+'供货商对进货总价的贡献占比',
							x: 'center'
					},

					tooltip : {
							trigger: 'item',
							formatter: "{a} <br/>{b} : {c} ({d}%)"
					},
					legend: {
							orient: 'vertical',
							left: 'left',
							data:source
					},
					toolbox: {
							show: true,
							feature: {
									dataView: {readOnly: false},
									saveAsImage: {}
							}
					},
					roseType:'angle',
					series : [
							{
									name:'进货总价',
									type:'pie',
									radius : [30, 110],
									center : ['50%', '50%'],
									roseType : 'area',
									data:dataArr,
									itemStyle: {
											emphasis: {
													shadowBlur: 10,
													shadowOffsetX: 0,
													shadowColor: 'rgba(0, 0, 0, 0.5)'
											}
									}
							}
					 ]
			 };
			echarts.init(document.getElementById('charts6')).setOption(option);
		});
	}
	$scope.getInitCharts6();


	// 客户分布图
	$scope.getMap = function(){
		var arr1 = [];var arr2 = [];
		$http.get(mapCP_ALL_URL,{
			params:{
				companyId:$stateParams.companyId
			}
		})
		.success(function(res) {
			for (var i = 0; i < res.resultParm.client.length; i++) {
				var obj = {
					name:res.resultParm.client[i],
					value:res.resultParm.AcountCompanyByclient[i]
				};
				arr1.push(obj);
			}
			for (var j = 0; j < res.resultParm.provider.length; j++) {
				var obj = {
					name:res.resultParm.provider[j],
					value:res.resultParm.AcountCompanyByprovider[j]
				};
				arr2.push(obj);
			}
			var option = {
					title: {
							text:$stateParams.companyName+' 客户/供货商分布图',
							left: 'center'
					},
					tooltip: {
							trigger: 'item'
					},
					legend: {
							orient: 'vertical',
							left: 'left',
							data:['客户','供货商']
					},
					visualMap: {
							min: 0,
							max: 2500,
							left: 'left',
							top: 'bottom',
							text: ['高','低'],           // 文本，默认为数值文本
							calculable: true
					},
					toolbox: {
							show: true,
							orient: 'vertical',
							left: 'right',
							top: 'center',
							feature: {
									dataView: {readOnly: false},
									restore: {},
									saveAsImage: {}
							}
					},
					series: [
							{
									name: '客户',
									type: 'map',
									mapType: 'china',
									roam: false,
									label: {
											normal: {
													show: true
											},
											emphasis: {
													show: true
											}
									},
									data:arr1
							},
							{
									name: '供货商',
									type: 'map',
									mapType: 'china',
									label: {
											normal: {
													show: true
											},
											emphasis: {
													show: true
											}
									},
									data:arr2
							}
					]
			};
			echarts.init(document.getElementById('charts7')).setOption(option);
		});
	}
	$scope.getMapCharts = function() {
		$http.get('json/china.json')
		.success(function(chinaJson){
			echarts.registerMap('china', chinaJson);
			$scope.getMap();
		});
	}
	$scope.getMapCharts();
});
