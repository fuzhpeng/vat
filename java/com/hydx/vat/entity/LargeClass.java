package com.hydx.vat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class LargeClass {

	private int largeClassId;
	private String largeClassName;
	private double largeClassVat;
	private Integer companyId;// 公司iD

	@Id
	@GeneratedValue
	public int getLargeClassId() {
		return largeClassId;
	}

	public void setLargeClassId(int largeClassId) {
		this.largeClassId = largeClassId;
	}

	public String getLargeClassName() {
		return largeClassName;
	}

	public void setLargeClassName(String largeClassName) {
		this.largeClassName = largeClassName;
	}

	public double getLargeClassVat() {
		return largeClassVat;
	}

	public void setLargeClassVat(double largeClassVat) {
		this.largeClassVat = largeClassVat;
	}

	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public LargeClass(int largeClassId, String largeClassName, double largeClassVat, Integer companyId) {
		super();
		this.largeClassId = largeClassId;
		this.largeClassName = largeClassName;
		this.largeClassVat = largeClassVat;
		this.companyId = companyId;
	}

	public LargeClass() {
		super();
	}

}
