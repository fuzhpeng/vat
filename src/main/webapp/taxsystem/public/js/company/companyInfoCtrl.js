tax.controller('companyInfoCtrl',function($scope,$http,$stateParams){
  var myUrl;
  if ($stateParams.type==1) {
    myUrl = getProviderByIdURL;
  }else {
    myUrl = getClientByIdURL;
  }
  /*******   根据id获取公司详情start  ***********/
    $scope.getCompanyInfo = function(id){
      $http.get(myUrl,{
        params:{
          companyId:id
        }
      })
      .success(function(res){
        if (res.serviceResult) {
          $scope.company = res.resultParm.InCompany;
        }else{
          alertMes(res.serviceResult,res.resultInfo);
        }
      })
      .error(function(){
        alertMes('noHttp',null);
      })
    }
  /*******   根据id获取公司详情end  ***********/

  $scope.getCompanyInfo($stateParams.companyId);
});
