package com.hydx.vat.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.OutCompany;

public interface OutCompanyDao extends JpaRepository<OutCompany, Integer> {

	OutCompany findByCompanyName(String outCompanyName);

	@Query(value = "select * from outcompany  where companyName like %?1%", nativeQuery = true)
	List<OutCompany> findByCompanyNameLike(String outCompanyName);

	// 通过地点统计数量
	@Query(value = "SELECT COUNT(companyId) FROM outcompany GROUP BY companyRegion", nativeQuery = true)
	List<Integer> AcountCompanyByPlace();

	// 通过地点统计数量
	@Query(value = "SELECT companyRegion FROM outcompany GROUP BY companyRegion", nativeQuery = true)
	List<String> getPlace();
}
