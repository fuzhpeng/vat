package com.hydx.vat.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class OutCompany {

	private Integer companyId;
	private String companyName;
	private String companyNumber; // 识别号
	private String companyAddress;
	private String companyPhone;
	private String companyAccount; // 开户账号
	private String companyRegion;// 公司区域
	private String bank;

	@Id
	@GeneratedValue
	public Integer getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Integer companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public String getCompanyAddress() {
		return companyAddress;
	}

	public void setCompanyAddress(String companyAddress) {
		this.companyAddress = companyAddress;
	}

	public String getCompanyRegion() {
		return companyRegion;
	}

	public void setCompanyRegion(String companyRegion) {
		this.companyRegion = companyRegion;
	}

	public String getCompanyPhone() {
		return companyPhone;
	}

	public void setCompanyPhone(String companyPhone) {
		this.companyPhone = companyPhone;
	}

	public String getCompanyAccount() {
		return companyAccount;
	}

	public void setCompanyAccount(String companyAccount) {
		this.companyAccount = companyAccount;
	}

	public String getBank() {
		return bank;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public OutCompany(Integer companyId, String companyName, String companyNumber, String companyAddress,
			String companyPhone, String companyAccount) {
		super();
		this.companyId = companyId;
		this.companyName = companyName;
		this.companyNumber = companyNumber;
		this.companyAddress = companyAddress;
		this.companyPhone = companyPhone;
		this.companyAccount = companyAccount;
	}

	public OutCompany() {
		super();
	}

	@Override
	public String toString() {
		return "OutCompany [companyId=" + companyId + ", companyName=" + companyName + ", companyNumber="
				+ companyNumber + ", companyAddress=" + companyAddress + ", companyPhone=" + companyPhone
				+ ", companyAccount=" + companyAccount + "]";
	}

}
