tax.controller('clientInfoCtrl',function($scope,$http){

  $scope.size = 15;
  $scope.currentPage = 1;
  $scope.totalPage = 1;
  $scope.clients = null;
	/****** 获取客户表start ********/
  $scope.getClient = function(page,size) {
    $http.get(getClientURL,{
      params:{
        currentPage:page-1,
        size:size
      }
    })
  		.success(function(res){
        if (res.serviceResult) {
          $scope.clients=res.resultParm.list;
          $scope.currentPage = res.resultParm.currentPage+1;
          $scope.totalPage = res.resultParm.totalPages;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
  		}).error(function(){
  			alertMes('noHttp',null);
  	});
  }
  $scope.getRecord = function(){
    $scope.getClient($scope.currentPage,$scope.size);
  }
 /****** 获取客户表end ********/

 /********* 删除客户start  *************/
 $scope.showModal = false;
 $scope.delClient = function(id){
   $scope.showModal = !$scope.showModal;
   $scope.deleteClientId = id;
 };
 $scope.sureDeleteclient =function(){
   $http.get(delClientURL,{
     params:{companyId:$scope.deleteClientId}
   })
   .success(function(res){
     alertMes(res.serviceResult,res.resultInfo);
     $scope.getRecord();
   }).error(function(){
       alertMes('noHttp',null);
   });
   $scope.showModal = !$scope.showModal;
 }

 /********* 删除客户end  *************/

  /****** 修改客户start *********/
  $scope.showUModel = false;
  $scope.updateClientFun = function(obj){
    $scope.updateClient = obj;
    $scope.showUModel = !$scope.showUModel;
  }
  $scope.sureUpdateClient = function(){
    $http.post(updateClientURL,$scope.updateClient)
    .success(function(res){
      if (!res.serviceResult) {
        alertMes(res.serviceResult,res.resultInfo);
      }else {
        $scope.getRecord();
        $scope.showUModel = !$scope.showUModel;
      }
    }).error(function(){
      alertMes('noHttp',null);
    });
  }
  $scope.hideUModel = function() {
    $scope.showUModel = !$scope.showUModel;
    $scope.getRecord();
  }
  /****** 修改客户end *********/

  /*********  按名称查找客户start ****************/
  $scope.checkClientByName = function(id){
    sessionStorage.currentPage = $scope.currentPage;
    $scope.clearResult = !$scope.clearResult;
    if(id!=null && id!='' && id!=' '){
      $http.get(getClientByNameURL,{params:{InCompanyName:id}})
      .success(function(res) {
        if (res.serviceResult) {
          $scope.clients=res.resultParm.list;
          $scope.totalPage = res.resultParm.totalPages;
          $scope.currentPage = res.resultParm.currentPage+1;
        }else {
          alertMes(res.serviceResult,res.resultInfo);
        }
      })
      .error(function(){
        alertMes('noHttp',null);
      });
    }
  }
  $scope.keyupId = function(){
    if(window.event.keyCode == 13){
      $scope.checkClientByName($scope.checkClientName);
    }
  }
  $scope.clearCheck = function(){
    $scope.getClient(sessionStorage.currentPage,$scope.size);
    $scope.clearResult = !$scope.clearResult;
    $scope.checkClientName = "";
  }
  /*********  按名称查找客户end ****************/


  /**********   添加客户start***************/
  $scope.showAModel = false;
  $scope.newClient = {};
  $scope.addClientBtn = function() {
    $scope.showAModel = !$scope.showAModel;
  }
  $scope.addClient = function() {
    $http.post(addClientURL,$scope.newClient)
    .success(function(res){
      if (!res.serviceResult) {
        alertMes(res.serviceResult,res.resultInfo);
      }else {
        $scope.getRecord();
        $scope.showAModel = !$scope.showAModel;
      }
    }).error(function(){
      alertMes('noHttp',null);
    });
  }
  /**********   添加客户end***************/

  $scope.getRecord();
});
