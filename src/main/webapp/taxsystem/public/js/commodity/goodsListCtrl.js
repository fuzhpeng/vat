tax.controller('goodsListCtrl',function($scope,$http,$stateParams){

  /******  根据发票ID获取进销清单start ***********/
    $scope.getGoodsListt = function(id){
        $http.get(getGoodsListURL,{
          params:{
            commodityId:id
          }
        })
        .success(function(res){
          $scope.goods = res.resultParm.list;
          $scope.vat = res.resultParm.commodity;
        })
        .error(function(){
          alertMes("noHttp",null);
        });
    }
  /******  根据发票ID获取进销清单end ***********/

  $scope.getGoodsListt($stateParams.commodityId);
});
