package com.hydx.vat.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hydx.vat.entity.Company;

public interface CompanyDao extends JpaRepository<Company, Integer> {

}
