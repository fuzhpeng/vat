package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.LargeClass;
import com.hydx.vat.entity.SmallClass;

public interface LargeClassService {

	List<LargeClass> getLargeClass();// 获取所有大类别

	List<SmallClass> getSmallClass(Integer largeClassId);// 获取所有大类别
	
	void updateLargeClass(LargeClass largeClass);// 更新类别

	void addLargeClass(LargeClass largeClass);// 添加类别

	void deleteLargeClass(Integer id);// 删除类别

	List<Integer> getVatByLargeClass(Integer companyId,Integer type, Integer year,String num);

	List<Integer> getMoneyByLargeClass(Integer companyId,Integer type, Integer year,String num);
}
