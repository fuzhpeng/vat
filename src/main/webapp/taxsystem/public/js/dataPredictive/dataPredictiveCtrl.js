tax.controller('dataPredictiveCtrl',function($http,$scope){
	if (typeof sessionStorage.user === "undefined" || JSON.parse(sessionStorage.user).type!=1) {
		history.back();
	}
  //获取所有公司
	$http.get(getCompanyURL)
		.success(function(res){
			$scope.companys = res.resultParm.list;
      $scope.line.requestCompay = {
        id:$scope.companys[0].companyId,
        name:$scope.companys[0].companyName
      };
		})
		.error(function(){
	});
  //获取所有小类别
  $http.get(getSmallClassURL)
		.success(function(res){
      if (res.serviceResult) {
        $scope.lists=res.resultParm.smallClass;
        $scope.line.requestGoods = {
          id:$scope.lists[0].smallClassId,
          name:$scope.lists[0].smallClassName
        };
        $scope.getInitLine();
      }
		}).error(function(){
			alertMes('noHttp',null);
	});

  //折线图
  $scope.line = {
    companys:[true],
    lists:[true],
    requestCompay:null,
    requestGoods:null
  };
  $scope.checkByCompany = function(id,index,name) {
    $scope.line.companys = [];
    $scope.line.companys[index]=true;
    $scope.line.requestCompay = {
      id:id,
      name:name
    };
    $scope.getInitLine();
  }
  $scope.checkByGood = function(id,index,name){
    $scope.line.lists = [];
    $scope.line.lists[index]=true;
    $scope.line.requestGoods = {
      id:id,
      name:name
    };
    $scope.getInitLine();
  }
  $scope.getInitLine = function(){
    $http.get(predictURL,{
      params:{
        companyId:$scope.line.requestCompay.id,
        smallClassId:$scope.line.requestGoods.id
      }
    })
    .success(function(res){
      if (res.serviceResult) {
        var option = {
            title: {
                text: '未来一年'+$scope.line.requestCompay.name+$scope.line.requestGoods.name+'商品销售量预测'
            },
            tooltip : {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                       backgroundColor: '#6a7985'
                    }
                }
            },
            // legend: {
            //     data:[res.resultParm.smallClass.smallClassName]
            // },
            toolbox: {
                feature: {
                    saveAsImage: {}
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis : [
                {
                    type : 'category',
                    boundaryGap : false,
                    data :res.resultParm.time
                }
            ],
            yAxis : [
                {
                    type : 'value'
                }
            ],
            series : [
                {
                    name:res.resultParm.smallClass.smallClassName,
                    type:'line',
                    stack: '总量',
                    areaStyle: {normal: {}},
                    data:res.resultParm.forecast
                }
            ]
        };
        echarts.init(document.getElementById('line')).setOption(option);
      }else {
        alertMes(res.serviceResult,res.resultInfo);
      }
    })
  }
});
