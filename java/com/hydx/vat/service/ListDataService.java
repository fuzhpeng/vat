package com.hydx.vat.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.ListData;

public interface ListDataService {

	List<String> getMonthBycompanyId(Integer companyId,Integer smallClassId);//获取所有的年份
	List<ListData> getListData(Integer commodityId);//获取进销货清单
	void addListData(ListData listData);//添加进销货清单
    void deleteListData(Integer id);//删除公司
	void updateListData(ListData listData);//更新进销货清单
}
