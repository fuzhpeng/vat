package com.hydx.vat.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.entity.OutCompany;
import com.hydx.vat.service.CommodityService;
import com.hydx.vat.service.CompanyService;
import com.hydx.vat.service.InCompanyService;
import com.hydx.vat.service.OutCompanyService;
import com.hydx.vat.tool.ResultMessage;

@Controller
@RequestMapping(value = "/InCompanyHandler")
public class InCompanyHandler {

	@Autowired
	private InCompanyService inCompanyService;

	@Autowired
	private OutCompanyService outCompanyService;

	@Autowired
	private CommodityService commodityService;

	@Autowired
	private InCompanyService inCommodityService;

	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getAllInCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllInCompany(
			@RequestParam(value = "currentPage", defaultValue = "0") Integer currentPage,
			@RequestParam(value = "size", defaultValue = "5") Integer size) {
		ResultMessage result = new ResultMessage();
		try {
			if (currentPage != null && size != null) {
				Pageable pageable = new PageRequest(currentPage, size);
				Page<InCompany> page = inCompanyService.getInCompany(pageable);
				Map<String, Object> map = new HashMap<String, Object>();
				result.setResultInfo("查询成功");
				result.getResultParm().put("list", page.getContent());
				result.getResultParm().put("totalPages", page.getTotalPages());
				result.getResultParm().put("currentPage", page.getNumber());
				return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
			} else {
				result.getResultParm().put("list", inCompanyService.getInCompany());
				return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
			}
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	// 客户对营业额的贡献占比
	@RequestMapping(value = "getTurnoverProportion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getTurnoverGrowth(Integer companyId, Integer year, Integer type, Integer num) {
		ResultMessage result = new ResultMessage();
		try {
			result.setResultInfo("查询成功");
			result.getResultParm().put("TurnoverProportion",
					inCommodityService.getTurnoverProportion(companyId, year, type, num));
			result.getResultParm().put("inCompany", inCommodityService.getInCompany());
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取供货商的分布
	 */
	@RequestMapping(value = "getCompanyPlce", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getAllInCompany(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			if (companyId != null) {
				result.getResultParm().put("client", inCompanyService.getPlace(companyId));
				result.getResultParm().put("AcountCompanyByclient", inCompanyService.AcountCompanyByPlace(companyId));
				result.getResultParm().put("provider", outCompanyService.getPlace());
				result.getResultParm().put("AcountCompanyByprovider",
						outCompanyService.AcountCompanyByPlace(companyId));
			} else {
				result.getResultParm().put("client", inCompanyService.getPlace());
				result.getResultParm().put("AcountCompanyByclient", inCompanyService.AcountCompanyByPlace());
				result.getResultParm().put("provider", outCompanyService.getPlace());
				result.getResultParm().put("AcountCompanyByprovider", outCompanyService.AcountCompanyByPlace());
			}
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取所有公司信息
	 */
	@RequestMapping(value = "getInCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInCompany(String InCompanyName) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("list", inCompanyService.getInCompany(InCompanyName));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 获取公司信息
	 */
	@RequestMapping(value = "getInCompanyById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> getInCompanyById(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			result.setResultInfo("查询成功");
			result.getResultParm().put("InCompany", inCompanyService.getInCompanyById(companyId));
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("查询失败");
		}
	}

	/*
	 * 添加公司信息
	 */
	@RequestMapping(value = "addCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> addCommodity(@RequestBody InCompany company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			inCompanyService.addInCompany(company);
			result.setResultInfo("添加成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("添加失败");
		}
	}

	/*
	 * 更新公司信息
	 */
	@RequestMapping(value = "updateCompany", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> updateCompany(@RequestBody InCompany company) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			inCompanyService.updateInCompany(company);
			result.setResultInfo("更新成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("更新失败");
		}
	}

	/*
	 * 删除公司信息
	 */
	@RequestMapping(value = "deleteCompany", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResultMessage> deleteCompany(Integer companyId) {
		ResultMessage result = new ResultMessage();
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			inCompanyService.deleteInCompany(companyId);
			result.setResultInfo("删除成功");
			result.setResultParm(map);
			return new ResponseEntity<ResultMessage>(result, HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			throw new SecurityException("删除失败");
		}
	}
}
