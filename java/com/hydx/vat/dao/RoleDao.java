package com.hydx.vat.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hydx.vat.entity.Role;

public interface RoleDao extends JpaRepository<Role, Integer> {

	/**
	 * 根据用户Id获取用具角色
	 * 
	 * @author Kellan
	 * @param userId
	 * @return
	 */
	@Query("SELECT r FROM Role r, User u WHERE u.id = ?1 and r.id = u.roleId")
	Role getRoleByUser(Integer userId);

	/**
	 * 根据角色名称获取角色.
	 * 
	 * @author Kellan
	 */
	Role findByRolename(String rolename);

}
