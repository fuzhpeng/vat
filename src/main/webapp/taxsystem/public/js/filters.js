tax.filter("showStatus",function(){
	return function(inputArray){
		var status = "";
		if(inputArray === 1){
			status ="待审核";
		}else if(inputArray === 2){
			status = "已完成";
		}else if(inputArray === 3){
			status = "已驳回";
		}
		return status;
	}
});
tax.filter("commodityType",function(){
	return function(inputArray){
		var status = "";
		if(inputArray === 0){
			status ="进项";
		}else if(inputArray === 1){
			status = "销项";
		}
		return status;
	}
});
tax.filter("userType",function(){
	return function(inputArray){
		var status = "";
		if(inputArray === 0){
			status ="普通管理员";
		}else if(inputArray === 1){
			status = "超级管理员";
		}
		return status;
	}
});
