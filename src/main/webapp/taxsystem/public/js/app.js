var tax = angular.module('tax', ['ui.router']);
var alertMes = function(serviceResult,resultInfo){
	var dataType;
	var iconType;
	if (serviceResult===true) {
		dataType="success";
		iconType='fa-check';
	}
	else if (serviceResult===false) {
		dataType="danger";
		iconType='fa-times'
	}
	else if(serviceResult==='noHttp'){
		dataType='warning';
		iconType='fa-bolt';
		resultInfo="请求得不到响应，请稍后重试..."
	}
	else if (serviceResult==='noPrivilege') {
		dataType='purple';
		iconType='fa-warning';
		resultInfo = "您无此项操作的权限！"
	}
	if(resultInfo!=null){
		Notify(resultInfo, 'top-right', '5000', dataType, iconType, true);
	}
}

tax.config(function($stateProvider,$urlRouterProvider) {

		$urlRouterProvider.otherwise('/welcome');
		//二维码管理模块路由配置
		$stateProvider.state("recordsPage",{
			url:"/recordsPage?companyId&companyName",
			templateUrl: "views/recordsPage.html"
		}).state("smallClassRecords",{
			url:"/smallClassRecords?classId&className&companyId&companyName",
			templateUrl: "views/smallClassRecords.html",
		}).state("largeClassRecords",{
			url:"/largeClassRecords?classId&className&companyId&companyName",
			templateUrl: "views/largeClassRecords.html",
		}).state("importVat",{
			url:"/importVat",
			templateUrl: "views/importVat.html",
		}).state("dataStatistic",{
			url:"/dataStatistic?companyId&companyName",
			templateUrl: "views/dataStatistic.html",
		}).state("dataStatisticAll",{
			url:"/dataStatisticAll",
			templateUrl: "views/dataStatisticAll.html",
		}).state("userInfo",{
			url:"/userInfo",
			templateUrl:"views/userInfo.html"
		}).state("dataPredictive",{
			url:"/dataPredictive",
			templateUrl: "views/dataPredictive.html",
		}).state("showCompanyVat",{
			url:"/showCompanyVat",
			templateUrl: "views/showCompanyVat.html",
		}).state("updateVat",{
			url:"/updateVat?commodityId",
			templateUrl:"views/updateVat.html"
		}).state("vatInfo",{
			url:"/vatInfo?commodityId",
			templateUrl:"views/vatInfo.html"
		}).state("companyInfo",{
			url:"/companyInfo?type&companyId",
			templateUrl:"views/companyInfo.html"
		}).state("smallClassInfo",{
			url:"/smallClassInfo?largeClassId&largeClassName",
			templateUrl:"views/smallClassInfo.html"
		}).state("clientInfo",{
			url:"/clientInfo",
			templateUrl:"views/clientInfo.html"
		}).state("providerInfo",{
			url:"/providerInfo",
			templateUrl:"views/providerInfo.html"
		}).state("companyManage",{
			url:"/companyManage",
			templateUrl:"views/companyManage.html"
		}).state("classInfo",{
			url:"/classInfo",
			templateUrl:"views/classInfo.html"
		}).state("systemUser",{
			url:"/systemUser",
			templateUrl:"views/systemUser.html"
		}).state("goodsList",{
			url:"/goodsList?commodityId",
			templateUrl:"views/goodsList.html"
		}).state("welcome",{
			url:"/welcome",
			templateUrl:"views/welcome.html"
		});
});
