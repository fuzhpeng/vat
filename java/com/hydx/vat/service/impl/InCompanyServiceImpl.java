package com.hydx.vat.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hydx.vat.dao.CommodityDao;
import com.hydx.vat.dao.CompanyDao;
import com.hydx.vat.dao.InCompanyDao;
import com.hydx.vat.entity.Company;
import com.hydx.vat.entity.InCompany;
import com.hydx.vat.service.InCompanyService;

@Service("InCompanyService")
public class InCompanyServiceImpl implements InCompanyService {

	@Autowired
	private InCompanyDao inCompanyDao;

	@Autowired
	private CommodityDao commodityDao;

	@Override
	public List<InCompany> getInCompany() {
		return inCompanyDao.findAll();
	}

	@Override
	public void addInCompany(InCompany company) {
		inCompanyDao.save(company);
	}

	@Override
	public Page<InCompany> getInCompany(Pageable pageable) {
		return inCompanyDao.findAll(pageable);
	}

	@Override
	public void deleteInCompany(Integer id) {
		inCompanyDao.delete(id);
	}

	@Override
	public void updateInCompany(InCompany company) {
		inCompanyDao.saveAndFlush(company);
	}

	@Override
	public List<InCompany> getInCompany(String InCompanyName) {
		return inCompanyDao.findByCompanyNameLike(InCompanyName);
	}

	@Override
	public List<Double> getTurnoverProportion(Integer companyId, Integer year, Integer type, Integer num) {
		List<Double> allMoney = new ArrayList<Double>();
		List<InCompany> list = inCompanyDao.findAll();
		for (InCompany company : list) {

			Double bb = 0.0;
			List<Double> money = commodityDao.getOutProportionByCompanyId(companyId, company.getCompanyId());
			Integer count = money.size();
			if (year > 3) {

				if (type == 1) {
					money = commodityDao.getOutProportionByQuarter(companyId, company.getCompanyId(), year + " " + num);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				}
				if (type == 2) {
					money = commodityDao.getOutProportionByMonth(companyId, company.getCompanyId(), year + "-" + num);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				} else {
					money = commodityDao.getOutProportionByCompanyId(companyId, company.getCompanyId(), year);
					for (Double aa : money) {
						if (aa == null) {
							aa = 0.0;
						}
						bb += aa;
					}
				}
			}
			if (year == 1 && count > 2) {
				money = money.subList(count - 3, count);
				for (Double aa : money) {
					bb += aa;
				}
			}

			if (year == 2 && count > 4) {
				money = money.subList(count - 5, count);
				for (Double aa : money) {
					bb += aa;
				}
			}
			if (year == 0) {
				for (Double aa : money) {
					bb += aa;
				}
			}
			allMoney.add(bb);
		}

		return allMoney;
	}

	@Override
	public InCompany getInCompanyById(Integer companyId) {
		return inCompanyDao.findOne(companyId);
	}

	@Override
	public List<Integer> AcountCompanyByPlace() {
		// TODO Auto-generated method stub
		return inCompanyDao.AcountCompanyByPlace();
	}

	@Override
	public List<String> getPlace() {
		// TODO Auto-generated method stub
		return inCompanyDao.getPlace();
	}

	@Override
	public List<Integer> AcountCompanyByPlace(Integer companyId) {
		List<String> place = new ArrayList<String>();
		List<Integer> count = new ArrayList<Integer>();
		List<Integer> incompanyIds = commodityDao.getOutCompanyId(companyId);
		for (Integer incompanyId : incompanyIds) {
			System.out.println(incompanyId+"incompanyId");
			place.add(inCompanyDao.findOne(incompanyId).getCompanyRegion());
		}
		Set<String> set = new HashSet<String>();
		set.addAll(place);// 给set填充
		for (String a : set) {
			count.add(Collections.frequency(place, a));
		}
		return count;
	}

	@Override
	public List<String> getPlace(Integer companyId) {
		List<String> place = new ArrayList<String>();
		List<Integer> incompanyIds = commodityDao.getInCompanyId(companyId);
		for (Integer incompanyId : incompanyIds) {
			place.add(inCompanyDao.findOne(incompanyId).getCompanyRegion());
		}
		return inCompanyDao.getPlace();
	}
}
